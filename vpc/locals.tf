locals {
  default_tags = {
    Context                 = "${var.context}"
    Environment             = "${var.environment}"
    Team                    = "${var.team}"
    AppRelease              = "${var.app_release}"
    AppSource               = "${var.app_source}"
    ProvisioningTool        = "${var.provisioning_tool}"
    ProvisioningVersion     = "${var.provisioning_version}"
    ProvisioningSource      = "${var.provisioning_source}"
    DeploymentTool          = "${var.deployment_tool}"
    DeploymentBuildName     = "${var.deployment_build_name}"
    DeploymentBuildNumber   = "${var.deployment_build_nr}"
    DeploymentLastExecution = formatdate("DD/MM/YYYY - hh:mm:ss - ZZZ", timestamp())
  }

  subnet_private_count = "${var.use_default_subnet}" == true ? 2 : length(var.subnet_private_options)
  subnet_public_count  = "${var.use_default_subnet}" == true ? 2 : length(var.subnet_public_options)
  vgw_count            = var.vgw_create == true ? 1 : 0
  peer_count           = var.peer_create == true ? length(var.peer_options) : 0
  vpn_count            = var.vpn_create == true ? length(var.vpn_options) : 0
  sg_count             = var.sg_create == true ? length(var.sg_options) : 0
  dhcp_count           = var.dhcp_create == true ? length(var.dhcp_options) : 0
}
