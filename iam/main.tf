resource "aws_iam_role" "this" {
  count                 = "${local.iam_role_count}"
  name                  = lookup(element(var.iam_role_options, count.index), "name")
  force_detach_policies = lookup(element(var.iam_role_options, count.index), "force_detach_policies")
  description           = lookup(element(var.iam_role_options, count.index), "description")
  max_session_duration  = lookup(element(var.iam_role_options, count.index), "max_session_duration")
  assume_role_policy    = file(lookup(element(var.iam_role_options, count.index), "role_json_file"))

  tags = merge(
    {
      Name = lookup(element(var.iam_role_options, count.index), "name"),
    },
    "${local.default_tags}",
    lookup(element(var.iam_role_options, 0), "extraTags")
  )
}

resource "aws_iam_role_policy" "this" {
  count  = "${local.iam_role_count}"
  name   = lookup(element(var.iam_role_options, count.index), "policy_name")
  role   = "${aws_iam_role.this[count.index].id}"
  policy = file(lookup(element(var.iam_role_options, count.index), "policy_json_file"))
}

