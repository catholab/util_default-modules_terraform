output "iam_role_id" {
  value       = "${aws_iam_role.this[*].id}"
  description = "The name of the role."

}
output "iam_role_name" {
  value       = "${aws_iam_role.this[*].name}"
  description = "The name of the role."
}

output "iam_role_arn" {
  value       = "${aws_iam_role.this[*].arn}"
  description = "The Amazon Resource Name (ARN) specifying the role."
}

output "iam_role_create_date" {
  value       = "${aws_iam_role.this[*].create_date}"
  description = "The creation date of the IAM role."
}

output "iam_role_description" {
  value       = "${aws_iam_role.this[*].description}"
  description = "The description of the role."
}

output "iam_role_unique_id" {
  value       = "${aws_iam_role.this[*].unique_id}"
  description = "The stable and unique string identifying the role."
}

output "iam_role_policy_id" {
  value       = "${aws_iam_role_policy.this[*].id}"
  description = "The role policy ID, in the form of role_name:role_policy_name."
}

output "iam_role_policy_name" {
  value       = "${aws_iam_role_policy.this[*].name}"
  description = "The name of the policy."
}

output "iam_role_policy_policy" {
  value       = "${aws_iam_role_policy.this[*].policy}"
  description = "The policy document attached to the role."
}

output "iam_role_policy_role" {
  value       = "${aws_iam_role_policy.this[*].role}"
  description = "The name of the role associated with the policy."
}
