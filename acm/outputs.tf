output "acm_certificate_id" {
  value       = "${aws_acm_certificate.this[*].id}"
  description = "The ARN of the certificate."
}

output "acm_certificate_arn" {
  value       = "${aws_acm_certificate.this[*].arn}"
  description = "The ARN of the certificate."
}

output "acm_certificate_domain_name" {
  value       = "${aws_acm_certificate.this[*].domain_name}"
  description = "The domain name for which the certificate is issued."
}

output "acm_certificate_domain_validation_options" {
  value       = "${aws_acm_certificate.this[*].domain_validation_options}"
  description = "A list of attributes to feed into other resources to complete certificate validation. Can have more than one element, e.g. if SANs are defined. Only set if DNS-validation was used."
}

output "acm_certificate_validation_emails" {
  value       = "${aws_acm_certificate.this[*].validation_emails}"
  description = "A list of addresses that received a validation E-Mail. Only set if EMAIL-validation was used."
}
