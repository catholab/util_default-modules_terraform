resource "aws_acm_certificate" "this" {
  count                     = "${local.acm_certificate_count}"
  domain_name               = lookup(element(var.acm_certificate_options, count.index), "domain_name")
  validation_method         = lookup(element(var.acm_certificate_options, count.index), "validation_method")
  subject_alternative_names = lookup(element(var.acm_certificate_options, count.index), "subject_alternative_names")
  private_key               = lookup(element(var.acm_certificate_options, count.index), "private_key")
  certificate_body          = lookup(element(var.acm_certificate_options, count.index), "certificate_file")
  certificate_chain         = lookup(element(var.acm_certificate_options, count.index), "certificate_chain")

  tags = merge(
    {
      Name = lookup(element(var.acm_certificate_options, count.index), "domain_name"),
    },
    "${local.default_tags}",
    lookup(element(var.acm_certificate_options, count.index), "extraTags")
  )
}
