resource "aws_lb" "this" {
  count                            = "${local.lb_count}"
  name                             = lookup(element(var.lb_options, count.index), "name")
  internal                         = lookup(element(var.lb_options, count.index), "internal")
  load_balancer_type               = lookup(element(var.lb_options, count.index), "load_balancer_type")
  subnets                          = "${data.aws_subnet_ids.this[count.index].ids}"
  security_groups                  = lookup(element(var.lb_options, count.index), "load_balancer_type") == "application" ? "${data.aws_security_groups.this[count.index].ids}" : null
  idle_timeout                     = lookup(element(var.lb_options, count.index), "idle_timeout")
  enable_cross_zone_load_balancing = lookup(element(var.lb_options, count.index), "enable_cross_zone_load_balancing")
  enable_http2                     = lookup(element(var.lb_options, count.index), "enable_http2")
  ip_address_type                  = lookup(element(var.lb_options, count.index), "ip_address_type")
  enable_deletion_protection       = lookup(element(var.lb_options, count.index), "enable_deletion_protection")

  dynamic "access_logs" {
    for_each = lookup(element(var.lb_options, count.index), "create_access_logs") == false ? [] : [for access_logs in lookup(element(var.lb_options, count.index), "access_logs") : {
      bucket = access_logs.bucket
      prefix = access_logs.prefix
    }]

    content {
      bucket = access_logs.value.bucket
      prefix = access_logs.value.prefix
    }
  }

  tags = merge(
    {
      Name = lookup(element(var.lb_options, count.index), "name"),
    },
    "${local.default_tags}",
    lookup(element(var.lb_options, count.index), "extraTags")
  )
}

resource "aws_lb_target_group" "this" {
  count                              = "${local.lb_count}"
  name                               = lookup(element(var.lb_options, count.index), "target_group_name")
  port                               = lookup(element(var.lb_options, count.index), "target_type") == "lambda" ? null : lookup(element(var.lb_options, count.index), "target_group_port")
  protocol                           = lookup(element(var.lb_options, count.index), "target_type") == "lambda" ? null : lookup(element(var.lb_options, count.index), "target_group_protocol")
  vpc_id                             = lookup(element(var.lb_options, count.index), "target_type") == "lambda" ? null : "${data.aws_vpc.this.id}"
  target_type                        = lookup(element(var.lb_options, count.index), "target_type")
  lambda_multi_value_headers_enabled = lookup(element(var.lb_options, count.index), "lambda_multi_value_headers_enabled")
  proxy_protocol_v2                  = lookup(element(var.lb_options, count.index), "proxy_protocol_v2")

  dynamic "stickiness" {
    for_each = lookup(element(var.lb_options, count.index), "create_stickiness") == false ? [] : [for stickiness in lookup(element(var.lb_options, count.index), "stickiness") : {
      type            = stickiness.type
      cookie_duration = stickiness.cookie_duration
      enabled         = stickiness.enabled
    }]

    content {
      type            = stickiness.value.type
      cookie_duration = stickiness.value.cookie_duration
      enabled         = stickiness.value.enabled
    }
  }

  dynamic "health_check" {
    for_each = lookup(element(var.lb_options, count.index), "create_health_check") == false ? [] : [for health_check in lookup(element(var.lb_options, count.index), "health_check") : {
      enabled             = health_check.enabled
      interval            = health_check.interval
      path                = health_check.path
      port                = health_check.port
      protocol            = health_check.protocol
      timeout             = health_check.timeout
      healthy_threshold   = health_check.healthy_threshold
      unhealthy_threshold = health_check.unhealthy_threshold
      matcher             = health_check.matcher
    }]

    content {
      enabled             = health_check.value.enabled
      interval            = health_check.value.interval
      path                = health_check.value.path
      port                = health_check.value.port
      protocol            = health_check.value.protocol
      timeout             = health_check.value.timeout
      healthy_threshold   = health_check.value.healthy_threshold
      unhealthy_threshold = health_check.value.unhealthy_threshold
      matcher             = health_check.value.matcher
    }
  }
}

resource "aws_lb_listener" "this" {
  count             = "${local.lb_count}"
  load_balancer_arn = "${aws_lb.this[count.index].arn}"
  port              = lookup(element(var.lb_options, count.index), "listener_port")
  protocol          = lookup(element(var.lb_options, count.index), "listener_protocol")
  ssl_policy        = lookup(element(var.lb_options, count.index), "listener_policy", null)
  certificate_arn   = lookup(element(var.lb_options, count.index), "listener_certificate_arn", null)

  dynamic "default_action" {
    for_each = [for default_action in lookup(element(var.lb_options, count.index), "default_action") : {
      type             = default_action.type
      target_group_arn = default_action.type == "forward" ? "${aws_lb_target_group.this[count.index].arn}" : null
      block            = default_action.type != "forward" ? default_action.block : null
    }]

    content {
      type             = default_action.value.type
      target_group_arn = default_action.value.target_group_arn

      dynamic "fixed_response" {
        for_each = default_action.value.type != "fixed-response" ? [] : [for fixed_response in default_action.value.block : {
          content_type = fixed_response.content_type
          message_body = fixed_response.message_body
          status_code  = fixed_response.status_code
        }]
        content {
          content_type = fixed_response.value.content_type
          message_body = fixed_response.value.message_body
          status_code  = fixed_response.value.status_code
        }
      }

      dynamic "redirect" {
        for_each = default_action.value.type != "redirect" ? [] : [for redirect in default_action.value.block : {
          host        = redirect.host
          path        = redirect.path
          port        = redirect.port
          protocol    = redirect.protocol
          query       = redirect.query
          status_code = redirect.status_code
        }]
        content {
          host        = redirect.value.host
          path        = redirect.value.path
          port        = redirect.value.port
          protocol    = redirect.value.protocol
          query       = redirect.value.query
          status_code = redirect.value.status_code
        }
      }
    }
  }
}

resource "aws_lb_target_group_attachment" "this" {
  count            = "${local.lb_attachment_count}"
  target_group_arn = "${data.aws_lb_target_group.this[count.index].arn}"
  target_id        = lookup(element(var.lb_attachment_options, count.index), "target_type") == "instance" ? "${data.aws_instance.this[count.index].private_ip}" : "${data.aws_lambda_function.this[count.index].arn}"
  port             = lookup(element(var.lb_attachment_options, count.index), "target_type") == "instance" ? lookup(element(var.lb_attachment_options, count.index), "target_port") : null
}
