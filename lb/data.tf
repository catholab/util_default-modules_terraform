data "aws_vpc" "this" {
  tags = {
    Name = format("vpc-%s", var.context)
  }
}

data "aws_subnet_ids" "this" {
  count  = "${local.lb_count}"
  vpc_id = "${data.aws_vpc.this.id}"
  filter {
    name   = "tag:Name"
    values = lookup(element(var.lb_options, count.index), "subnet_names")
  }
}

data "aws_security_groups" "this" {
  count = "${local.lb_count}"
  filter {
    name   = lookup(element(var.lb_options, count.index), "load_balancer_type") == "network" ? "vpc-id" : "tag:Name"
    values = lookup(element(var.lb_options, count.index), "load_balancer_type") == "network" ? ["${data.aws_vpc.this.id}"] : lookup(element(var.lb_options, count.index), "security_group_names")
  }
}

data "aws_lb_target_group" "this" {
  count      = "${local.lb_attachment_count}"
  name       = lookup(element(var.lb_attachment_options, count.index), "target_group_name")
  depends_on = ["aws_lb_target_group.this"]
}

data "aws_lambda_function" "this" {
  count         = "${local.data_lambda_count}"
  function_name = lookup(element(var.lb_attachment_options, count.index), "lambda_function_name")
}

data "aws_instance" "this" {
  count = "${local.data_instance_count}"
  filter {
    name   = "tag:Name"
    values = list(lookup(element(var.lb_attachment_options, count.index), "instance_name"))
  }
}
