locals {
  default_tags = {
    Context                 = "${var.context}"
    Environment             = "${var.environment}"
    Team                    = "${var.team}"
    AppRelease              = "${var.app_release}"
    AppSource               = "${var.app_source}"
    ProvisioningTool        = "${var.provisioning_tool}"
    ProvisioningVersion     = "${var.provisioning_version}"
    ProvisioningSource      = "${var.provisioning_source}"
    DeploymentTool          = "${var.deployment_tool}"
    DeploymentBuildName     = "${var.deployment_build_name}"
    DeploymentBuildNumber   = "${var.deployment_build_nr}"
    DeploymentLastExecution = formatdate("DD/MM/YYYY - hh:mm:ss - ZZZ", timestamp())
  }

  lb_count            = var.lb_create == true ? length(var.lb_options) : 0
  data_lambda_count   = var.lb_attachment_create == true ? [for option in var.lb_attachment_options : [for key, value in option : value if key == "lambda_function_name"]][0] == [""] ? 0 : length([for option in var.lb_attachment_options : [for key, value in option : value if key == "lambda_function_name"]]) : 0
  data_instance_count = var.lb_attachment_create == true ? [for option in var.lb_attachment_options : [for key, value in option : value if key == "instance_name"]][0] == [""] ? 0 : length([for option in var.lb_attachment_options : [for key, value in option : value if key == "instance_name"]]) : 0
  lb_attachment_count = var.lb_attachment_create == true ? length(var.lb_attachment_options) : 0
}
