resource "aws_ecr_repository" "this" {
  count                = "${local.ecr_repository_count}"
  name                 = lookup(element(var.ecr_repository_options, count.index), "name")
  image_tag_mutability = lookup(element(var.ecr_repository_options, count.index), "image_tag_mutability")

  tags = merge(
    {
      Name = lookup(element(var.ecr_repository_options, count.index), "name"),
    },
    "${local.default_tags}",
    lookup(element(var.ecr_repository_options, count.index), "extraTags")
  )
}

resource "aws_ecr_repository_policy" "this" {
  count      = "${local.ecr_repository_count}"
  repository = "${aws_ecr_repository.this.name}"
  policy     = file(lookup(element(var.ecr_repository_options, count.index), "policy_path"))
}
