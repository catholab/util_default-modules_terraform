output "kinesis_stream_id" {
  value       = "${aws_kinesis_stream.this[*].id}"
  description = "The unique Stream id."

}

output "kinesis_stream_name" {
  value       = "${aws_kinesis_stream.this[*].name}"
  description = "The unique Stream name."
}

output "kinesis_stream_arn" {
  value       = "${aws_kinesis_stream.this[*].arn}"
  description = "The Amazon Resource Name (ARN) specifying the Stream (same as id)."
}

output "kinesis_stream_shard_count" {
  value       = "${aws_kinesis_stream.this[*].shard_count}"
  description = "The count of Shards for this Stream."
}
