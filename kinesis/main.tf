resource "aws_kinesis_stream" "this" {
  count                     = "${local.kinesis_stream_count}"
  name                      = lookup(element(var.kinesis_stream_options, count.index), "name")
  shard_count               = lookup(element(var.kinesis_stream_options, count.index), "shard_count")
  retention_period          = lookup(element(var.kinesis_stream_options, count.index), "retention_period")
  shard_level_metrics       = lookup(element(var.kinesis_stream_options, count.index), "shard_level_metrics")
  enforce_consumer_deletion = lookup(element(var.kinesis_stream_options, count.index), "enforce_consumer_deletion")

  tags = merge(
    {
      Name = lookup(element(var.kinesis_stream_options, count.index), "name"),
    },
    "${local.default_tags}",
    lookup(element(var.kinesis_stream_options, count.index), "extraTags")
  )
}
