resource "aws_sqs_queue" "this" {
  count                       = "${local.sqs_queue_count}"
  name                        = lookup(element(var.sqs_queue_options, count.index), "name")
  delay_seconds               = lookup(element(var.sqs_queue_options, count.index), "delay_seconds")
  max_message_size            = lookup(element(var.sqs_queue_options, count.index), "max_message_size")
  message_retention_seconds   = lookup(element(var.sqs_queue_options, count.index), "message_retention_seconds")
  receive_wait_time_seconds   = lookup(element(var.sqs_queue_options, count.index), "receive_wait_time_seconds")
  policy                      = lookup(element(var.sqs_queue_options, count.index), "policy_json_file") != "" ? file(lookup(element(var.sqs_queue_options, count.index), "policy_json_file")) : null
  redrive_policy              = lookup(element(var.sqs_queue_options, count.index), "redrive_policy_json_file") != "" ? file(lookup(element(var.sqs_queue_options, count.index), "redrive_policy_json_file")) : null
  visibility_timeout_seconds  = lookup(element(var.sqs_queue_options, count.index), "visibility_timeout_seconds")
  fifo_queue                  = lookup(element(var.sqs_queue_options, count.index), "fifo_queue")
  content_based_deduplication = lookup(element(var.sqs_queue_options, count.index), "fifo_queue") == true ? lookup(element(var.sqs_queue_options, count.index), "content_based_deduplication") : null

  tags = merge(
    {
      Name = lookup(element(var.sqs_queue_options, count.index), "name"),
    },
    "${local.default_tags}",
    lookup(element(var.sqs_queue_options, count.index), "extraTags")
  )
}
