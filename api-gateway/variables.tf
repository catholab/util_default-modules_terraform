## GLOBAL VARIABLES ##
variable "context" {
  type = "string"
}

variable "environment" {
  type = "string"
}

variable "team" {
  type = "string"
}

variable "app_release" {
  type = "string"
}

variable "app_source" {
  type = "string"
}

variable "provisioning_tool" {
  type = "string"
}

variable "provisioning_version" {
  type = "string"
}

variable "provisioning_source" {
  type = "string"
}

variable "deployment_tool" {
  type = "string"
}

variable "deployment_build_name" {
  type = "string"
}

variable "deployment_build_nr" {
  type = "string"
}

variable "api_gateway_account_options" {
  type    = "list"
  default = []
}

variable "api_gateway_account_create" {
  type = bool
}

variable "api_gateway_api_key_options" {
  type    = "list"
  default = []
}

variable "api_gateway_api_key_create" {
  type = bool
}

variable "api_gateway_client_certificate_options" {
  type    = "list"
  default = []
}

variable "api_gateway_client_certificate_create" {
  type = bool
}

variable "api_gateway_rest_api_options" {
  type    = "list"
  default = []
}

variable "api_gateway_rest_api_create" {
  type = bool
}

variable "api_gateway_resource_options" {
  type    = "list"
  default = []
}

variable "api_gateway_resource_create" {
  type = bool
}

variable "api_gateway_authorizer_options" {
  type    = "list"
  default = []
}

variable "api_gateway_authorizer_create" {
  type = bool
}

variable "api_gateway_method_options" {
  type    = "list"
  default = []
}

variable "api_gateway_method_create" {
  type = bool
}

variable "api_gateway_documentation_part_options" {
  type    = "list"
  default = []
}

variable "api_gateway_documentation_part_create" {
  type = bool
}

variable "api_gateway_gateway_response_options" {
  type    = "list"
  default = []
}

variable "api_gateway_gateway_response_create" {
  type = bool
}

variable "api_gateway_integration_options" {
  type    = "list"
  default = []
}

variable "api_gateway_integration_create" {
  type = bool
}

variable "api_gateway_deployment_options" {
  type    = "list"
  default = []
}

variable "api_gateway_deployment_create" {
  type = bool
}
