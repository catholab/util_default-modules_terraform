locals {
  api_gateway_account_count            = var.api_gateway_account_create == true ? length(var.api_gateway_account_options) : 0
  api_gateway_api_key_count            = var.api_gateway_api_key_create == true ? length(var.api_gateway_api_key_options) : 0
  api_gateway_client_certificate_count = var.api_gateway_client_certificate_create == true ? length(var.api_gateway_client_certificate_options) : 0
  api_gateway_rest_api_count           = var.api_gateway_rest_api_create == true ? length(var.api_gateway_rest_api_options) : 0
  api_gateway_resource_count           = var.api_gateway_resource_create == true ? length(var.api_gateway_resource_options) : 0
  api_gateway_authorizer_count         = var.api_gateway_authorizer_create == true ? length(var.api_gateway_authorizer_options) : 0
  api_gateway_method_count             = var.api_gateway_method_create == true ? length(var.api_gateway_method_options) : 0
  api_gateway_documentation_part_count = var.api_gateway_documentation_part_create == true ? length(var.api_gateway_documentation_part_options) : 0
  api_gateway_gateway_response_count   = var.api_gateway_gateway_response_create == true ? length(var.api_gateway_gateway_response_options) : 0
  api_gateway_integration_count        = var.api_gateway_integration_create == true ? length(var.api_gateway_integration_options) : 0
  api_gateway_deployment_count         = var.api_gateway_deployment_create == true ? length(var.api_gateway_deployment_options) : 0
}
