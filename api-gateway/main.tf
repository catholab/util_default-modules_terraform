resource "aws_api_gateway_account" "this" {
  count               = "${local.api_gateway_account_count}"
  cloudwatch_role_arn = lookup(element(var.api_gateway_account_options, count.index), "cloudwatch_role_arn")
}

resource "aws_api_gateway_api_key" "this" {
  count       = "${local.api_gateway_api_key_count}"
  name        = lookup(element(var.api_gateway_api_key_options, count.index), "name")
  description = lookup(element(var.api_gateway_api_key_options, count.index), "description")
  enabled     = lookup(element(var.api_gateway_api_key_options, count.index), "enabled")
  value       = lookup(element(var.api_gateway_api_key_options, count.index), "value")
}

resource "aws_api_gateway_client_certificate" "this" {
  count       = "${local.api_gateway_client_certificate_count}"
  description = lookup(element(var.api_gateway_client_certificate_options, count.index), "description")
}

resource "aws_api_gateway_rest_api" "this" {
  count       = "${local.api_gateway_rest_api_count}"
  name        = lookup(element(var.api_gateway_rest_api_options, count.index), "name")
  description = lookup(element(var.api_gateway_rest_api_options, count.index), "description")
}

resource "aws_api_gateway_resource" "this" {
  count       = "${local.api_gateway_resource_count}"
  rest_api_id = "${data.api_gateway_rest_api.resource.id}"
  parent_id   = "${data.api_gateway_rest_api.resource.root_resource_id}"
  path_part   = lookup(element(var.api_gateway_resource_options, count.index), "resource_path_part")
}

resource "aws_api_gateway_authorizer" "this" {
  count                            = "${local.api_gateway_authorizer_count}"
  name                             = lookup(element(var.api_gateway_authorizer_options, count.index), "name")
  rest_api_id                      = "${data.api_gateway_rest_api.authorizer.id}"
  authorizer_uri                   = lookup(element(var.api_gateway_authorizer_options, count.index), "authorizer_uri")
  authorizer_credentials           = lookup(element(var.api_gateway_authorizer_options, count.index), "authorizer_credentials")
  identity_source                  = lookup(element(var.api_gateway_authorizer_options, count.index), "identity_source")
  type                             = lookup(element(var.api_gateway_authorizer_options, count.index), "type")
  authorizer_result_ttl_in_seconds = lookup(element(var.api_gateway_authorizer_options, count.index), "authorizer_result_ttl_in_seconds")
  identity_validation_expression   = lookup(element(var.api_gateway_authorizer_options, count.index), "identity_validation_expression")
}

resource "aws_api_gateway_method" "this" {
  count                = "${local.api_gateway_method_count}"
  rest_api_id          = "${data.api_gateway_rest_api.method.id}"
  resource_id          = "${data.api_gateway_resource.method.id}"
  http_method          = lookup(element(var.api_gateway_method_options, count.index), "http_method")
  authorization        = lookup(element(var.api_gateway_method_options, count.index), "authorization")
  authorizer_id        = lookup(element(var.api_gateway_method_options, count.index), "authorizer_id")
  authorization_scopes = lookup(element(var.api_gateway_method_options, count.index), "authorization_scopes")
  api_key_required     = lookup(element(var.api_gateway_method_options, count.index), "api_key_required")
  request_models       = lookup(element(var.api_gateway_method_options, count.index), "request_models")
  request_validator_id = lookup(element(var.api_gateway_method_options, count.index), "request_validator_id")
  request_parameters   = lookup(element(var.api_gateway_method_options, count.index), "request_parameters")
}

resource "aws_api_gateway_documentation_part" "this" {
  count       = "${local.api_gateway_documentation_part_count}"
  properties  = file(lookup(element(var.api_gateway_documentation_part_options, count.index), "properties_json_file"))
  rest_api_id = "${data.api_gateway_rest_api.documentation_part.id}"

  dynamic "location" {
    for_each = [for location in lookup(element(var.api_gateway_documentation_part_options, count.index), "location") : {
      type        = location.type
      name        = location.name
      method      = location.method
      path        = location.path
      status_code = location.status_code
    }]

    content {
      type        = location.value.type
      name        = location.value.name
      method      = location.value.method
      path        = location.value.path
      status_code = location.value.status_code
    }
  }
}

resource "aws_api_gateway_documentation_version" "this" {
  count       = "${local.api_gateway_documentation_part_count}"
  version     = lookup(element(var.api_gateway_documentation_part_options, count.index), "version")
  rest_api_id = "${data.api_gateway_rest_api.documentation_part.id}"
  description = lookup(element(var.api_gateway_documentation_part_options, count.index), "description")
}

resource "aws_api_gateway_gateway_response" "this" {
  count               = "${local.api_gateway_gateway_response_count}"
  rest_api_id         = "${data.api_gateway_rest_api.gateway_response.id}"
  status_code         = lookup(element(var.api_gateway_gateway_response_options, count.index), "status_code")
  response_type       = lookup(element(var.api_gateway_gateway_response_options, count.index), "response_type")
  response_templates  = lookup(element(var.api_gateway_gateway_response_options, count.index), "response_templates")
  response_parameters = lookup(element(var.api_gateway_gateway_response_options, count.index), "response_parameters")
}

resource "aws_api_gateway_integration" "this" {
  count                   = "${local.api_gateway_integration_count}"
  rest_api_id             = "${data.api_gateway_rest_api.integration.id}"
  resource_id             = "${data.api_gateway_resource.integration.id}"
  http_method             = lookup(element(var.api_gateway_integration_options, count.index), "http_method")
  integration_http_method = lookup(element(var.api_gateway_integration_options, count.index), "integration_http_method")
  type                    = lookup(element(var.api_gateway_integration_options, count.index), "type")
  cache_key_parameters    = lookup(element(var.api_gateway_integration_options, count.index), "cache_key_parameters")
  cache_namespace         = lookup(element(var.api_gateway_integration_options, count.index), "cache_namespace")
  timeout_milliseconds    = lookup(element(var.api_gateway_integration_options, count.index), "timeout_milliseconds")
  connection_type         = lookup(element(var.api_gateway_integration_options, count.index), "connection_type")
  connection_id           = lookup(element(var.api_gateway_integration_options, count.index), "connection_id")
  uri                     = lookup(element(var.api_gateway_integration_options, count.index), "uri")
  credentials             = lookup(element(var.api_gateway_integration_options, count.index), "credentials")
  passthrough_behavior    = lookup(element(var.api_gateway_integration_options, count.index), "passthrough_behavior")
  content_handling        = lookup(element(var.api_gateway_integration_options, count.index), "content_handling")
  request_parameters      = lookup(element(var.api_gateway_integration_options, count.index), "request_parameters")
  request_templates       = lookup(element(var.api_gateway_integration_options, count.index), "request_templates")
}

resource "aws_api_gateway_deployment" "this" {
  count             = "${local.api_gateway_deployment_count}"
  rest_api_id       = "${data.api_gateway_rest_api.deployment.id}"
  stage_name        = lookup(element(var.api_gateway_deployment_options, count.index), "stage_name")
  description       = lookup(element(var.api_gateway_deployment_options, count.index), "description")
  stage_description = lookup(element(var.api_gateway_deployment_options, count.index), "stage_description")
  variables         = lookup(element(var.api_gateway_deployment_options, count.index), "variables")
}
