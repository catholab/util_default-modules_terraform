data "aws_api_gateway_rest_api" "resource" {
  count      = "${local.api_gateway_resource_count}"
  name       = lookup(element(var.api_gateway_resource_options, count.index), "rest_api_name")
  depends_on = ["aws_api_gateway_rest_api.this"]
}

data "aws_api_gateway_rest_api" "authorizer" {
  count      = "${local.api_gateway_authorizer_count}"
  name       = lookup(element(var.api_gateway_authorizer_options, count.index), "rest_api_name")
  depends_on = ["aws_api_gateway_rest_api.this"]
}

data "aws_api_gateway_rest_api" "method" {
  count      = "${local.api_gateway_method_count}"
  name       = lookup(element(var.api_gateway_method_options, count.index), "rest_api_name")
  depends_on = ["aws_api_gateway_rest_api.this"]
}

data "aws_api_gateway_rest_api" "documentation_part" {
  count      = "${local.api_gateway_documentation_part_count}"
  name       = lookup(element(var.api_gateway_documentation_part_options, count.index), "rest_api_name")
  depends_on = ["aws_api_gateway_rest_api.this"]
}

data "aws_api_gateway_rest_api" "gateway_response" {
  count      = "${local.api_gateway_gateway_response_count}"
  name       = lookup(element(var.api_gateway_gateway_response_options, count.index), "rest_api_name")
  depends_on = ["aws_api_gateway_rest_api.this"]
}

data "aws_api_gateway_rest_api" "integration" {
  count      = "${local.api_gateway_integration_count}"
  name       = lookup(element(var.api_gateway_integration_options, count.index), "rest_api_name")
  depends_on = ["aws_api_gateway_rest_api.this"]
}

data "aws_api_gateway_rest_api" "deployment" {
  count      = "${local.api_gateway_deployment_count}"
  name       = lookup(element(var.api_gateway_deployment_options, count.index), "rest_api_name")
  depends_on = ["aws_api_gateway_rest_api.this"]
}

data "aws_api_gateway_resource" "method" {
  count       = "${local.api_gateway_method_count}"
  rest_api_id = "${data.aws_api_gateway_rest_api.method.id}"
  path        = lookup(element(var.api_gateway_method_options, count.index), "resourcet_path")
  depends_on  = ["aws_api_gateway_resource.this"]
}

data "aws_api_gateway_resource" "integration" {
  count       = "${local.api_gateway_integration_count}"
  rest_api_id = "${data.aws_api_gateway_rest_api.integration.id}"
  path        = lookup(element(var.api_gateway_integration_options, count.index), "resourcet_path")
  depends_on  = ["aws_api_gateway_resource.this"]
}
