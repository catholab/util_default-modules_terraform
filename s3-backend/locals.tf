locals {
  default_tags = {
    Context                 = "${var.context}"
    Environment             = "${var.environment}"
    Team                    = "${var.team}"
    AppRelease              = "${var.app_release}"
    AppSource               = "${var.app_source}"
    ProvisioningTool        = "${var.provisioning_tool}"
    ProvisioningVersion     = "${var.provisioning_version}"
    ProvisioningSource      = "${var.provisioning_source}"
    DeploymentTool          = "${var.deployment_tool}"
    DeploymentBuildName     = "${var.deployment_build_name}"
    DeploymentBuildNumber   = "${var.deployment_build_nr}"
    DeploymentLastExecution = formatdate("DD/MM/YYYY - hh:mm:ss - ZZZ", timestamp())
  }

  s3_count = var.s3_create == true ? 1 : 0
  s3_object_count = var.s3_object_create == true ? length(var.s3_object_options) : 0
}
