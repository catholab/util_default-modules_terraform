## DYNAMODB TABLE RESOURCE ##
resource "aws_dynamodb_table" "this" {
  count            = "${local.dynamodb_table_count}"
  name             = lookup(element(var.dynamodb_table_options, count.index), "name")
  billing_mode     = lookup(element(var.dynamodb_table_options, count.index), "billing_mode")
  read_capacity    = lookup(element(var.dynamodb_table_options, count.index), "read_capacity")
  write_capacity   = lookup(element(var.dynamodb_table_options, count.index), "write_capacity")
  hash_key         = lookup(element(var.dynamodb_table_options, count.index), "hash_key")
  range_key        = lookup(element(var.dynamodb_table_options, count.index), "range_key")
  stream_enabled   = lookup(element(var.dynamodb_table_options, count.index), "stream_enabled")
  stream_view_type = lookup(element(var.dynamodb_table_options, count.index), "stream_view_type")

  dynamic "attribute" {
    for_each = [for attribute in lookup(element(var.dynamodb_table_options, count.index), "attribute") : {
      name = attribute.name
      type = attribute.type
    }]

    content {
      name = attribute.value.name
      type = attribute.value.type
    }
  }

  dynamic "ttl" {
    for_each = lookup(element(var.dynamodb_table_options, count.index), "create_ttl") == false ? [] : [for ttl in lookup(element(var.dynamodb_table_options, count.index), "ttl") : {
      attribute_name = ttl.attribute_name
      enabled        = ttl.enabled
    }]

    content {
      attribute_name = ttl.value.attribute_name
      enabled        = ttl.value.enabled
    }
  }

  dynamic "local_secondary_index" {
    for_each = lookup(element(var.dynamodb_table_options, count.index), "create_local_secondary_index") == false ? [] : [for local_secondary_index in lookup(element(var.dynamodb_table_options, count.index), "local_secondary_index") : {
      name               = local_secondary_index.name
      range_key          = local_secondary_index.range_key
      projection_type    = local_secondary_index.projection_type
      non_key_attributes = local_secondary_index.non_key_attributes
    }]

    content {
      name               = local_secondary_index.value.name
      range_key          = local_secondary_index.value.range_key
      projection_type    = local_secondary_index.value.projection_type
      non_key_attributes = local_secondary_index.value.non_key_attributes
    }
  }

  dynamic "global_secondary_index" {
    for_each = lookup(element(var.dynamodb_table_options, count.index), "create_global_secondary_index") == false ? [] : [for global_secondary_index in lookup(element(var.dynamodb_table_options, count.index), "global_secondary_index") : {
      name               = global_secondary_index.name
      hash_key           = global_secondary_index.hash_key
      range_key          = global_secondary_index.range_key
      write_capacity     = global_secondary_index.write_capacity
      read_capacity      = global_secondary_index.read_capacity
      projection_type    = global_secondary_index.projection_type
      non_key_attributes = global_secondary_index.non_key_attributes
    }]

    content {
      name               = global_secondary_index.value.name
      hash_key           = global_secondary_index.value.hash_key
      range_key          = global_secondary_index.value.range_key
      write_capacity     = global_secondary_index.value.write_capacity
      read_capacity      = global_secondary_index.value.read_capacity
      projection_type    = global_secondary_index.value.projection_type
      non_key_attributes = global_secondary_index.value.non_key_attributes
    }
  }

  dynamic "server_side_encryption" {
    for_each = lookup(element(var.dynamodb_table_options, count.index), "create_server_side_encryption") == false ? [] : [for server_side_encryption in lookup(element(var.dynamodb_table_options, count.index), "server_side_encryption") : {
      enabled = server_side_encryption.enabled
    }]

    content {
      enabled = server_side_encryption.value.enabled
    }
  }

  dynamic "point_in_time_recovery" {
    for_each = lookup(element(var.dynamodb_table_options, count.index), "create_point_in_time_recovery") == false ? [] : [for point_in_time_recovery in lookup(element(var.dynamodb_table_options, count.index), "point_in_time_recovery") : {
      enabled = point_in_time_recovery.enabled
    }]

    content {
      enabled = point_in_time_recovery.value.enabled
    }
  }
  tags = merge(
    {
      Name = lookup(element(var.dynamodb_table_options, count.index), "name"),
    },
    "${local.default_tags}",
    lookup(element(var.dynamodb_table_options, 0), "extraTags")
  )
}

## DYNAMODB GLOBAL TABLE RESOURCE ##
resource "aws_dynamodb_global_table" "this" {
  count = "${local.dynamodb_global_count}"
  name  = lookup(element(var.dynamodb_global_options, count.index), "name")

  dynamic "replica" {
    iterator = replica
    for_each = lookup(element(var.dynamodb_global_options, count.index), "replica")
    content {
      region_name = replica.value
    }
  }

  depends_on = ["aws_dynamodb_table.this"]
}

## DYNAMODB ITEM TABLE RESOURCE ##
resource "aws_dynamodb_table_item" "this" {
  count      = "${local.dynamodb_item_count}"
  table_name = "${data.aws_dynamodb_table.this[count.index].name}"
  hash_key   = "${data.aws_dynamodb_table.this[count.index].hash_key}"
  range_key  = "${data.aws_dynamodb_table.this[count.index].range_key}"

  item       = file(lookup(element(var.dynamodb_item_options, count.index), "dynamodb_table_item_json_path"))
  depends_on = ["data.aws_dynamodb_table.this"]
}
