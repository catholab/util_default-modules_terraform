## DYNAMODB TABLE OUTPUT ##
output "dynamodb_table_id" {
  value       = "${aws_dynamodb_table.this[*].id}"
  description = "The name of the table"
}

output "dynamodb_table_arn" {
  value       = "${aws_dynamodb_table.this[*].arn}"
  description = "The arn of the table"
}

output "dynamodb_table_stream_arn" {
  value       = "${aws_dynamodb_table.this[*].stream_arn}"
  description = "The ARN of the Table Stream"
}

output "dynamodb_table_stream_label" {
  value       = "${aws_dynamodb_table.this[*].stream_label}"
  description = "A timestamp, in ISO 8601 format, for this stream"
}

## DYNAMODB GLOBAL TABLE OUTPUT ##
output "dynamodb_global_id" {
  value       = "${aws_dynamodb_global_table.this[*].id}"
  description = "The name of the global table"
}

output "dynamodb_global_arn" {
  value       = "${aws_dynamodb_global_table.this[*].arn}"
  description = "The arn of the global table"
}

## DYNAMODB ITEM TABLE OUTPUT ##
output "dynamodb_item_table_name" {
  value       = "${aws_dynamodb_table_item.this[*].table_name}"
  description = "The name of the table"
}

output "dynamodb_item" {
  value       = "${aws_dynamodb_table_item.this[*].item}"
  description = "The item of the table"
}
