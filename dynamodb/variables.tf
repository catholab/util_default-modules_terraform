## GLOBAL VARIABLES ##
variable "context" {
  type = "string"
}

variable "environment" {
  type = "string"
}

variable "team" {
  type = "string"
}

variable "app_release" {
  type = "string"
}

variable "app_source" {
  type = "string"
}

variable "provisioning_tool" {
  type = "string"
}

variable "provisioning_version" {
  type = "string"
}

variable "provisioning_source" {
  type = "string"
}

variable "deployment_tool" {
  type = "string"
}

variable "deployment_build_name" {
  type = "string"
}

variable "deployment_build_nr" {
  type = "string"
}

## DYNAMODB TABLE VARIABLES ##
variable "dynamodb_table_create" {
  type = bool
}

variable "dynamodb_table_options" {
  type    = "list"
  default = []
}

## DYNAMODB GLOBAL TABLE VARIABLES ##
variable "dynamodb_global_create" {
  type = bool
}

variable "dynamodb_global_options" {
  type    = "list"
  default = []
}

## DYNAMODB ITEM TABLE VARIABLES ##
variable "dynamodb_item_create" {
  type = bool
}

variable "dynamodb_item_options" {
  type    = "list"
  default = []
}
