data "aws_dynamodb_table" "this" {
  count = "${local.dynamodb_item_count}"
  name  = lookup(element(var.dynamodb_item_options, count.index), "table_name")
}
