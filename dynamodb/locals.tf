locals {
  default_tags = {
    Context                 = "${var.context}"
    Environment             = "${var.environment}"
    Team                    = "${var.team}"
    AppRelease              = "${var.app_release}"
    AppSource               = "${var.app_source}"
    ProvisioningTool        = "${var.provisioning_tool}"
    ProvisioningVersion     = "${var.provisioning_version}"
    ProvisioningSource      = "${var.provisioning_source}"
    DeploymentTool          = "${var.deployment_tool}"
    DeploymentBuildName     = "${var.deployment_build_name}"
    DeploymentBuildNumber   = "${var.deployment_build_nr}"
    DeploymentLastExecution = formatdate("DD/MM/YYYY - hh:mm:ss - ZZZ", timestamp())
  }

  dynamodb_table_count  = var.dynamodb_table_create == true ? length(var.dynamodb_table_options) : 0
  dynamodb_global_count = var.dynamodb_global_create == true ? length(var.dynamodb_global_options) : 0
  dynamodb_item_count   = var.dynamodb_item_create == true ? length(var.dynamodb_item_options) : 0
}
