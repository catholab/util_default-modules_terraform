locals {
  default_tags = {
    Context                 = "${var.context}"
    Environment             = "${var.environment}"
    Team                    = "${var.team}"
    AppRelease              = "${var.app_release}"
    AppSource               = "${var.app_source}"
    ProvisioningTool        = "${var.provisioning_tool}"
    ProvisioningVersion     = "${var.provisioning_version}"
    ProvisioningSource      = "${var.provisioning_source}"
    DeploymentTool          = "${var.deployment_tool}"
    DeploymentBuildName     = "${var.deployment_build_name}"
    DeploymentBuildNumber   = "${var.deployment_build_nr}"
    DeploymentLastExecution = formatdate("DD/MM/YYYY - hh:mm:ss - ZZZ", timestamp())
  }

  r53_resolver_endpoint_inbound_count  = var.r53_resolver_endpoint_inbound_create == true ? length(var.r53_resolver_endpoint_inbound_options) : 0
  r53_resolver_endpoint_outbound_count = var.r53_resolver_endpoint_outbound_create == true ? length(var.r53_resolver_endpoint_outbound_options) : 0
}
