resource "aws_route53_resolver_endpoint" "inbound" {
  count              = "${local.r53_resolver_endpoint_inbound_count}"
  name               = lookup(element(var.r53_resolver_endpoint_inbound_options, count.index), "name")
  direction          = "INBOUND"
  security_group_ids = lookup(element(var.r53_resolver_endpoint_inbound_options, count.index), "security_group_ids")

  dynamic "ip_address" {
    iterator = subnet
    for_each = lookup(element(var.r53_resolver_endpoint_inbound_options, count.index), "subnet_ids")
    content {
      subnet_id = subnet.value
    }
  }

  tags = merge(
    {
      Name = lookup(element(var.r53_resolver_endpoint_inbound_options, count.index), "name")
    },
    "${local.default_tags}",
    lookup(element(var.r53_resolver_endpoint_inbound_options, count.index), "extraTags")
  )
}

resource "aws_route53_resolver_endpoint" "outbound" {
  count              = "${local.r53_resolver_endpoint_outbound_count}"
  name               = lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "name")
  direction          = "OUTBOUND"
  security_group_ids = lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "security_group_ids")

  dynamic "ip_address" {
    iterator = subnet
    for_each = lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "subnet_ids")
    content {
      subnet_id = subnet.value
    }
  }

  tags = merge(
    {
      Name = lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "name")
    },
    "${local.default_tags}",
    lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "extraTags")
  )
}

resource "aws_route53_resolver_rule" "this" {
  count                = "${local.r53_resolver_endpoint_outbound_count}"
  domain_name          = lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "domain_name")
  name                 = lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "rule_name")
  rule_type            = lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "rule_type")
  resolver_endpoint_id = "${aws_route53_resolver_endpoint.outbound[count.index].id}"

  dynamic "target_ip" {
    for_each = lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "rule_type") != "FORWARD" ? [] : [for target_ip in lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "target_ip") : {
      ip   = target_ip.ip
      port = target_ip.port
    }]

    content {
      ip   = target_ip.value.ip
      port = target_ip.value.port
    }
  }

  tags = merge(
    {
      Name = lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "name")
    },
    "${local.default_tags}",
    lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "extraTags")
  )
}

resource "aws_route53_resolver_rule_association" "outbound" {
  count            = "${local.r53_resolver_endpoint_outbound_count}"
  resolver_rule_id = "${aws_route53_resolver_rule.this[count.index].id}"
  vpc_id           = lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "vpc_id")
}

