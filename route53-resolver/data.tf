/*
data "aws_vpc" "this" {
  tags = {
    Name = format("vpc-%s", var.context)
  }
}

data "aws_subnet_ids" "inbound" {
  count  = "${local.r53_resolver_endpoint_inbound_count}"
  vpc_id = "${data.aws_vpc.this.id}"
  filter {
    name   = "tag:Name"
    values = lookup(element(var.r53_resolver_endpoint_inbound_options, count.index), "subnet_names")
  }
}

data "aws_security_groups" "inbound" {
  count = "${local.r53_resolver_endpoint_inbound_count}"
  filter {
    name   = "tag:Name"
    values = lookup(element(var.r53_resolver_endpoint_inbound_options, count.index), "security_group_names")
  }
}

data "aws_subnet_ids" "outbound" {
  count  = "${local.r53_resolver_endpoint_outbound_count}"
  vpc_id = "${data.aws_vpc.this.id}"
  filter {
    name   = "tag:Name"
    values = lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "subnet_names")
  }
}

data "aws_security_groups" "outbound" {
  count = "${local.r53_resolver_endpoint_outbound_count}"
  filter {
    name   = "tag:Name"
    values = lookup(element(var.r53_resolver_endpoint_outbound_options, count.index), "security_group_names")
  }
}
*/
