data "aws_iam_role" "this" {
  count = "${local.lambda_function_count}"
  name  = lookup(element(var.lambda_function_options, count.index), "iam_role_name")
}

data "aws_vpc" "this" {
  tags = {
    Name = format("vpc-%s", var.context)
  }
}

data "aws_subnet_ids" "this" {
  count  = "${local.lambda_function_count}"
  vpc_id = "${data.aws_vpc.this.id}"
  filter {
    name   = "tag:Name"
    values = lookup(element(var.lambda_function_options, count.index), "subnet_names")
  }
}

data "aws_security_groups" "this" {
  count = "${local.lambda_function_count}"
  filter {
    name   = "tag:Name"
    values = lookup(element(var.lambda_function_options, count.index), "security_group_names")
  }
}
