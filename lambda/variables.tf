## GLOBAL VARIABLES ##
variable "context" {
  type = "string"
}

variable "environment" {
  type = "string"
}

variable "team" {
  type = "string"
}

variable "app_release" {
  type = "string"
}

variable "app_source" {
  type = "string"
}

variable "provisioning_tool" {
  type = "string"
}

variable "provisioning_version" {
  type = "string"
}

variable "provisioning_source" {
  type = "string"
}

variable "deployment_tool" {
  type = "string"
}

variable "deployment_build_name" {
  type = "string"
}

variable "deployment_build_nr" {
  type = "string"
}

variable "lambda_function_create" {
  type = bool
}

variable "lambda_function_options" {
  type    = "list"
  default = []
}

variable "lambda_alias_create" {
  type = bool
}

variable "lambda_alias_options" {
  type    = "list"
  default = []
}

variable "lambda_permission_create" {
  type = bool
}

variable "lambda_permission_options" {
  type    = "list"
  default = []
}

variable "lambda_event_source_mapping_create" {
  type = bool
}

variable "lambda_event_source_mapping_options" {
  type    = "list"
  default = []
}
