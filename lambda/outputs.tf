output "lambda_function_arn" {
  value       = "${aws_lambda_function.this[*].arn}"
  description = "The Amazon Resource Name (ARN) identifying your Lambda Function."
}

output "lambda_function_qualified_arn" {
  value       = "${aws_lambda_function.this[*].qualified_arn}"
  description = "The Amazon Resource Name (ARN) identifying your Lambda Function Version (if versioning is enabled via publish = true)."
}

output "lambda_function_invoke_arn" {
  value       = "${aws_lambda_function.this[*].invoke_arn}"
  description = "The ARN to be used for invoking Lambda Function from API Gateway - to be used in aws_api_gateway_integration's uri."
}

output "lambda_function_version" {
  value       = "${aws_lambda_function.this[*].version}"
  description = "Latest published version of your Lambda Function."
}

output "lambda_function_last_modified" {
  value       = "${aws_lambda_function.this[*].last_modified}"
  description = "The date this resource was last modified."
}

output "lambda_function_source_code_hash" {
  value       = "${aws_lambda_function.this[*].source_code_hash}"
  description = "Base64-encoded representation of raw SHA-256 sum of the zip file, provided either via filename or s3_* parameters."
}

output "lambda_function_source_code_size" {
  value       = "${aws_lambda_function.this[*].source_code_size}"
  description = "The size in bytes of the function .zip file."
}

output "lambda_alias_arn" {
  value       = "${aws_lambda_alias.this[*].arn}"
  description = "The Amazon Resource Name (ARN) identifying your Lambda function alias."
}

output "lambda_alias_invoke_arn" {
  value       = "${aws_lambda_alias.this[*].invoke_arn}"
  description = "The ARN to be used for invoking Lambda Function from API Gateway - to be used in aws_api_gateway_integration's uri."
}

