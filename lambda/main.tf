resource "aws_lambda_function" "this" {
  count                          = "${local.lambda_function_count}"
  function_name                  = lookup(element(var.lambda_function_options, count.index), "name")
  runtime                        = lookup(element(var.lambda_function_options, count.index), "runtime")
  handler                        = lookup(element(var.lambda_function_options, count.index), "handler")
  role                           = "${data.aws_iam_role.this[count.index].arn}"
  filename                       = lookup(element(var.lambda_function_options, count.index), "file_location") == "local" ? lookup(element(var.lambda_function_options, count.index), "filename") : null
  source_code_hash               = lookup(element(var.lambda_function_options, count.index), "file_location") == "local" ? filebase64sha256(lookup(element(var.lambda_function_options, count.index), "filename")) : null
  s3_bucket                      = lookup(element(var.lambda_function_options, count.index), "file_location") == "s3" ? lookup(element(var.lambda_function_options, count.index), "s3_bucket") : null
  s3_key                         = lookup(element(var.lambda_function_options, count.index), "file_location") == "s3" ? lookup(element(var.lambda_function_options, count.index), "s3_key") : null
  s3_object_version              = lookup(element(var.lambda_function_options, count.index), "file_location") == "s3" ? lookup(element(var.lambda_function_options, count.index), "s3_object_version") : null
  memory_size                    = lookup(element(var.lambda_function_options, count.index), "memory_size")
  timeout                        = lookup(element(var.lambda_function_options, count.index), "timeout")
  reserved_concurrent_executions = lookup(element(var.lambda_function_options, count.index), "reserved_concurrent_executions")
  publish                        = lookup(element(var.lambda_function_options, count.index), "publish")
  description                    = lookup(element(var.lambda_function_options, count.index), "description")

  dynamic "dead_letter_config" {
    for_each = lookup(element(var.lambda_function_options, count.index), "create_dead_letter_config") == false ? [] : [for dead_letter_config in lookup(element(var.lambda_function_options, count.index), "dead_letter_config") : {
      target_arn = dead_letter_config.target_arn
    }]

    content {
      target_arn = dead_letter_config.target_arn
    }
  }

  dynamic "tracing_config" {
    for_each = lookup(element(var.lambda_function_options, count.index), "create_tracing_config") == false ? [] : [for tracing_config in lookup(element(var.lambda_function_options, count.index), "tracing_config") : {
      mode = tracing_config.mode
    }]

    content {
      mode = tracing_config.value.mode
    }
  }

  vpc_config {
    subnet_ids         = "${data.aws_subnet_ids.this[count.index].ids}"
    security_group_ids = "${data.aws_security_groups.this[count.index].ids}"
  }

  environment {
    variables = lookup(element(var.lambda_function_options, count.index), "environment_variables")
  }

}

resource "aws_lambda_alias" "this" {
  count            = "${local.lambda_alias_count}"
  name             = lookup(element(var.lambda_alias_options, count.index), "name")
  description      = lookup(element(var.lambda_alias_options, count.index), "description")
  function_name    = lookup(element(var.lambda_alias_options, count.index), "function_name")
  function_version = lookup(element(var.lambda_alias_options, count.index), "function_version")
}


resource "aws_lambda_permission" "this" {
  count          = "${local.lambda_permission_count}"
  action         = lookup(element(var.lambda_permission_options, count.index), "action")
  function_name  = lookup(element(var.lambda_permission_options, count.index), "function_name")
  principal      = lookup(element(var.lambda_permission_options, count.index), "principal")
  source_arn     = lookup(element(var.lambda_permission_options, count.index), "source_arn")
  source_account = lookup(element(var.lambda_permission_options, count.index), "source_account")
}

resource "aws_lambda_event_source_mapping" "this" {
  count                              = "${local.lambda_event_source_mapping_count}"
  event_source_arn                   = lookup(element(var.lambda_event_source_mapping_options, count.index), "event_source_arn")
  function_name                      = lookup(element(var.lambda_event_source_mapping_options, count.index), "function_name")
  enabled                            = lookup(element(var.lambda_event_source_mapping_options, count.index), "enabled")
  batch_size                         = lookup(element(var.lambda_event_source_mapping_options, count.index), "batch_size")
}