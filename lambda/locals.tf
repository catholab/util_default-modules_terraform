locals {
  default_tags = {
    Context                 = "${var.context}"
    Environment             = "${var.environment}"
    Team                    = "${var.team}"
    AppRelease              = "${var.app_release}"
    AppSource               = "${var.app_source}"
    ProvisioningTool        = "${var.provisioning_tool}"
    ProvisioningVersion     = "${var.provisioning_version}"
    ProvisioningSource      = "${var.provisioning_source}"
    DeploymentTool          = "${var.deployment_tool}"
    DeploymentBuildName     = "${var.deployment_build_name}"
    DeploymentBuildNumber   = "${var.deployment_build_nr}"
    DeploymentLastExecution = formatdate("DD/MM/YYYY - hh:mm:ss - ZZZ", timestamp())
  }

  lambda_function_count   = var.lambda_function_create == true ? length(var.lambda_function_options) : 0
  lambda_alias_count      = var.lambda_alias_create == true ? length(var.lambda_alias_options) : 0
  lambda_permission_count = var.lambda_permission_create == true ? length(var.lambda_permission_options) : 0
  lambda_event_source_mapping_count = var.lambda_event_source_mapping_create == true ? length(var.lambda_event_source_mapping_options) : 0
}
