terraform {
  backend "s3" {
    bucket                  = "infra-lab-terraform-state"
    key                     = "client-vpn/terraform.tfstate"
    region                  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "catho-infra"
  }
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "${var.profile}"
}

module "client-vpn" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//client-vpn"


  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  ec2_client_vpn_endpoint_create = true
  ec2_client_vpn_endpoint_options = list(
    {
      name                   = "client-vpn-uoldiveo-virginia",
      description            = "client-vpn-uoldiveo-virginia",
      server_certificate_arn = "${data.aws_acm_certificate.this.arn}",
      client_cidr_block      = "172.16.250.0/22",
      dns_servers = list(
        "10.250.10.113",
        "10.250.10.129",
      )
      split_tunnel                   = false,
      transport_protocol             = "udp",
      authentication_options_type    = "certificate-authentication",
      root_certificate_chain_arn     = "${data.aws_acm_certificate.this.arn}",
      active_directory_id            = "",
      connection_log_options_enabled = false,
      cloudwatch_log_group           = "",
      cloudwatch_log_stream          = "",
      subnet_names = list(
        "UTILS-ZONE-A",
        "UTILS-ZONE-B",
      )
      extraTags = {
      }
    },
  )
}
