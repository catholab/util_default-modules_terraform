output "ec2_client_vpn_endpoint_id" {
  value       = "${module.client-vpn.ec2_client_vpn_endpoint_id}"
  description = "The ID of the Client VPN endpoint."
}

output "ec2_client_vpn_endpoint_dns_name" {
  value       = "${module.client-vpn.ec2_client_vpn_endpoint_dns_name}"
  description = "The DNS name to be used by clients when establishing their VPN session."
}

output "ec2_client_vpn_endpoint_status" {
  value       = "${module.client-vpn.ec2_client_vpn_endpoint_status}"
  description = "The current state of the Client VPN endpoint."
}

output "ec2_client_vpn_network_association_id" {
  value       = "${module.client-vpn.ec2_client_vpn_network_association_id}"
  description = "The unique ID of the target network association."
}

output "ec2_client_vpn_network_association_status" {
  value       = "${module.client-vpn.ec2_client_vpn_network_association_status}"
  description = "The current state of the target network association."
}
