# AWS [`CLIENT VPN ENDPOINT`](https://aws.amazon.com/vpn/) Terraform module example

Terraform module which creates the following resources on AWS:
* CLIENT VPN ENDPOINT

These types of resources are supported:

* [aws_ec2_client_vpn_endpoint](https://www.terraform.io/docs/providers/aws/r/ec2_client_vpn_endpoint.html)

## Terraform version
- This module was written in terraform version 0.12. For more details, see this [page](https://www.hashicorp.com/blog/announcing-terraform-0-12).

## Usage

```hcl
module "client-vpn" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//client-vpn"


  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  ec2_client_vpn_endpoint_create = true
  ec2_client_vpn_endpoint_options = list(
    {
      name                   = "client-vpn-uoldiveo-virginia",
      description            = "client-vpn-uoldiveo-virginia",
      server_certificate_arn = "${data.aws_acm_certificate.this.arn}",
      client_cidr_block      = "172.16.250.0/22",
      dns_servers = list(
        "10.250.10.113",
        "10.250.10.129",
      )
      split_tunnel                   = false,
      transport_protocol             = "udp",
      authentication_options_type    = "certificate-authentication",
      root_certificate_chain_arn     = "${data.aws_acm_certificate.this.arn}",
      active_directory_id            = "",
      connection_log_options_enabled = false,
      cloudwatch_log_group           = "",
      cloudwatch_log_stream          = "",
      subnet_names = list(
        "UTILS-ZONE-A",
        "UTILS-ZONE-B",
      )
      extraTags = {
      }
    },
  )
}
```

## Global Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| context | Name of team context. | string | "" | yes |
| environment | Name of environment. | string | "" | yes |
| team | Name of team. | string | "" | yes |
| app_release | Release of application. | string | "" | yes |
| app_source | Link of application git repository. | string | "" | yes |
| provisioning_tool | Name of provisioning tool. | string | "" | yes |
| provisioning_version | Version of provisioning tool. | string | "" | yes |
| provisioning_source | Link of provisioning tool git repository. | string | "" | yes |
| deployment_tool | Name of deployment tool. | string | "" | yes |
| deployment_build_name | Name of deployment build. | string | "" | yes |
| deployment_build_nr | Number of deployment build. | string | "" | yes |

---

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| ec2_client_vpn_endpoint_create | Boolean to create or not the Client VPN Endpoint resource. | bool | - | yes |
| [ec2_client_vpn_endpoint_options](#ec2_client_vpn_endpoint_options) | List with the arguments to create the Client VPN Endpoint resource. | list(map) | [] | yes |

---

### Arguments
#### **ec2_client_vpn_endpoint_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | Name of the repository. | string | "" | yes |
| description | Name of the repository. | string | "" | yes |
| server_certificate_arn | The ARN of the ACM server certificate. | string | "" | yes |
| client_cidr_block | The IPv4 address range, in CIDR notation, from which to assign client IP addresses. **The address range cannot overlap with the local CIDR of the VPC in which the associated subnet is located, or the routes that you add manually**. **The address range cannot be changed after the Client VPN endpoint has been created**. **The CIDR block should be ```/22``` or greater**. | string | "" | yes |
| dns_servers | Information about the DNS servers to be used for DNS resolution. A Client VPN endpoint can have up to two DNS servers. **If no DNS server is specified, the DNS address of the VPC that is to be associated with Client VPN endpoint is used as the DNS server**. | string | "" | yes |
| split_tunnel | Indicates whether split-tunnel is enabled on VPN endpoint. | bool | - | yes |
| authentication_options_type | The type of client authentication to be used. Specify **```certificate-authentication```** to use certificate-based authentication, or **```directory-service-authentication```** to use Active Directory authentication. | string | "" | yes |
| root_certificate_chain_arn | The ARN of the client certificate. The certificate must be signed by a certificate authority (CA) and it must be provisioned in AWS Certificate Manager (ACM). **Only necessary when type is set to ```certificate-authentication```**. | string | "" | no |
| active_directory_id | The ID of the Active Directory to be used for authentication **if type is ```directory-service-authentication```**. | string | "" | no |
| connection_log_options_enabled | Indicates whether connection logging is enabled. | bool | - | yes |
| cloudwatch_log_group | The name of the CloudWatch Logs log group. | string | "" | no |
| cloudwatch_log_stream | The name of the CloudWatch Logs log stream to which the connection data is published. | string | "" | no |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

---

## Outputs

| Name | Description |
|------|-------------|
| module.client-vpn.ec2_client_vpn_endpoint_id | The ID of the Client VPN endpoint. |
| module.client-vpn.ec2_client_vpn_endpoint_dns_name | The DNS name to be used by clients when establishing their VPN session. |
| module.client-vpn.ec2_client_vpn_endpoint_status | The current state of the Client VPN endpoint. |
| module.client-vpn.ec2_client_vpn_network_association_id | The unique ID of the target network association. |
| module.client-vpn.ec2_client_vpn_network_association_status | The current state of the target network association. |
