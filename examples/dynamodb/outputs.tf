## DYNAMODB TABLE OUTPUT ##
output "dynamodb_table_id" {
  value       = "${module.dynamodb.dynamodb_table_id}"
  description = "The name of the table"
}

output "dynamodb_table_arn" {
  value       = "${module.dynamodb.dynamodb_table_arn}"
  description = "The arn of the table"
}

output "dynamodb_table_stream_arn" {
  value       = "${module.dynamodb.dynamodb_table_stream_arn}"
  description = "The ARN of the Table Stream"
}

output "dynamodb_table_stream_label" {
  value       = "${module.dynamodb.dynamodb_table_stream_label}"
  description = "A timestamp, in ISO 8601 format, for this stream"
}

## DYNAMODB GLOBAL TABLE OUTPUT ##
output "dynamodb_global_id" {
  value       = "${module.dynamodb.dynamodb_global_id}"
  description = "The name of the global table"
}

output "dynamodb_global_arn" {
  value       = "${module.dynamodb.dynamodb_global_arn}"
  description = "The arn of the global table"
}

## DYNAMODB ITEM TABLE OUTPUT ##
output "dynamodb_item_table_name" {
  value       = "${module.dynamodb.dynamodb_item_table_name}"
  description = "The name of the table"
}

output "dynamodb_item" {
  value       = "${module.dynamodb.dynamodb_item}"
  description = "The item of the table"
}
