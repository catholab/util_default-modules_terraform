# AWS [`DYNAMODB`](https://aws.amazon.com/dynamodb/) Terraform module example

Terraform module which creates the following resources on AWS:
* DynamoDB Table
* DynamoDB Global Table
* DynamoDB Table Item

These types of resources are supported:

* [aws_dynamodb_table](https://www.terraform.io/docs/providers/aws/r/dynamodb_table.html)
* [aws_dynamodb_global_table](https://www.terraform.io/docs/providers/aws/r/dynamodb_global_table.html)
* [aws_dynamodb_table_item](https://www.terraform.io/docs/providers/aws/r/dynamodb_table_item.html)

## Terraform version
- This module was written in terraform version 0.12. For more details, see this [page](https://www.hashicorp.com/blog/announcing-terraform-0-12).

## Usage

```hcl
module "dynamodb" {
  source                = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//dynamodb"

  ## GLOBAL OPTIONS ##
  context = "infra"
  environment = "lab"
  team = "infra"
  app_release = "1.0.1"
  app_source = "https://github.com/teste/teste.git"
  provisioning_tool = "terraform"
  provisioning_version = "0.12.6"
  provisioning_source = "https://github.com/teste/teste.git"
  deployment_tool = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr = "23"

  ## DYNAMODB TABLE OPTIONS ##
  dynamodb_table_create		= true
  dynamodb_table_options	= list(
				    {
				      name = "GameScores",
				      billing_mode = "PROVISIONED",
				      read_capacity = 20,
				      write_capacity = 20,
				      hash_key = "UserId",
				      range_key = "GameTitle",
				      stream_enabled = false,
				      stream_view_type = "",
				      attribute = list(
				        {
				          name = "UserId",
				          type = "S",
				        },
				        {
				          name = "GameTitle",
				          type = "S",
				        },
				      )
				      create_ttl = false
				      ttl = list(
				        {
	  			          attribute_name = "TimeToExist",
					  enabled = false,
				        },
				      )
				      create_local_secondary_index = false
				      local_secondary_index = list(
				        {
	  			          name = "GameTitleIndex",
					  range_key = "TopScore",
					  projection_type = "INCLUDE",
					  non_key_attributes = ["UserId"],
				        },
				      )
				      create_global_secondary_index = false
				      global_secondary_index = list(
				        {
	  			          name = "GameTitleIndex",
					  hash_key = "GameTitle",
					  range_key = "TopScore",
					  write_capacity = 10,
					  read_capacity = 10,
					  projection_type = "INCLUDE",
					  non_key_attributes = ["UserId"],
				        },
				      )
				      create_server_side_encryption = false
				      server_side_encryption = list(
				        {
					  enabled = false,
				        },
				      )
				      create_point_in_time_recovery = false
				      point_in_time_recovery = list(
				        {
					  enabled = false,
				        },
				      )
				      role = "table",
				      environment = "production"
				      extraTags = {
				      }
				    },
			  )

  ## DYNAMODB GLOBAL TABLE OPTIONS ##
  dynamodb_global_create	= true
  dynamodb_global_options	= list(
				    {
				      name = "GameScores",
				      replica = list(
				        "us-east-1",
				      )
				      role = "global",
				      environment = "production"
				      extraTags = {
				      }
				    },
			  )

  ## DYNAMODB ITEM TABLE OPTIONS ##
  dynamodb_item_create		= true
  dynamodb_item_options		= list(
				    {
				      table_name = "GameScores",
				      dynamodb_table_item_json_path = "item.json",
				    },
			  )

}
```

#### item.json 

```hcl
{
  "exampleHashKey": {"S": "something"},
  "one": {"N": "11111"},
  "two": {"N": "22222"},
  "three": {"N": "33333"},
  "four": {"N": "44444"}
}
```

## Global Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| context | Name of team context. | string | "" | yes |
| environment | Name of environment. | string | "" | yes |
| team | Name of team. | string | "" | yes |
| app_release | Release of application. | string | "" | yes |
| app_source | Link of application git repository. | string | "" | yes |
| provisioning_tool | Name of provisioning tool. | string | "" | yes |
| provisioning_version | Version of provisioning tool. | string | "" | yes |
| provisioning_source | Link of provisioning tool git repository. | string | "" | yes |
| deployment_tool | Name of deployment tool. | string | "" | yes |
| deployment_build_name | Name of deployment build. | string | "" | yes |
| deployment_build_nr | Number of deployment build. | string | "" | yes |

---

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| dynamodb_table_create | Boolean to create or not the DynamoDB Table resource. | bool | - | yes |
| [dynamodb_table_options](#dynamodb_table_options) | List with the arguments to create the DynamoDB Table resource. | list(map) | [] | yes |
| dynamodb_global_create | Boolean to create or not the DynamoDB Global Table resource. | bool | - | yes |
| [dynamodb_global_options](#dynamodb_global_options) | List with the arguments to create the DynamoDB Global Table resource. | list(map) | [] | yes |
| dynamodb_item_create | Boolean to create or not the DynamoDB Table Item resource. | bool | - | yes |
| [dynamodb_item_options](#dynamodb_item_options) | List with the arguments to create the DynamoDB Table Item resource. | list(map) | [] | yes |

---

### Arguments
#### **dynamodb_table_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The name of the table, this needs to be unique within a region. | string | "" | yes |
| billing_mode | Controls how you are charged for read and write throughput and how you manage capacity. The valid values are **```PROVISIONED```** and **```PAY_PER_REQUEST```**. | string | "" | yes |
| read_capacity | The number of read units for this table. **If the billing_mode is ```PROVISIONED```, this field is required**. | number | - | no |
| write_capacity | The number of write units for this table. **If the billing_mode is ```PROVISIONED```, this field is required**.  | number | - | no |
| hash_key | The attribute to use as the hash (partition) key. **Must also be defined as an ```attribute```**, see below. | string | "" | yes |
| range_key | The attribute to use as the range (sort) key. **Must also be defined as an ```attribute```**, see below. | string | "" | no |
| stream_enabled | Indicates whether Streams are to be enabled (true) or disabled (false). | bool | - | no |
| stream_view_type | When an item in the table is modified, StreamViewType determines what information is written to the table's stream. Valid values are **```KEYS_ONLY```**, **```NEW_IMAGE```**, **```OLD_IMAGE```**, **```NEW_AND_OLD_IMAGES```**.  | string | "" | no |
| [attribute](#attribute) | List of nested attribute definitions. **Only required for ```hash_key``` and ```range_key``` attributes**, see below. | block | - | yes |
| create_ttl | Boolean to create or not the **```ttl```** block. | bool | - | yes |
| [ttl](#ttl) | Defines ttl, has two properties, see below. | block | - | no |
| create_local_secondary_index | Boolean to create or not the **```local_secondary_index```** block. | bool | - | yes |
| [local_secondary_index](#local_secondary_index) | Describe an LSI on the table; these can only be allocated at creation so you cannot change this definition after you have created the resource. | block | - | no |
| create_global_secondary_index | Boolean to create or not the **```global_secondary_index```** block | bool | - | yes |
| [global_secondary_index](#global_secondary_index) | Describe a GSI for the table; subject to the normal limits on the number of GSIs, projected attributes, etc. | block | - | no |
| create_server_side_encryption | Boolean to create or not the **```server_side_encryption```** block | bool | - | yes |
| [server_side_encryption](#server_side_encryption) | Encryption at rest options. AWS DynamoDB tables are automatically encrypted at rest with an AWS owned Customer Master Key if this argument isn't specified, see below. | block | - | no |
| create_point_in_time_recovery | Boolean to create or not the **```point_in_time_recovery```** block | bool | - | yes |
| [point_in_time_recovery](#point_in_time_recovery) | Point-in-time recovery options, see below. | block | - | no |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

##### attribute
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The name of the attribute. | string | "" | yes |
| type | Attribute type, which must be a scalar type: **```S```**, **```N```**, or **```B```** for (S)tring, (N)umber or (B)inary data. | string | "" | yes |
---

##### ttl
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| enabled | Indicates whether ttl is enabled (true) or disabled (false). | bool | - | yes |
| attribute_name | The name of the table attribute to store the TTL timestamp in. | string | "" | yes |
---

##### local_secondary_index
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The name of the index. | string | "" | yes |
| range_key | The name of the range key; must be defined. | string | "" | yes |
| projection_type | One of **```ALL```**, **```INCLUDE```** or **```KEYS_ONLY```**, where **```ALL```** projects every attribute into the index, **```KEYS_ONLY```** projects just the hash and range key into the index, and **```INCLUDE```** projects only the keys specified in the non_key_attributes parameter. | string | "" | yes |
| non_key_attributes | Only required with **```INCLUDE```** as a projection type; a list of attributes to project into the index. These do not need to be defined as attributes on the table. | list | [] | no |
---

##### global_secondary_index
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The name of the index. | string | "" | yes |
| write_capacity | The number of write units for this table. **If the billing_mode is ```PROVISIONED```, this field is required**.  | number | - | no |
| read_capacity | The number of read units for this table. **If the billing_mode is ```PROVISIONED```, this field is required**. | number | - | no |
| hash_key | The attribute to use as the hash (partition) key. **Must also be defined as an ```attribute```**, see below. | string | "" | yes |
| range_key | The attribute to use as the range (sort) key. **Must also be defined as an ```attribute```**, see below. | string | "" | no |
| projection_type | One of **```ALL```**, **```INCLUDE```** or **```KEYS_ONLY```**, where **```ALL```** projects every attribute into the index, **```KEYS_ONLY```** projects just the hash and range key into the index, and **```INCLUDE```** projects only the keys specified in the non_key_attributes parameter. | string | "" | yes |
| non_key_attributes | Only required with **```INCLUDE```** as a projection type; a list of attributes to project into the index. These do not need to be defined as attributes on the table. | list | [] | no |
---

##### server_side_encryption
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| enabled | Whether or not to enable encryption at rest using an AWS managed Customer Master Key. If **```enabled```** is **```false```** then server-side encryption is set to AWS owned CMK (shown as **```DEFAULT```** in the AWS console). If **```enabled```** is **```true```** then server-side encryption is set to AWS managed CMK (shown as **```KMS```** in the AWS console). The [AWS KMS documentation](https://docs.aws.amazon.com/pt_br/kms/latest/developerguide/concepts.html) explains the difference between AWS owned and AWS managed CMKs. | bool | - | yes |
---

##### point_in_time_recovery
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| enabled | Whether to enable point-in-time recovery - note that it can take up to 10 minutes to enable for new tables. **If the ```point_in_time_recovery``` block is not provided then this defaults to ```false```**. | bool | - | yes |

---

#### **dynamodb_global_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The name of the global table. Must match underlying DynamoDB Table names in all regions. | string | "" | yes |
| replica | List AWS region name of replica DynamoDB Table. e.g. **```us-east-1```**. | list | [] | yes |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

---

#### **dynamodb_item_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| table_name | The name of the table to contain the item. | string | "" | yes |
| dynamodb_table_item_json_path | File with JSON representation of a map of attribute name/value pairs, one for each attribute. Only the primary key attributes are required; you can optionally provide other attribute name-value pairs for the item. | list | [] | yes |

---

## Outputs

| Name | Description |
|------|-------------|
| module.dynamodb.dynamodb_table_id | The name of the table |
| module.dynamodb.dynamodb_table_arn | The arn of the table |
| module.dynamodb.dynamodb_table_stream_arn | The ARN of the Table Stream |
| module.dynamodb.dynamodb_table_stream_label | A timestamp, in ISO 8601 format, for this stream |
| module.dynamodb.dynamodb_global_id | The name of the global table |
| module.dynamodb.dynamodb_global_arn | The arn of the global table |
| module.dynamodb.dynamodb_item_table_name | The name of the table |
| module.dynamodb.dynamodb_item | The item of the table |
