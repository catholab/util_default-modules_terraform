terraform {
  backend "s3" {
    bucket                  = "infra-lab-terraform-state"
    key                     = "dynamodb/terraform.tfstate"
    region                  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "catho-infra"
  }
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "${var.profile}"
}

module "dynamodb" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//dynamodb"

  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  ## DYNAMODB TABLE OPTIONS ##
  dynamodb_table_create = true
  dynamodb_table_options = list(
    {
      name             = "GameScores",
      billing_mode     = "PROVISIONED",
      read_capacity    = 20,
      write_capacity   = 20,
      hash_key         = "UserId",
      range_key        = "GameTitle",
      stream_enabled   = false,
      stream_view_type = "",
      attribute = list(
        {
          name = "UserId",
          type = "S",
        },
        {
          name = "GameTitle",
          type = "S",
        },
      )
      create_ttl = false
      ttl = list(
        {
          attribute_name = "TimeToExist",
          enabled        = false,
        },
      )
      create_local_secondary_index = false
      local_secondary_index = list(
        {
          name               = "GameTitleIndex",
          range_key          = "TopScore",
          projection_type    = "INCLUDE",
          non_key_attributes = ["UserId"],
        },
      )
      create_global_secondary_index = false
      global_secondary_index = list(
        {
          name               = "GameTitleIndex",
          hash_key           = "GameTitle",
          range_key          = "TopScore",
          write_capacity     = 10,
          read_capacity      = 10,
          projection_type    = "INCLUDE",
          non_key_attributes = ["UserId"],
        },
      )
      create_server_side_encryption = false
      server_side_encryption = list(
        {
          enabled = false,
        },
      )
      create_point_in_time_recovery = false
      point_in_time_recovery = list(
        {
          enabled = false,
        },
      )
      extraTags = {
      }
    },
  )

  ## DYNAMODB GLOBAL TABLE OPTIONS ##
  dynamodb_global_create = true
  dynamodb_global_options = list(
    {
      name = "GameScores",
      replica = list(
        "us-east-1",
      )
      extraTags = {
      }
    },
  )

  ## DYNAMODB ITEM TABLE OPTIONS ##
  dynamodb_item_create = true
  dynamodb_item_options = list(
    {
      table_name                    = "GameScores",
      dynamodb_table_item_json_path = "item.json",
    },
  )

}
