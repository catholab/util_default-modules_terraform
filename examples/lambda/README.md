# AWS [`Lambda`](https://aws.amazon.com/lambda/) Terraform module example

Terraform module which creates Lambda resources on AWS.

These types of resources are supported:

* [aws_lambda_function](https://www.terraform.io/docs/providers/aws/r/lambda_function.html)
* [aws_lambda_alias](https://www.terraform.io/docs/providers/aws/r/lambda_alias.html)
* [aws_lambda_permission](https://www.terraform.io/docs/providers/aws/r/lambda_permission.html)

## Terraform version

* This module was written in terraform version 0.12. For more details, see this [page](https://www.hashicorp.com/blog/announcing-terraform-0-12).

## Usage

```hcl
module "lambda" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//lambda"

  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste_terraform.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  lambda_function_create = true
  lambda_function_options = list(
    {
      name                           = "lambda-function-infra-teste",
      runtime                        = "python3.7",
      handler                        = "lambda-function-teste.lambda_handler",
      iam_role_name                  = "role-infra-lambda-teste",
      file_location                  = "local",
      filename                       = "functions/lambda-function-teste.zip",
      s3_bucket                      = "",
      s3_key                         = "",
      s3_object_version              = "",
      memory_size                    = 128,
      timeout                        = 5,
      reserved_concurrent_executions = -1,
      publish                        = false,
      description                    = "Lambda Function Teste",
      environment_variables = {
        teste = "teste"
      }
      subnet_names = list(
        "subnet-infra-private-zone-a",
        "subnet-infra-private-zone-b",
      )
      security_group_names = list(
        "sg_infra_allow_catho",
      )
      create_dead_letter_config = false,
      dead_letter_config = list(
        {
          target_arn = "",
        },
      )
      create_tracing_config = false,
      tracing_config = list(
        {
          mode = "Active",
        },
      )
    },
  )

  lambda_alias_create = true
  lambda_alias_options = list(
    {
      name             = "teste-alias",
      description      = "Alias for lambda-function-infra-teste"
      function_name    = "lambda-function-infra-teste",
      function_version = "1",
      additional_version_weights = {
        "2" = 0.5
      }
    },
  )

  lambda_permission_create = true
  lambda_permission_options = list(
    {
      action         = "lambda:InvokeFunction",
      function_name  = "lambda-function-infra-teste",
      principal      = "sns.amazonaws.com",
      source_arn     = "${data.aws_sns_topic.default.arn}",
      source_account = "",
    },
  )
}
```

## Global Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| context | Name of team context. | string | "" | yes |
| environment | Name of environment. | string | "" | yes |
| team | Name of team. | string | "" | yes |
| app_release | Release of application. | string | "" | yes |
| app_source | Link of application git repository. | string | "" | yes |
| provisioning_tool | Name of provisioning tool. | string | "" | yes |
| provisioning_version | Version of provisioning tool. | string | "" | yes |
| provisioning_source | Link of provisioning tool git repository. | string | "" | yes |
| deployment_tool | Name of deployment tool. | string | "" | yes |
| deployment_build_name | Name of deployment build. | string | "" | yes |
| deployment_build_nr | Number of deployment build. | string | "" | yes |

---

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| lambda_function_create | Boolean to create or not the lambda function resource. | bool | - | yes |
| [lambda_function_options](#lambda_function_options) | List whit the arguments to create the lambda function resource | list(map) | [] | yes |
| lambda_alias_create | Boolean to create or not the lambda alias resource. | bool | - | yes |
| [lambda_alias_options](#lambda_alias_options) | List whit the arguments to create the lambda alias resource | list(map) | [] | yes |
| lambda_permission_create | Boolean to create or not the lambda permission resource. | bool | - | yes |
| [lambda_permission_options](#lambda_alias_options) | List whit the arguments to create the lambda permission resource | list(map) | [] | yes |

---

### Arguments

#### **lambda_function_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | A unique name for your Lambda Function. | string | "" | yes |
| runtime | See [Runtimes](https://docs.aws.amazon.com/pt_br/lambda/latest/dg/API_CreateFunction.html#SSS-CreateFunction-request-Runtime) for valid values. | string | "" | yes |
| handler | The function [entrypoint](https://docs.aws.amazon.com/pt_br/lambda/latest/dg/with-userapp.html) in your code. | string | "" | yes |
| iam_role_name | IAM role attached to the Lambda Function. **The role must exist** | string | "" | yes |
| file_location | The location where your function's deployment package is storage. Values accepted **```local```** or **```s3```**. If local, set filename argument. If s3, set s3_-prefixed arguments | string | "" | yes |
| filename | The path to the function's deployment package within the local filesystem. **If defined, The s3_-prefixed options cannot be used.** | string | "" | no |
| s3_bucket | The S3 bucket location containing the function's deployment package. **Conflicts with filename.** This bucket must reside in the same AWS region where you are creating the Lambda function. | string | "" | no |
| s3_key | The S3 key of an object containing the function's deployment package. **Conflicts with filename.** | string | "" | no |
| s3_object_version | The object version containing the function's deployment package. **Conflicts with filename.** | string | "" | no |
| memory_size | Amount of memory in MB your Lambda Function can use at runtime. | number | 128 | no |
| timeout | The amount of time your Lambda Function has to run in seconds. | number | 3 | no |
| reserved_concurrent_executions | The amount of reserved concurrent executions for this lambda function. A value of 0 disables lambda from being triggered and -1 removes any concurrency limitations. | number | -1 | no |
| publish | Whether to publish creation/change as new Lambda Function Version. | bool | false | no |
| description | Description of what your Lambda Function does. | string | "" | no |
| environment_variables | A map that defines environment variables for the Lambda function. | map | {} | no |
| subnet_names | List of subnet names associated with the Lambda function. | list | [] | yes |
| security_group_names | List of security group names associated with the Lambda function. | list | [] | yes |
| create_dead_letter_config | Boolean to enable or not dead_letter_config argument block. | bool | - | yes |
| [dead_letter_config](#dead_letter_config) | Nested block to configure the function's dead letter queue. See details below. | block | - | yes |
| create_tracing_config | Boolean to enable or not tracing_config argument block. | bool | - | yes |
| [tracing_config](#tracing_config) | Nested block to configure the function's tracing mode. See details below. | block | - | yes |

##### dead_letter_config

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| target_arn | The ARN of an SNS topic or SQS queue to notify when an invocation fails. **If this option is used, the function's IAM role must be granted suitable access to write to the target object**, which means allowing either the sns:Publish or sqs:SendMessage action on this ARN, depending on which service is targeted. | string | - | yes |

##### tracing_config

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| mode | Can be either **```PassThrough```** or **```Active```**. If PassThrough, Lambda will only trace the request from an upstream service if it contains a tracing header with "sampled=1". If Active, Lambda will respect any tracing header it receives from an upstream service. If no tracing header is received, Lambda will call X-Ray for a tracing decision. | string | - | yes |

---

#### **lambda_alias_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | Name for the alias you are creating. Pattern: ```(?!^[0-9]+$)([a-zA-Z0-9-_]+)```. | string | "" | yes |
| description | Description of the alias. | string | "" | yes |
| function_name | The Name of the Lambda function for which you want to create an alias. | string | "" | yes |
| function_version | Lambda function version for which you are creating the alias. Pattern: ```(\$LATEST|[0-9]+)```. | string | "" | no |
| additional_version_weights | A map that defines the proportion of events that should be sent to different versions of a lambda function. | map | {} | no |

---

#### **lambda_permission_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name |  | string | "" | yes |
| action | The AWS Lambda action you want to allow in this statement. (e.g. ```lambda:InvokeFunction```) | string | "" | yes |
| function_name | Name of the Lambda function whose resource policy you are updating | string | "" | yes |
| principal | The principal who is getting this permission. e.g. ```s3.amazonaws.com```, an AWS account ID, or any valid AWS service principal such as ```events.amazonaws.com``` or ```sns.amazonaws.com```. | string | "" | yes |
| source_arn | When granting Amazon S3 or CloudWatch Events permission to invoke your function, you should specify this field with the Amazon Resource Name (ARN) for the S3 Bucket or CloudWatch Events Rule as its value. This ensures that only events generated from the specified bucket or rule can invoke the function. API Gateway ARNs have a unique structure described [here](https://docs.aws.amazon.com/pt_br/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html). | string | "" | yes |
| source_account | This parameter is used for S3 and SES. The AWS account ID (without a hyphen) of the source owner. | string | "" | yes |
| alias |  | string | "" | yes |

---

## Outputs

| Name | Description |
|------|-------------|
| module.lambda.lambda_function_arn | The Amazon Resource Name (ARN) identifying your Lambda Function. |
| module.lambda.lambda_function_qualified_arn | The Amazon Resource Name (ARN) identifying your Lambda Function Version (if versioning is enabled via publish  |
| module.lambda.lambda_function_invoke_arn | The ARN to be used for invoking Lambda Function from API Gateway - to be used in aws_api_gateway_integration's uri. |
| module.lambda.lambda_function_version | Latest published version of your Lambda Function. |
| module.lambda.lambda_function_last_modified | The date this resource was last modified. |
| module.lambda.lambda_function_source_code_hash | Base64-encoded representation of raw SHA-256 sum of the zip file, provided either via filename or s3_* parameters. |
| module.lambda.lambda_function_source_code_size | The size in bytes of the function .zip file. |
| module.lambda.lambda_alias_arn | The Amazon Resource Name (ARN) identifying your Lambda function alias. |
| module.lambda.lambda_alias_invoke_arn | The ARN to be used for invoking Lambda Function from API Gateway - to be used in aws_api_gateway_integration's uri. |
