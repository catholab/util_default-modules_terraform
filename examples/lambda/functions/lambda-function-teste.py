import json
import socket

host = socket.gethostbyname('google.com')
def lambda_handler(event, context):
    return {
        'statusCode': 200,
        'body': json.dumps(host)
    }
