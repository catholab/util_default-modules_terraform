terraform {
  backend "s3" {
    bucket                  = "infra-lab-terraform-state"
    key                     = "lambda/terraform.tfstate"
    region                  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "catho-infra"
  }
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "${var.profile}"
}

module "lambda" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//lambda"

  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste_terraform.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  lambda_function_create = true
  lambda_function_options = list(
    {
      name                           = "lambda-function-infra-teste",
      runtime                        = "python3.7",
      handler                        = "lambda-function-teste.lambda_handler",
      iam_role_name                  = "role-infra-lambda-teste",
      file_location                  = "local",
      filename                       = "functions/lambda-function-teste.zip",
      s3_bucket                      = "",
      s3_key                         = "",
      s3_object_version              = "",
      memory_size                    = 128,
      timeout                        = 5,
      reserved_concurrent_executions = -1,
      publish                        = false,
      description                    = "Lambda Function Teste",
      environment_variables = {
        teste = "teste"
      }
      subnet_names = list(
        "subnet-infra-private-zone-a",
        "subnet-infra-private-zone-b",
      )
      security_group_names = list(
        "sg_infra_allow_catho",
      )
      create_dead_letter_config = false,
      dead_letter_config = list(
        {
          target_arn = "",
        },
      )
      create_tracing_config = false,
      tracing_config = list(
        {
          mode = "Active",
        },
      )
    },
  )

  lambda_alias_create = true
  lambda_alias_options = list(
    {
      name             = "teste-alias",
      description      = "Alias for lambda-function-infra-teste"
      function_name    = "lambda-function-infra-teste",
      function_version = "1",
      additional_version_weights = {
        "2" = 0.5
      }
    },
  )

  lambda_permission_create = true
  lambda_permission_options = list(
    {
      action         = "lambda:InvokeFunction",
      function_name  = "lambda-function-infra-teste",
      principal      = "sns.amazonaws.com",
      source_arn     = "${data.aws_sns_topic.default.arn}",
      source_account = "",
    },
  )
}
