output "lambda_function_arn" {
  value       = "${module.lambda.lambda_function_arn}"
  description = "The Amazon Resource Name (ARN) identifying your Lambda Function."
}

output "lambda_function_qualified_arn" {
  value       = "${module.lambda.lambda_function_qualified_arn}"
  description = "The Amazon Resource Name (ARN) identifying your Lambda Function Version (if versioning is enabled via publish = true)."
}

output "lambda_function_invoke_arn" {
  value       = "${module.lambda.lambda_function_invoke_arn}"
  description = "The ARN to be used for invoking Lambda Function from API Gateway - to be used in aws_api_gateway_integration's uri."
}

output "lambda_function_version" {
  value       = "${module.lambda.lambda_function_version}"
  description = "Latest published version of your Lambda Function."
}

output "lambda_function_last_modified" {
  value       = "${module.lambda.lambda_function_last_modified}"
  description = "The date this resource was last modified."
}

output "lambda_function_source_code_hash" {
  value       = "${module.lambda.lambda_function_source_code_hash}"
  description = "Base64-encoded representation of raw SHA-256 sum of the zip file, provided either via filename or s3_* parameters."
}

output "lambda_function_source_code_size" {
  value       = "${module.lambda.lambda_function_source_code_size}"
  description = "The size in bytes of the function .zip file."
}

output "lambda_alias_arn" {
  value       = "${module.lambda.lambda_alias_arn}"
  description = "The Amazon Resource Name (ARN) identifying your Lambda function alias."
}

output "lambda_alias_invoke_arn" {
  value       = "${module.lambda.lambda_alias_invoke_arn}"
  description = "The ARN to be used for invoking Lambda Function from API Gateway - to be used in aws_api_gateway_integration's uri."
}

