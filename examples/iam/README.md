# AWS [`IAM`](https://aws.amazon.com/iam/) Terraform module example

Terraform module which creates the following resources on AWS:
* IAM Role

These types of resources are supported:

* [aws_iam_role](https://www.terraform.io/docs/providers/aws/r/iam_role.html)
* [aws_iam_role_policy](https://www.terraform.io/docs/providers/aws/r/iam_role_policy.html)

## Terraform version
- This module was written in terraform version 0.12. For more details, see this [page](https://www.hashicorp.com/blog/announcing-terraform-0-12).

## Usage

```hcl
module "iam" {
  source                = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//iam"


  ## GLOBAL OPTIONS ##
  context = "infra"
  environment = "lab"
  team = "infra"
  app_release = "1.0.1"
  app_source = "https://github.com/teste/teste.git"
  provisioning_tool = "terraform"
  provisioning_version = "0.12.6"
  provisioning_source = "https://github.com/teste/teste.git"
  deployment_tool = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr = "23"

  iam_role_create                 	= true
  iam_role_options                	= list(
                                            {
                                              name = "role-infra-lambda-teste",
                                              force_detach_policies = false,
                                              description = "Lambda Teste Role",
                                              max_session_duration = 43200,
                                              role_json_file = "policies/role-lambda-teste.json",
                                              policy_name = "policy-infra-lambda-teste",
                                              policy_json_file = "policies/policy-lambda-teste.json",
                              		      extraTags = {
                              		      }
                                            },
                                            {
                                              name = "role-infra-ecs-teste",
                                              force_detach_policies = false,
                                              description = "ECS Teste Role",
                                              max_session_duration = 43200,
                                              role_json_file = "policies/role-ecs-teste.json",
                                              policy_name = "policy-infra-ecs-teste",
                                              policy_json_file = "policies/policy-ecs-teste.json",
                              		      extraTags = {
                              		      }
                                            },
                                            {
                                              name = "role-infra-ecs-task-teste",
                                              force_detach_policies = false,
                                              description = "ECS Task Teste Role",
                                              max_session_duration = 43200,
                                              role_json_file = "policies/role-ecs-task-execution-teste.json",
                                              policy_name = "policy-infra-ecs-teste",
                                              policy_json_file = "policies/policy-ecs-task-execution-teste.json",
                              		      extraTags = {
                              		      }
                                            },
                                          )
}
```

#### policies/policy-ecs-task-execution-teste.json

```hcl
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
        }
    ]
}
```

#### policies/role-ecs-task-execution-teste.json

```hcl
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["ecs-tasks.amazonaws.com"]
      },
      "Effect": "Allow"
    }
  ]
}
```

## Global Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| context | Name of team context. | string | "" | yes |
| environment | Name of environment. | string | "" | yes |
| team | Name of team. | string | "" | yes |
| app_release | Release of application. | string | "" | yes |
| app_source | Link of application git repository. | string | "" | yes |
| provisioning_tool | Name of provisioning tool. | string | "" | yes |
| provisioning_version | Version of provisioning tool. | string | "" | yes |
| provisioning_source | Link of provisioning tool git repository. | string | "" | yes |
| deployment_tool | Name of deployment tool. | string | "" | yes |
| deployment_build_name | Name of deployment build. | string | "" | yes |
| deployment_build_nr | Number of deployment build. | string | "" | yes |

---

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| iam_role_create | Boolean to create or not the IAM Role resource. | bool | - | yes |
| [iam_role_options](#iam_role_options) | List with the arguments to create the IAM Role resource. | list(map) | [] | yes |

---

### Arguments
#### **iam_role_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The name of the role. If omitted, Terraform will assign a random, unique name. | string | "" | yes |
| force_detach_policies | Specifies to force detaching any policies the role has before destroying it. | bool | - | yes |
| description | The description of the role. | string | "" | yes |
| max_session_duration | The maximum session duration (in seconds) that you want to set for the specified role. This setting can have a value from 1 hour to 12 hours. | number | - | yes |
| role_json_file | The policy file that grants an entity permission to assume the role. | string | "" | yes |
| policy_name | The name of the role policy. If omitted, Terraform will assign a random, unique name. | string | "" | yes |
| policy_json_file | The policy document. This is a JSON formatted string. For more information about building IAM policy documents with Terraform, see the [AWS IAM Policy Document Guide](https://www.terraform.io/docs/providers/aws/guides/iam-policy-documents.html) | string | "" | yes |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

---

## Outputs

| Name | Description |
|------|-------------|
| module.iam.iam_role_id | The name of the role. |
| module.iam.iam_role_name | The name of the role. |
| module.iam.iam_role_arn | The Amazon Resource Name (ARN) specifying the role. |
| module.iam.iam_role_create_date | The creation date of the IAM role. |
| module.iam.iam_role_description | The description of the role. |
| module.iam.iam_role_unique_id | The stable and unique string identifying the role. |
| module.iam.iam_role_policy_id | The role policy ID. |
| module.iam.iam_role_policy_name | The name of the policy. |
| module.iam.iam_role_policy_policy | The policy document attached to the role. |
| module.iam.iam_role_policy_role | The name of the role associated with the policy. |
