output "iam_role_id" {
  value       = "${module.iam.iam_role_id}"
  description = "The name of the role."

}
output "iam_role_name" {
  value       = "${module.iam.iam_role_name}"
  description = "The name of the role."
}

output "iam_role_arn" {
  value       = "${module.iam.iam_role_arn}"
  description = "The Amazon Resource Name (ARN) specifying the role."
}

output "iam_role_create_date" {
  value       = "${module.iam.iam_role_create_date}"
  description = "The creation date of the IAM role."
}

output "iam_role_description" {
  value       = "${module.iam.iam_role_description}"
  description = "The description of the role."
}

output "iam_role_unique_id" {
  value       = "${module.iam.iam_role_unique_id}"
  description = "The stable and unique string identifying the role."
}

output "iam_role_policy_id" {
  value       = "${module.iam.iam_role_policy_id}"
  description = "The role policy ID."
}

output "iam_role_policy_name" {
  value       = "${module.iam.iam_role_policy_name}"
  description = "The name of the policy."
}

output "iam_role_policy_policy" {
  value       = "${module.iam.iam_role_policy_policy}"
  description = "The policy document attached to the role."
}

output "iam_role_policy_role" {
  value       = "${module.iam.iam_role_policy_role}"
  description = "The name of the role associated with the policy."
}
