terraform {
  backend "s3" {
    bucket                  = "infra-lab-terraform-state"
    key                     = "iam/terraform.tfstate"
    region                  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "catho-infra"
  }
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "${var.profile}"
}

module "iam" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//iam"


  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  iam_role_create = true
  iam_role_options = list(
    {
      name                  = "role-infra-lambda-teste",
      force_detach_policies = false,
      description           = "Lambda Teste Role",
      max_session_duration  = 43200,
      role_json_file        = "policies/role-lambda-teste.json",
      policy_name           = "policy-infra-lambda-teste",
      policy_json_file      = "policies/policy-lambda-teste.json",
      extraTags = {
      }
    },
    {
      name                  = "role-infra-ecs-teste",
      force_detach_policies = false,
      description           = "ECS Teste Role",
      max_session_duration  = 43200,
      role_json_file        = "policies/role-ecs-teste.json",
      policy_name           = "policy-infra-ecs-teste",
      policy_json_file      = "policies/policy-ecs-teste.json",
      extraTags = {
      }
    },
    {
      name                  = "role-infra-ecs-task-teste",
      force_detach_policies = false,
      description           = "ECS Task Teste Role",
      max_session_duration  = 43200,
      role_json_file        = "policies/role-ecs-task-execution-teste.json",
      policy_name           = "policy-infra-ecs-teste",
      policy_json_file      = "policies/policy-ecs-task-execution-teste.json",
      extraTags = {
      }
    },
  )
}
