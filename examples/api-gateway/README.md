# AWS [`API GATEWAY`](https://aws.amazon.com/api-gateway/) Terraform module example

Terraform module which creates the following resources on AWS:
* API Gateway Account
* API Gateway API KEY
* API Gateway Client Certificate
* API Gateway REST API
* API Gateway Resource
* API Gateway Authorizer
* API Gateway Method
* API Gateway Documentation Part
* API Gateway Response
* API Gateway Integration
* API Gateway Deployment

These types of resources are supported:

* [aws_api_gateway_account](https://www.terraform.io/docs/providers/aws/r/api_gateway_account.html)
* [aws_api_gateway_api_key](https://www.terraform.io/docs/providers/aws/r/api_gateway_api_key.html)
* [aws_api_gateway_client_certificate](https://www.terraform.io/docs/providers/aws/r/api_gateway_client_certificate.html)
* [aws_api_gateway_rest_api](https://www.terraform.io/docs/providers/aws/r/api_gateway_rest_api.html)
* [aws_api_gateway_resource](https://www.terraform.io/docs/providers/aws/r/api_gateway_resource.html)
* [aws_api_gateway_authorizer](https://www.terraform.io/docs/providers/aws/r/api_gateway_authorizer.html)
* [aws_api_gateway_method](https://www.terraform.io/docs/providers/aws/r/api_gateway_method.html)
* [aws_api_gateway_documentation_part](https://www.terraform.io/docs/providers/aws/r/api_gateway_documentation_part.html)
* [aws_api_gateway_gateway_response](https://www.terraform.io/docs/providers/aws/r/api_gateway_gateway_response.html)
* [aws_api_gateway_integration](https://www.terraform.io/docs/providers/aws/r/api_gateway_integration.html)
* [aws_api_gateway_deployment](https://www.terraform.io/docs/providers/aws/r/api_gateway_deployment.html)

## Terraform version
- This module was written in terraform version 0.12. For more details, see this [page](https://www.hashicorp.com/blog/announcing-terraform-0-12).

## Usage

```hcl
module "api-gateway" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//api-gateway"

  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste_terraform.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"


  api_gateway_account_create = true
  api_gateway_account_options = list(
    {
      cloudwatch_role_arn = "${data.aws_iam_role.cloudwatch.arn}"
    },
  )

  api_gateway_api_key_create = true
  api_gateway_api_key_options = list(
    {
      name        = "api-key-infra-teste"
      description = "API Key teste"
      enabled     = true
      value       = ""
    },
  )

  api_gateway_client_certificate_create = true
  api_gateway_client_certificate_options = list(
    {
      description = "Client Certificate Teste"
    },
  )

  api_gateway_rest_api_create = true
  api_gateway_rest_api_options = list(
    {
      name        = "api-infra-teste"
      description = "API teste"
    },
  )

  api_gateway_resource_create = true
  api_gateway_resource_options = list(
    {
      rest_api_name = "api-infra-teste"
      path_part     = "teste"
    },
  )

  api_gateway_authorizer_create = true
  api_gateway_authorizer_options = list(
    {
      name                             = "api-authorizer-infra-teste"
      rest_api_name                    = "api-infra-teste"
      authorizer_uri                   = "${data.aws_lambda_function.teste.invoke_arn}"
      authorizer_credentials           = "${aws_iam_role.api_gateway_authorizer_role.arn}"
      identity_source                  = "method.request.header.Authorization"
      type                             = "TOKEN"
      authorizer_result_ttl_in_seconds = 300
      identity_validation_expression   = "[a-zA-Z0-9]+"
    },
  )

  api_gateway_method_create = true
  api_gateway_method_options = list(
    {
      rest_api_name        = "api-infra-teste"
      resource_path        = "/endpoint/path"
      http_method          = "GET"
      authorization        = "NONE"
      authorizer_id        = ""
      authorization_scopes = ""
      api_key_required     = false
      request_validator_id = "${data.aws_api_gateway_request_validator.teste.id}"
      request_models       = {}
      request_parameters   = {}
    },
  )

  api_gateway_documentation_part_create = true
  api_gateway_documentation_part_options = list(
    {
      properties_json_file = "properties/properties.json"
      rest_api_name        = "api-infra-teste"
      version              = "1.0"
      description          = "Documentation teste"
      location = list(
        {
          type        = "METHOD"
          name        = "example_get"
          method      = "GET"
          path        = "/example"
          status_code = "200"
        },
        {
          type        = "METHOD"
          name        = "example_post"
          method      = "POST"
          path        = "/example"
          status_code = "201"
        },
      )
    },
  )

  api_gateway_gateway_response_create = true
  api_gateway_gateway_response_options = list(
    {
      rest_api_name       = "api-infra-teste"
      status_code         = "200"
      response_type       = "OK"
      response_templates  = {}
      response_parameters = {}
    },
  )

  api_gateway_integration_create = true
  api_gateway_integration_options = list(
    {
      rest_api_name           = "api-infra-teste"
      resource_path           = "/endpoint/path"
      http_method             = "GET"
      integration_http_method = "POST"
      type                    = "AWS_PROXY"
      cache_key_parameters    = []
      cache_namespace         = "cache-teste"
      timeout_milliseconds    = "29,000"
      connection_type         = "INTERNET"
      connection_id           = ""
      uri                     = "https://www.teste.com"
      credentials             = "${data.aws_iam_role.api_gateway_integration_role.arn}"
      passthrough_behavior    = ""
      content_handling        = ""
      request_parameters      = {}
      request_templates       = {}
    },
  )

  api_gateway_deployment_create = true
  api_gateway_deployment_options = list(
    {
      rest_api_name     = "api-infra-teste"
      stage_name        = "stage-teste"
      description       = "Deployment Teste"
      stage_description = "Stage Teste"
      variables         = {}
    },
  )
}
```

#### properties/properties.json

```hcl
```

## Global Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| context | Name of team context. | string | "" | yes |
| environment | Name of environment. | string | "" | yes |
| team | Name of team. | string | "" | yes |
| app_release | Release of application. | string | "" | yes |
| app_source | Link of application git repository. | string | "" | yes |
| provisioning_tool | Name of provisioning tool. | string | "" | yes |
| provisioning_version | Version of provisioning tool. | string | "" | yes |
| provisioning_source | Link of provisioning tool git repository. | string | "" | yes |
| deployment_tool | Name of deployment tool. | string | "" | yes |
| deployment_build_name | Name of deployment build. | string | "" | yes |
| deployment_build_nr | Number of deployment build. | string | "" | yes |

---

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| api_gateway_account_create | Boolean to create or not the API Gateway Account resource. | bool | - | yes |
| [api_gateway_account_options](#api_gateway_account_options) | List with the arguments to create the API Gateway Account resource. | list(map) | [] | yes |
| api_gateway_api_key_create | Boolean to create or not the API Gateway API-KEY resource. | bool | - | yes |
| [api_gateway_api_key_options](#api_gateway_api_key_options) | List with the arguments to create the API Gateway API-KEY resource. | list(map) | [] | yes |
| api_gateway_client_certificate_create | Boolean to create or not the API Gateway Client Certificate resource. | bool | - | yes |
| [api_gateway_client_certificate_options](#api_gateway_client_certificate_options) | List with the arguments to create the API Gateway Client Certificate resource. | list(map) | [] | yes |
| api_gateway_rest_api_create | Boolean to create or not the API Gateway REST API resource. | bool | - | yes |
| [api_gateway_rest_api_options](#api_gateway_rest_api_options) | List with the arguments to create the API Gateway REST API resource. | list(map) | [] | yes |
| api_gateway_resource_create | Boolean to create or not the API Gateway Resource resource. | bool | - | yes |
| [api_gateway_resource_options](#api_gateway_resource_options) | List with the arguments to create the API Gateway Resource resource. | list(map) | [] | yes |
| api_gateway_authorizer_create | Boolean to create or not the API Gateway Authorizer resource. | bool | - | yes |
| [api_gateway_authorizer_options](#api_gateway_authorizer_options) | List with the arguments to create the API Gateway Authorizer resource. | list(map) | [] | yes |
| api_gateway_method_create | Boolean to create or not the API Gateway Method resource. | bool | - | yes |
| [api_gateway_method_options](#api_gateway_method_options) | List with the arguments to create the API Gateway Method resource. | list(map) | [] | yes |
| api_gateway_documentation_part_create | Boolean to create or not the API Gateway Documentation Part resource. | bool | - | yes |
| [api_gateway_documentation_part_options](#api_gateway_documentation_part_options) | List with the arguments to create the API Gateway Documentation Part resource. | list(map) | [] | yes |
| api_gateway_gateway_response_create | Boolean to create or not the API Gateway Response resource. | bool | - | yes |
| [api_gateway_gateway_response_options](#api_gateway_gateway_response_options) | List with the arguments to create the API Gateway Response resource. | list(map) | [] | yes |
| api_gateway_integration_create | Boolean to create or not the API Gateway Integration resource. | bool | - | yes |
| [api_gateway_integration_options](#api_gateway_integration_options) | List with the arguments to create the API Gateway Integration resource. | list(map) | [] | yes |
| api_gateway_deployment_create | Boolean to create or not the API Gateway Deployment resource. | bool | - | yes |
| [api_gateway_deployment_options](#api_gateway_deployment_options) | List with the arguments to create the API Gateway Deployment resource. | list(map) | [] | yes |

---

### Arguments
#### **api_gateway_account_options**
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| cloudwatch_role_arn | The ARN of an IAM role for CloudWatch (to allow logging & monitoring). See more in [AWS Docs](https://docs.aws.amazon.com/pt_br/apigateway/latest/developerguide/welcome.html). Logging & monitoring can be enabled/disabled and otherwise tuned on the API Gateway Stage level. | string | "" | yes |

---

#### **api_gateway_api_key_options**
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The name of the API key. | string | "" | yes |
| description | The API key description. Defaults to "Managed by Terraform". | string | "" | yes |
| enabled | Specifies whether the API key can be used by callers. | bool | - | yes |
| value | The value of the API key. If not specified, it will be automatically generated by AWS on creation. | string | "" | no |

---

#### **api_gateway_client_certificate_options**
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| description | The description of the client certificate. | string | "" | yes |

---

#### **api_gateway_rest_api_options**
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The name of the REST API. | string | "" | yes |
| description | The description of the REST API. | string | "" | yes |

---

#### **api_gateway_resource_options**
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| rest_api_name | The Name of the associated REST API. | string | "" | yes |
| path_part | The last path segment of this API resource. | string | "" | yes |

---

#### **api_gateway_authorizer_options**
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The name of the authorizer. | string | "" | yes |
| rest_api_name | The Name of the associated REST API. | string | "" | yes |
| authorizer_uri | required for type TOKEN/REQUEST) The authorizer's Uniform Resource Identifier (URI). **This must be a well-formed Lambda function URI in the form of ```arn:aws:apigateway:{region}:lambda:path/{service_api}```**, e.g. **```arn:aws:apigateway:us-west-2:lambda:path/2015-03-31/functions/arn:aws:lambda:us-west-2:012345678912:function:my-function/invocations```** | string | "" | no |
| authorizer_credentials | The credentials required for the authorizer. To specify an IAM Role for API Gateway to assume, use the IAM Role ARN. | string | "" | no |
| identity_source | The source of the identity in an incoming request. **For ```REQUEST``` type, this may be a comma-separated list of values, including headers, query string parameters and stage variables - e.g. ```"method.request.header.SomeHeaderName,method.request.querystring.SomeQueryStringName,stageVariables.SomeStageVariableName"```**. | string | "" | no |
| type | The type of the authorizer. Possible values are **```TOKEN```** for a Lambda function using a single authorization token submitted in a custom header, **```REQUEST```** for a Lambda function using incoming request parameters, or **```COGNITO_USER_POOLS```** for using an Amazon Cognito user pool. | string | "" | yes |
| authorizer_result_ttl_in_seconds | The TTL of cached authorizer results in seconds. | number | - | yes |
| identity_validation_expression | A validation expression for the incoming identity. **For ```TOKEN``` type, this value should be a regular expression**. The incoming token from the client is matched against this expression, and will proceed if the token matches. If the token doesn't match, the client receives a 401 Unauthorized response. | string | "" | no |

---

#### **api_gateway_method_options**
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| rest_api_name | The Name of the associated REST API. | string | "" | yes |
| resource_path | The API resource PATH | string | "" | yes |
| http_method | The HTTP Method (**```GET```**, **```POST```**, **```PUT```**, **```DELETE```**, **```HEAD```**, **```OPTIONS```**, **```ANY```**) | string | "" | yes |
| authorization | The type of authorization used for the method (**```NONE```**, **```CUSTOM```**, **```AWS_IAM```**, **```COGNITO_USER_POOLS```**). | string | "" | yes |
| authorizer_id | The authorizer id to be used when the authorization is **```CUSTOM```** or **```COGNITO_USER_POOLS```**. | string | "" | no |
| authorization_scopes | The authorization scopes used when the authorization is **```COGNITO_USER_POOLS```** | string | "" | no |
| api_key_required | Specify if the method requires an API key. | string | "" | yes |
| request_validator_id | The ID of a **```aws_api_gateway_request_validator```**. | string | "" | no |
| request_models | A map of the API models used for the request's content type where key is the content type (e.g. **```application/json```**) and value is either **```Error```**, **```Empty```** (built-in models) or **```aws_api_gateway_model's name```**. | map | {} | no |
| request_parameters | A map of request query string parameters and headers that should be passed to the integration. For example: **```request_parameters = {"method.request.header.X-Some-Header" = true "method.request.querystring.some-query-param" = true}```** would define that the header **```X-Some-Header```** and the query string **```some-query-param```** must be provided in the request. | map | {} | no |

---

#### **api_gateway_documentation_part_options**
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| properties_json_file | A JSON file with content map of API-specific key-value pairs describing the targeted API entity. **The map must be encoded as a JSON string, e.g., ```"{ \"description\": \"The API does ...\" }"```. Only Swagger-compliant key-value pairs can be exported and, hence, published**. | string | "" | yes |
| rest_api_name | The Name of the associated REST API. | string | "" | yes |
| version | | string | "" | yes |
| description | | string | "" | yes |
| location | The location of the targeted API entity of the to-be-created documentation part, see below. | block | - | yes |

##### location
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| type | The type of API entity to which the documentation content applies. e.g. **```API```**, **```METHOD```** or **```REQUEST_BODY```**. | string | "" | yes |
| name | The name of the targeted API entity. | string | "" | yes |
| method | The HTTP verb of a method. Type **```*```** for any method. | string | "" | yes |
| path | The URL path of the target. Type **```/```** for the root resource. | string | "" | yes |
| status_code | The HTTP status code of a response. Type **```*```** for any status code. | string | "" | yes |

---

#### **api_gateway_gateway_response_options**
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| rest_api_name | The Name of the associated REST API. | string | "" | yes |
| status_code | The HTTP status code of the Gateway Response. | string | "" | yes |
| response_type | The response type of the associated GatewayResponse. | string | "" | yes |
| response_templates | A map specifying the templates used to transform the response body. | map | {} | no |
| response_parameters | A map specifying the parameters (paths, query strings and headers) of the Gateway Response. | map | {} | no |

---

#### **api_gateway_integration_options**
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| rest_api_name | The Name of the associated REST API. | string | "" | yes |
| resource_path | The API resource PATH | string | "" | yes |
| http_method | The HTTP Method (**```GET```**, **```POST```**, **```PUT```**, **```DELETE```**, **```HEAD```**, **```OPTIONS```**, **```ANY```**) when calling the associated resource. | string | "" | yes |
| integration_http_method | The integration HTTP method (**```GET```**, **```POST```**, **```PUT```**, **```DELETE```**, **```HEAD```**, **```OPTIONs```**, **```ANY```**, **```PATCH```**) specifying how API Gateway will interact with the back end. **Required if type is ```AWS```, ```AWS_PROXY```, ```HTTP``` or ```HTTP_PROXY```**. Not all methods are compatible with all **```AWS```** integrations. e.g. Lambda function [can only be invoked](https://github.com/awslabs/aws-apigateway-importer/issues/9#issuecomment-129651005) via **```POST```**. | string | "" | no |
| type | The integration input's type. Valid values are **```HTTP```** (for HTTP backends), **```MOCK```** (not calling any real backend), **```AWS```** (for AWS services), **```AWS_PROXY```** (for Lambda proxy integration) and **```HTTP_PROXY```** (for HTTP proxy integration). An **```HTTP```** or **```HTTP_PROXY```** integration with a **```connection_type```** of **```VPC_LINK```** is referred to as a private integration and uses a VpcLink to connect API Gateway to a network load balancer of a VPC. | string | "" | no |
| cache_key_parameters | A list of cache key parameters for the integration. | list | [] | no |
| cache_namespace | The integration's cache namespace. | string | "" | no |
| timeout_milliseconds | Custom timeout between 50 and 29,000 milliseconds. | number | - | no |
| connection_type | The integration input's connectionType. Valid values are **```INTERNET```** (default for connections through the public routable internet), and **```VPC_LINK```** (for private connections between API Gateway and a network load balancer in a VPC). | string | "" | yes |
| connection_id | The id of the VpcLink used for the integration. **Required if ```connection_type``` is ```VPC_LINK```**. | string | "" | no |
| uri | The input's URI. **Required if ```type``` is ```AWS```, ```AWS_PROXY```, ```HTTP``` or ```HTTP_PROXY```**. For HTTP integrations, the URI must be a fully formed, encoded HTTP(S) URL according to the RFC-3986 specification . For AWS integrations, the URI should be of the form **```arn:aws:apigateway:{region}:{subdomain.service|service}:{path|action}/{service_api}```**. **```region```**, **```subdomain```** and **```service```** are used to determine the right endpoint. e.g. **```arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions/arn:aws:lambda:eu-west-1:012345678901:function:my-func/invocations```**. | string | "" | no |
| credentials | The credentials required for the integration. For **```AWS```** integrations, 2 options are available. To specify an IAM Role for Amazon API Gateway to assume, use the role's ARN. To require that the caller's identity be passed through from the request, specify the string **```arn:aws:iam::\*:user/\*```**. | string | "" | no |
| passthrough_behavior | The integration passthrough behavior (**```WHEN_NO_MATCH```**, **```WHEN_NO_TEMPLATES```**, **```NEVER```**). **Required if ```request_templates``` is used**. | string | "" | no |
| content_handling | Specifies how to handle request payload content type conversions. Supported values are **```CONVERT_TO_BINARY```** and **```CONVERT_TO_TEXT```**. **If this property is not defined, the request payload will be passed through from the method request to integration request without modification, provided that the passthroughBehaviors is configured to support payload pass-through**. | string | "" | no |
| request_parameters | A map of request query string parameters and headers that should be passed to the backend responder. For example: **```request_parameters = { "integration.request.header.X-Some-Other-Header" = "method.request.header.X-Some-Header" }```**. | map | {} | no |
| request_templates | A map of the integration's request templates. | map | "" | no |

---

#### **api_gateway_deployment_options**
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| rest_api_name | The Name of the associated REST API. | string | "" | yes |
| stage_name | The name of the stage. If the specified stage already exists, it will be updated to point to the new deployment. If the stage does not exist, a new one will be created and point to this deployment. | string | "" | yes |
| description | The description of the deployment. | string | "" | yes |
| stage_description | The description of the stage. | string | "" | yes |
| variables | A map that defines variables for the stage. | map | {} | no |

---

## Outputs

| Name | Description |
|------|-------------|
| module.api-gateway.api_gateway_api_key_id | The ID of the API key. |
| module.api-gateway.api_gateway_api_key_created_date | The creation date of the API key. |
| module.api-gateway.api_gateway_api_key_last_updated_date | The last update date of the API key. |
| module.api-gateway.api_gateway_api_key_value | The value of the API key. |
| module.api-gateway.api_gateway_client_certificate_id | The identifier of the client certificate. |
| module.api-gateway.api_gateway_client_certificate_created_date | The date when the client certificate was created. |
| module.api-gateway.api_gateway_client_certificate_expiration_date | The date when the client certificate will expire. |
| module.api-gateway.api_gateway_client_certificate_pem_encoded_certificate | The PEM-encoded public key of the client certificate. |
| module.api-gateway.api_gateway_rest_api_id | The ID of the REST API. |
| module.api-gateway.api_gateway_rest_api_root_resource_id | The resource ID of the REST API's root. |
| module.api-gateway.api_gateway_rest_api_created_date | The creation date of the REST API. |
| module.api-gateway.api_gateway_rest_api_execution_arn | The execution ARN part to be used in lambda_permission's source_arn when allowing API Gateway to invoke a Lambda function, e.g. arn:aws:execute-api:eu-west-2:123456789012:z4675bid1j, which can be concatenated with allowed stage, method and resource path. |
| module.api-gateway.api_gateway_resource_id | The resource's identifier. |
| module.api-gateway.api_gateway_resource_path | The complete path for this API resource, including all parent paths. |
| module.api-gateway.api_gateway_documentation_part_id | The unique ID of the Documentation Part. |
| module.api-gateway.api_gateway_deployment_id | The ID of the deployment. |
| module.api-gateway.api_gateway_deployment_invoke_url | The URL to invoke the API pointing to the stage, e.g. https://z4675bid1j.execute-api.eu-west-2.amazonaws.com/prod. |
| module.api-gateway.api_gateway_deployment_execution_arn | The execution ARN to be used in lambda_permission's source_arn when allowing API Gateway to invoke a Lambda function, e.g. arn:aws:execute-api:eu-west-2:123456789012:z4675bid1j/prod. |
| module.api-gateway.api_gateway_deployment_created_date | The creation date of the deployment. |
