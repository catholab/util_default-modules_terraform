output "api_gateway_api_key_id" {
  value       = "${module.api-gateway.api_gateway_api_key_id}"
  description = "The ID of the API key."
}

output "api_gateway_api_key_created_date" {
  value       = "${module.api-gateway.api_gateway_api_key_created_date}"
  description = "The creation date of the API key."
}

output "api_gateway_api_key_last_updated_date" {
  value       = "${module.api-gateway.api_gateway_api_key_last_updated_date}"
  description = "The last update date of the API key."
}

output "api_gateway_api_key_value" {
  value       = "${module.api-gateway.api_gateway_api_key_value}"
  description = "The value of the API key."
}


output "api_gateway_client_certificate_id" {
  value       = "${module.api-gateway.api_gateway_client_certificate_id}"
  description = "The identifier of the client certificate."
}

output "api_gateway_client_certificate_created_date" {
  value       = "${module.api-gateway.api_gateway_client_certificate_created_date}"
  description = "The date when the client certificate was created."
}

output "api_gateway_client_certificate_expiration_date" {
  value       = "${module.api-gateway.api_gateway_client_certificate_expiration_date}"
  description = "The date when the client certificate will expire."
}


output "api_gateway_client_certificate_pem_encoded_certificate" {
  value       = "${module.api-gateway.api_gateway_client_certificate_pem_encoded_certificate}"
  description = "The PEM-encoded public key of the client certificate."
}


output "api_gateway_rest_api_id" {
  value       = "${module.api-gateway.api_gateway_rest_api_id}"
  description = "The ID of the REST API."
}

output "api_gateway_rest_api_root_resource_id" {
  value       = "${module.api-gateway.api_gateway_rest_api_root_resource_id}"
  description = "The resource ID of the REST API's root."
}

output "api_gateway_rest_api_created_date" {
  value       = "${module.api-gateway.api_gateway_rest_api_created_date}"
  description = "The creation date of the REST API."
}

output "api_gateway_rest_api_execution_arn" {
  value       = "${module.api-gateway.api_gateway_rest_api_execution_arn}"
  description = "The execution ARN part to be used in lambda_permission's source_arn when allowing API Gateway to invoke a Lambda function, e.g. arn:aws:execute-api:eu-west-2:123456789012:z4675bid1j, which can be concatenated with allowed stage, method and resource path."
}


output "api_gateway_resource_id" {
  value       = "${module.api-gateway.api_gateway_resource_id}"
  description = "The resource's identifier."
}

output "api_gateway_resource_path" {
  value       = "${module.api-gateway.api_gateway_resource_path}"
  description = "The complete path for this API resource, including all parent paths."
}


output "api_gateway_documentation_part_id" {
  value       = "${module.api-gateway.api_gateway_documentation_part_id}"
  description = "The unique ID of the Documentation Part."
}


output "api_gateway_deployment_id" {
  value       = "${module.api-gateway.api_gateway_deployment_id}"
  description = "The ID of the deployment."
}

output "api_gateway_deployment_invoke_url" {
  value       = "${module.api-gateway.api_gateway_deployment_invoke_url}"
  description = "The URL to invoke the API pointing to the stage, e.g. https://z4675bid1j.execute-api.eu-west-2.amazonaws.com/prod."
}

output "api_gateway_deployment_execution_arn" {
  value       = "${module.api-gateway.api_gateway_deployment_execution_arn}"
  description = "The execution ARN to be used in lambda_permission's source_arn when allowing API Gateway to invoke a Lambda function, e.g. arn:aws:execute-api:eu-west-2:123456789012:z4675bid1j/prod."
}

output "api_gateway_deployment_created_date" {
  value       = "${module.api-gateway.api_gateway_deployment_created_date}"
  description = "The creation date of the deployment."
}
