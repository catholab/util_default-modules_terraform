terraform {
  backend "s3" {
    bucket                  = "infra-lab-terraform-state"
    key                     = "api-gateway/terraform.tfstate"
    region                  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "catho-infra"
  }
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "${var.profile}"
}

module "api-gateway" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//api-gateway"

  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste_terraform.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"


  api_gateway_account_create = true
  api_gateway_account_options = list(
    {
      cloudwatch_role_arn = "${data.aws_iam_role.cloudwatch.arn}"
    },
  )

  api_gateway_api_key_create = true
  api_gateway_api_key_options = list(
    {
      name        = "api-key-infra-teste"
      description = "API Key teste"
      enabled     = true
      value       = ""
    },
  )

  api_gateway_client_certificate_create = true
  api_gateway_client_certificate_options = list(
    {
      description = "Client Certificate Teste"
    },
  )

  api_gateway_rest_api_create = true
  api_gateway_rest_api_options = list(
    {
      name        = "api-infra-teste"
      description = "API teste"
    },
  )

  api_gateway_resource_create = true
  api_gateway_resource_options = list(
    {
      rest_api_name = "api-infra-teste"
      path_part     = "teste"
    },
  )

  api_gateway_authorizer_create = true
  api_gateway_authorizer_options = list(
    {
      name                             = "api-authorizer-infra-teste"
      rest_api_name                    = "api-infra-teste"
      authorizer_uri                   = "${data.aws_lambda_function.teste.invoke_arn}"
      authorizer_credentials           = "${aws_iam_role.api_gateway_authorizer_role.arn}"
      identity_source                  = "method.request.header.Authorization"
      type                             = "TOKEN"
      authorizer_result_ttl_in_seconds = 300
      identity_validation_expression   = "[a-zA-Z0-9]+"
    },
  )

  api_gateway_method_create = true
  api_gateway_method_options = list(
    {
      rest_api_name        = "api-infra-teste"
      resource_path        = "/endpoint/path"
      http_method          = "GET"
      authorization        = "NONE"
      authorizer_id        = ""
      authorization_scopes = ""
      api_key_required     = false
      request_validator_id = "${data.aws_api_gateway_request_validator.teste.id}"
      request_models       = {}
      request_parameters   = {}
    },
  )

  api_gateway_documentation_part_create = true
  api_gateway_documentation_part_options = list(
    {
      properties_json_file = "properties/properties.json"
      rest_api_name        = "api-infra-teste"
      version              = "1.0"
      description          = "Documentation teste"
      location = list(
        {
          type        = "METHOD"
          name        = "example_get"
          method      = "GET"
          path        = "/example"
          status_code = "200"
        },
        {
          type        = "METHOD"
          name        = "example_post"
          method      = "POST"
          path        = "/example"
          status_code = "201"
        },
      )
    },
  )

  api_gateway_gateway_response_create = true
  api_gateway_gateway_response_options = list(
    {
      rest_api_name       = "api-infra-teste"
      status_code         = "200"
      response_type       = "OK"
      response_templates  = {}
      response_parameters = {}
    },
  )

  api_gateway_integration_create = true
  api_gateway_integration_options = list(
    {
      rest_api_name           = "api-infra-teste"
      resource_path           = "/endpoint/path"
      http_method             = "GET"
      integration_http_method = "POST"
      type                    = "AWS_PROXY"
      cache_key_parameters    = []
      cache_namespace         = "cache-teste"
      timeout_milliseconds    = "29,000"
      connection_type         = "INTERNET"
      connection_id           = ""
      uri                     = "https://www.teste.com"
      credentials             = "${data.aws_iam_role.api_gateway_integration_role.arn}"
      passthrough_behavior    = ""
      content_handling        = ""
      request_parameters      = {}
      request_templates       = {}
    },
  )

  api_gateway_deployment_create = true
  api_gateway_deployment_options = list(
    {
      rest_api_name     = "api-infra-teste"
      stage_name        = "stage-teste"
      description       = "Deployment Teste"
      stage_description = "Stage Teste"
      variables         = {}
    },
  )
}
