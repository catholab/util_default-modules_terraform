terraform {
  backend "s3" {
    bucket                  = "infra-lab-terraform-state"
    key                     = "kinesis/terraform.tfstate"
    region                  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "catho-infra"
  }
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "${var.profile}"
}

module "kinesis" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//kinesis"


  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  kinesis_stream_create = true
  kinesis_stream_options = list(
    {
      name                      = "kinesis-stream-infra-teste",
      shard_count               = 1,
      retention_period          = 48,
      enforce_consumer_deletion = false
      shard_level_metrics = list(
        "IncomingBytes",
        "OutgoingBytes",
      )
      extraTags = {
      }
    },
  )
}
