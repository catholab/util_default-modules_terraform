# AWS [`KINESIS`](https://aws.amazon.com/kinesis/) Terraform module example

Terraform module which creates the following resources on AWS:
* Kinesis Stream

These types of resources are supported:

* [aws_kinesis_stream](https://www.terraform.io/docs/providers/aws/r/kinesis_stream.html)

## Terraform version
- This module was written in terraform version 0.12. For more details, see this [page](https://www.hashicorp.com/blog/announcing-terraform-0-12).

## Usage

```hcl
module "kinesis" {
  source                = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//kinesis"


  ## GLOBAL OPTIONS ##
  context = "infra"
  environment = "lab"
  team = "infra"
  app_release = "1.0.1"
  app_source = "https://github.com/teste/teste.git"
  provisioning_tool = "terraform"
  provisioning_version = "0.12.6"
  provisioning_source = "https://github.com/teste/teste.git"
  deployment_tool = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr = "23"

  kinesis_stream_create                	= true
  kinesis_stream_options               	= list(
                                            {
                                              name = "kinesis-stream-infra-teste",
                                              shard_count = 1, 
                                              retention_period = 48,
                                              enforce_consumer_deletion = false
                                              shard_level_metrics = list(
					        "IncomingBytes",
					        "OutgoingBytes",
					      )
                              		      extraTags = {
                              		      }
                                            },
                                          )
}
```
## Global Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| context | Name of team context. | string | "" | yes |
| environment | Name of environment. | string | "" | yes |
| team | Name of team. | string | "" | yes |
| app_release | Release of application. | string | "" | yes |
| app_source | Link of application git repository. | string | "" | yes |
| provisioning_tool | Name of provisioning tool. | string | "" | yes |
| provisioning_version | Version of provisioning tool. | string | "" | yes |
| provisioning_source | Link of provisioning tool git repository. | string | "" | yes |
| deployment_tool | Name of deployment tool. | string | "" | yes |
| deployment_build_name | Name of deployment build. | string | "" | yes |
| deployment_build_nr | Number of deployment build. | string | "" | yes |

---

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| kinesis_stream_create | Boolean to create or not the Kinesis Stream resource. | bool | - | yes |
| [kinesis_stream_options](#kinesis_stream_options) | List with the arguments to create the Kinesis Stream resource. | list(map) | [] | yes |

---

### Arguments
#### **kinesis_stream_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | A name to identify the stream. This is unique to the AWS account and region the Stream is created in. | string | "" | yes |
| shard_count | The number of shards that the stream will use. Amazon has guidelines for specifying the Stream size that should be referenced when creating a Kinesis stream. See [Amazon Kinesis Streams](https://docs.aws.amazon.com/pt_br/streams/latest/dev/amazon-kinesis-streams.html) for more.  | number | - | yes |
| retention_period | Length of time data records are accessible after they are added to the stream. The maximum value of a stream's retention period is 168 hours. Minimum value is 24. | number | - | no |
| enforce_consumer_deletion | A boolean that indicates all registered consumers should be deregistered from the stream so that the stream can be destroyed without error. | bool | - | no |
| shard_level_metrics | A list of shard-level CloudWatch metrics which can be enabled for the stream. See [Monitoring with CloudWatch](https://docs.aws.amazon.com/pt_br/streams/latest/dev/monitoring-with-cloudwatch.html) for more. Note that the value ALL should not be used; instead you should provide an explicit list of metrics you wish to enable. | list | [] | no |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

---

## Outputs

| Name | Description |
|------|-------------|
| module.kinesis.kinesis_stream_id | The unique Stream id. |
| module.kinesis.kinesis_stream_name | The unique Stream name. |
| module.kinesis.kinesis_stream_arn | The Amazon Resource Name (ARN) specifying the Stream (same as id). |
| module.kinesis.kinesis_stream_shard_count | The count of Shards for this Stream. |
