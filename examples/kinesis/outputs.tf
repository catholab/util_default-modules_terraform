output "kinesis_stream_id" {
  value       = "${module.kinesis.kinesis_stream_id}"
  description = "The unique Stream id."

}

output "kinesis_stream_name" {
  value       = "${module.kinesis.kinesis_stream_name}"
  description = "The unique Stream name."
}

output "kinesis_stream_arn" {
  value       = "${module.kinesis.kinesis_stream_arn}"
  description = "The Amazon Resource Name (ARN) specifying the Stream (same as id)."
}

output "kinesis_stream_shard_count" {
  value       = "${module.kinesis.kinesis_stream_shard_count}"
  description = "The count of Shards for this Stream."
}
