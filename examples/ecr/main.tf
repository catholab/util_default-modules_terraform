terraform {
  backend "s3" {
    bucket                  = "infra-lab-terraform-state"
    key                     = "ecr/terraform.tfstate"
    region                  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "catho-infra"
  }
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "${var.profile}"
}

module "ecr" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//ecr"

  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  ecr_repository_create = true
  ecr_repository_options = list(
    {
      name                 = "ecr-infra-teste",
      image_tag_mutability = "ECR Repository Teste",
      policy_path          = "./policies/ecr_policy.json",
      extraTags = {
      }
    },
  )
}
