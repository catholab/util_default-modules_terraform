output "ecr_repository_registry_id" {
  value       = "${module.ecr.ecr_repository_registry_id}"
  description = "The registry ID where the repository was created."
}

output "ecr_repository_arn" {
  value       = "${module.ecr.ecr_repository_arn}"
  description = "Full ARN of the repository."
}

output "ecr_repository_name" {
  value       = "${module.ecr.ecr_repository_name}"
  description = "The name of the repository."
}

output "ecr_repository_repository_url" {
  value       = "${module.ecr.ecr_repository_repository_url}"
  description = "The URL of the repository (in the form aws_account_id.dkr.ecr.region.amazonaws.com/repositoryName)"
}
