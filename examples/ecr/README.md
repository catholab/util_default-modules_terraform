# AWS [`ECR`](https://aws.amazon.com/ecr/) Terraform module example

Terraform module which creates the following resources on AWS:
* ECR Repository
* ECR Repository Policy

These types of resources are supported:

* [aws_ecr_repository](https://www.terraform.io/docs/providers/aws/r/ecr_repository.html)
* [aws_ecr_repository_policy](https://www.terraform.io/docs/providers/aws/r/ecr_repository_policy.html)

## Terraform version
- This module was written in terraform version 0.12. For more details, see this [page](https://www.hashicorp.com/blog/announcing-terraform-0-12).

## Usage

```hcl
module "ecr" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//ecr"

  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  ecr_repository_create = true
  ecr_repository_options = list(
    {
      name                 = "ecr-infra-teste",
      image_tag_mutability = "ECR Repository Teste",
      policy_path          = "policies/ecr_policy.json",
      extraTags = {
      }
    },
  )
}
```

#### policies/ecr_policy.json

```hcl
{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "new policy",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "ecr:BatchCheckLayerAvailability",
                "ecr:PutImage",
                "ecr:InitiateLayerUpload",
                "ecr:UploadLayerPart",
                "ecr:CompleteLayerUpload",
                "ecr:DescribeRepositories",
                "ecr:GetRepositoryPolicy",
                "ecr:ListImages",
                "ecr:DeleteRepository",
                "ecr:BatchDeleteImage",
                "ecr:SetRepositoryPolicy",
                "ecr:DeleteRepositoryPolicy"
            ]
        }
    ]
}
```

## Global Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| context | Name of team context. | string | "" | yes |
| environment | Name of environment. | string | "" | yes |
| team | Name of team. | string | "" | yes |
| app_release | Release of application. | string | "" | yes |
| app_source | Link of application git repository. | string | "" | yes |
| provisioning_tool | Name of provisioning tool. | string | "" | yes |
| provisioning_version | Version of provisioning tool. | string | "" | yes |
| provisioning_source | Link of provisioning tool git repository. | string | "" | yes |
| deployment_tool | Name of deployment tool. | string | "" | yes |
| deployment_build_name | Name of deployment build. | string | "" | yes |
| deployment_build_nr | Number of deployment build. | string | "" | yes |

---

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| ecr_repository_create | Boolean to create or not the ECR Repository resource. | bool | - | yes |
| [ecr_repository_options](#ecr_repository_options) | List with the arguments to create the ECR Repository resource. | list(map) | [] | yes |

---

### Arguments
#### **ecr_repository_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | Name of the repository. | string | "" | yes |
| image_tag_mutability | The tag mutability setting for the repository. **Must be one of: ```MUTABLE``` or ```IMMUTABLE```**. | string | "" | no |
| policy_path | The path of policy document. This is a JSON formatted string. For more information about building IAM policy documents with Terraform, see the [AWS IAM Policy Document Guide](https://www.terraform.io/docs/providers/aws/guides/iam-policy-documents.html). | string | "" | yes |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

---

## Outputs

| Name | Description |
|------|-------------|
| module.ecr.ecr_repository_registry_id | The registry ID where the repository was created. |
| module.ecr.ecr_repository_arn | Full ARN of the repository. |
| module.ecr.ecr_repository_name | The name of the repository. |
| module.ecr.ecr_repository_repository_url | The URL of the repository (in the form **```aws_account_id.dkr.ecr.region.amazonaws.com/repositoryName```**) |
