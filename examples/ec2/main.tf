terraform {
  backend "s3" {
    bucket                  = "infra-lab-terraform-state"
    key                     = "ec2/terraform.tfstate"
    region                  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "catho-infra"
  }
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "${var.profile}"
}

module "ec2" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//ec2"

  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste_terraform.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  ## EC2 KEY PAIR OPTIONS ##
  key_pair_create = true
  key_pair_options = list(
    {
      key_name        = "aws-haproy-catho-infra",
      public_key_path = "~/.ssh/id_rsa.pub",
    },
  )

  ## EC2 INSTANCE OPTIONS ##
  instance_create = true
  instance_options = list(
    {
      name                        = "haproxy-public",
      ami                         = "ami-0b898040803850657",
      type                        = "t2.nano",
      subnet_name                 = "subnet-public-infra-zone-a",
      associate_public_ip_address = true,
      private_ip                  = "",
      source_dest_check           = true,
      key_name                    = "aws-haproy-catho-infra",
      root_volume_size            = "8",
      root_volume_type            = "standard",
      create_ebs_block_device     = false
      ebs_block_device = list(
        {
          device_name = ""
          volume_type = ""
          volume_size = ""
        },
      )
      security_group_names = list(
        "sg_catho_infra_allow_catho",
      )
      extraTags = {
      }
    },
  )
}
