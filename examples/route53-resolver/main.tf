terraform {
  backend "s3" {
    bucket                  = "infra-lab-terraform-state"
    key                     = "route53-resolver/terraform.tfstate"
    region                  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "catho-infra"
  }
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "${var.profile}"
}

module "route53-resolver" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//route53-resolver"


  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  r53_resolver_endpoint_inbound_create = true
  r53_resolver_endpoint_inbound_options = list(
    {
      name               = "route53-resolver-infra-inbound",
      subnet_ids         = "${module.vpc.subnet_private_id}"
      security_group_ids = "${module.vpc.sg_id}"
      extraTags = {
      }
    },
  )

  r53_resolver_endpoint_outbound_create = true
  r53_resolver_endpoint_outbound_options = list(
    {
      name               = "route53-resolver-infra-outbound",
      vpc_id             = "${module.vpc.id}"
      subnet_ids         = "${module.vpc.subnet_private_id}"
      security_group_ids = "${module.vpc.sg_id}"
      rule_name          = "route53-resolver-rule-bridge-prod",
      domain_name        = "bridge",
      rule_type          = "FORWARD",
      target_ip = list(
        {
          ip   = "10.255.0.11",
          port = "53",
        },
        {
          ip   = "10.255.1.11",
          port = "53",
        },
      )
      extraTags = {
      }
    },
  )
}
