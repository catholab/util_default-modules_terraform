output "route53_resolver_endpoint_inbound_id" {
  value       = "${module.route53-resolver.route53_resolver_endpoint_inbound_id}"
  description = "The ID of the Route 53 Resolver endpoint."
}

output "route53_resolver_endpoint_inbound_arn" {
  value       = "${module.route53-resolver.route53_resolver_endpoint_inbound_arn}"
  description = "The ARN of the Route 53 Resolver endpoint."
}

output "route53_resolver_endpoint_inbound_host_vpc_id" {
  value       = "${module.route53-resolver.route53_resolver_endpoint_inbound_host_vpc_id}"
  description = "The ID of the VPC that you want to create the resolver endpoint in."
}

output "route53_resolver_endpoint_outbound_id" {
  value       = "${module.route53-resolver.route53_resolver_endpoint_outbound_id}"
  description = "The ID of the Route 53 Resolver endpoint."
}

output "route53_resolver_endpoint_outbound_arn" {
  value       = "${module.route53-resolver.route53_resolver_endpoint_outbound_arn}"
  description = "The ARN of the Route 53 Resolver endpoint."
}

output "route53_resolver_endpoint_outbound_host_vpc_id" {
  value       = "${module.route53-resolver.route53_resolver_endpoint_outbound_host_vpc_id}"
  description = "The ID of the VPC that you want to create the resolver endpoint in."
}

output "route53_resolver_rule_id" {
  value       = "${module.route53-resolver.route53_resolver_rule_id}"
  description = "The ID of the resolver rule."
}

output "route53_resolver_rule_arn" {
  value       = "${module.route53-resolver.route53_resolver_rule_arn}"
  description = "The ARN (Amazon Resource Name) for the resolver rule."
}

output "route53_resolver_rule_owner_id" {
  value       = "${module.route53-resolver.route53_resolver_rule_owner_id}"
  description = "When a rule is shared with another AWS account, the account ID of the account that the rule is shared with."
}

output "route53_resolver_rule_share_status" {
  value       = "${module.route53-resolver.route53_resolver_rule_share_status}"
  description = "Whether the rules is shared and, if so, whether the current account is sharing the rule with another account, or another account is sharing the rule with the current account. Values are NOT_SHARED, SHARED_BY_ME or SHARED_WITH_ME."
}
