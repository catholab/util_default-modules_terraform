# AWS [`ROUTE53-RESOLVER`](https://aws.amazon.com/route53/) Terraform module example

Terraform module which creates the following resources on AWS:
* Route53-Resolver Endpoint
* Route53-Resolver Rule

These types of resources are supported:

* [aws_route53_resolver_endpoint](https://www.terraform.io/docs/providers/aws/r/route53_resolver_endpoint.html)
* [aws_route53_resolver_rule](https://www.terraform.io/docs/providers/aws/r/route53_resolver_rule.html)

## Terraform version
- This module was written in terraform version 0.12. For more details, see this [page](https://www.hashicorp.com/blog/announcing-terraform-0-12).

## Usage

```hcl
module "route53-resolver" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//route53-resolver"


  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  r53_resolver_endpoint_inbound_create = true
  r53_resolver_endpoint_inbound_options = list(
    {
      name = "route53-resolver-infra-inbound",
      subnet_names = list(
        "subnet-infra-private-zone-a",
        "subnet-infra-private-zone-b",
      )
      security_group_names = list(
        "sg_infra_allow_catho",
      )
      extraTags = {
      }
    },
  )

  r53_resolver_endpoint_outbound_create = true
  r53_resolver_endpoint_outbound_options = list(
    {
      name = "route53-resolver-infra-outbound",
      subnet_names = list(
        "subnet-infra-private-zone-a",
        "subnet-infra-private-zone-b",
      )
      security_group_names = list(
        "sg_infra_allow_catho",
      )
      rule_name   = "route53-resolver-rule-bridge-prod",
      domain_name = "bridge",
      rule_type   = "FORWARD",
      target_ip = list(
        {
          ip   = "10.255.0.11",
          port = "53",
        },
        {
          ip   = "10.255.1.11",
          port = "53",
        },
      )
      extraTags = {
      }
    },
  )
}
```

## Global Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| context | Name of team context. | string | "" | yes |
| environment | Name of environment. | string | "" | yes |
| team | Name of team. | string | "" | yes |
| app_release | Release of application. | string | "" | yes |
| app_source | Link of application git repository. | string | "" | yes |
| provisioning_tool | Name of provisioning tool. | string | "" | yes |
| provisioning_version | Version of provisioning tool. | string | "" | yes |
| provisioning_source | Link of provisioning tool git repository. | string | "" | yes |
| deployment_tool | Name of deployment tool. | string | "" | yes |
| deployment_build_name | Name of deployment build. | string | "" | yes |
| deployment_build_nr | Number of deployment build. | string | "" | yes |

---

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| r53_resolver_endpoint_inbound_create | Boolean to create or not the Route53 Resolver - Endpoint **```INBOUND```** resource. | bool | - | yes |
| [r53_resolver_endpoint_inbound_options](#r53_resolver_endpoint_inbound_options) | List with the arguments to create the Route53 Resolver - Endpoint **```INBOUND```** resource. | list(map) | [] | yes |
| r53_resolver_endpoint_outbound_create | Boolean to create or not the Route53 Resolver - Endpoint **```OUTBOUND```** resource. | bool | - | yes |
| [r53_resolver_endpoint_outbound_options](#r53_resolver_endpoint_outbound_options) | List with the arguments to create the Route53 Resolver - Endpoint **```OUTBOUND```** resource. | list(map) | [] | yes |

---

### Arguments
#### **r53_resolver_endpoint_inbound_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The friendly name of the Route 53 Resolver endpoint. | string | "" | yes |
| subnet_names | The Name of one or more subnet that you want to use to access the DNS Server on your VPC or outside them. | list | - | yes |
| security_group_names | The Name of one or more security groups that you want to use to control access to this VPC. | list | - | yes |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

---

#### **r53_resolver_endpoint_outbound_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The friendly name of the Route 53 Resolver endpoint **```OUTBOUND```**. | string | "" | yes |
| subnet_names | The Name of one or more subnet that you want to use to access the DNS Server on your VPC or outside them. | list | - | yes |
| security_group_names | The Name of one or more security groups that you want to use to control access to this VPC. | list | - | yes |
| rule_name | A friendly name that lets you easily find a rule in the Resolver dashboard in the Route 53 console. | string | "" | yes |
| domain_name | DNS queries for this domain name are forwarded to the IP addresses that are specified using **```target_ip```**. | string | "" | yes |
| rule_type | The rule type. Valid values are **```FORWARD```**, **```SYSTEM```** and **```RECURSIVE```**. | string | "" | yes |
| target_ip | Configuration block(s) indicating the IPs that you want Resolver to forward DNS queries to, see below. **This argument should only be specified for ```FORWARD``` type rules**. | block | - | yes |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

##### target_ip
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| ip | One IP address that you want to forward DNS queries to. You can specify only IPv4 addresses. | string | "" | yes |
| port | The port at **```ip```** that you want to forward DNS queries to. | number | 53 | no |

---

## Outputs

| Name | Description |
|------|-------------|
| module.route53-resolver.route53_resolver_endpoint_inbound_id | The ID of the Route 53 Resolver endpoint **```INBOUND```**. |
| module.route53-resolver.route53_resolver_endpoint_inbound_arn | The ARN of the Route 53 Resolver endpoint **```INBOUND```**. |
| module.route53-resolver.route53_resolver_endpoint_inbound_host_vpc_id | The ID of the VPC that you want to create the resolver endpoint **```INBOUND```** in. |
| module.route53-resolver.route53_resolver_endpoint_outbound_id | The ID of the Route 53 Resolver endpoint **```OUTBOUND```**. |
| module.route53-resolver.route53_resolver_endpoint_outbound_arn | The ARN of the Route 53 Resolver endpoint **```OUTBOUND```**. |
| module.route53-resolver.route53_resolver_endpoint_outbound_host_vpc_id | The ID of the VPC that you want to create the resolver endpoint **```OUTBOUND```** in. |
| module.route53-resolver.route53_resolver_rule_id | The ID of the resolver rule. |
| module.route53-resolver.route53_resolver_rule_arn | The ARN (Amazon Resource Name) for the resolver rule. |
| module.route53-resolver.route53_resolver_rule_owner_id | When a rule is shared with another AWS account, the account ID of the account that the rule is shared with. |
| module.route53-resolver.route53_resolver_rule_share_status | Whether the rules is shared and, if so, whether the current account is sharing the rule with another account, or another account is sharing the rule with the current account. Values are NOT_SHARED, SHARED_BY_ME or SHARED_WITH_ME. |
