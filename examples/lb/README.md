# AWS [`Elastic Load Balancing`](https://aws.amazon.com/elasticloadbalancing/) Terraform module example

Terraform module which creates the following resources on AWS:
* Load Balancer (ALB/NLB)
* Load Balancer Listener
* Load Balancer Target Group
* Load Balancer Target Group Attachment

These types of resources are supported:

* [aws_lb](https://www.terraform.io/docs/providers/aws/r/lb.html)
* [aws_lb_listener](https://www.terraform.io/docs/providers/aws/r/lb_listener.html)
* [aws_lb_target_group](https://www.terraform.io/docs/providers/aws/r/lb_target_group.html)
* [aws_lb_target_group_attachment](https://www.terraform.io/docs/providers/aws/r/lb_target_group_attachment.html)

## Terraform version
- This module was written in terraform version 0.12. For more details, see this [page](https://www.hashicorp.com/blog/announcing-terraform-0-12).

## Usage

```hcl
module "lb" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//lb"

  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste_terraform.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  lb_create = true
  lb_options = list(
    {
      name                               = "lb-infra-http-lamda",
      internal                           = false,
      load_balancer_type                 = "application",
      idle_timeout                       = 60,
      enable_cross_zone_load_balancing   = true,
      enable_http2                       = true,
      ip_address_type                    = "ipv4",
      enable_deletion_protection         = false,
      target_group_name                  = "tg-infra-http-lambda",
      target_group_port                  = "80",
      target_group_protocol              = "HTTP",
      target_type                        = "lambda",
      lambda_multi_value_headers_enabled = true,
      proxy_protocol_v2                  = false,
      listener_port                      = "80",
      listener_protocol                  = "HTTP",
      subnet_names = list(
        "subnet-infra-public-zone-a",
        "subnet-infra-public-zone-b",
      )
      security_group_names = list(
        "sg_infra_allow_catho",
      )
      create_access_logs = false
      access_logs = list(
        {
          bucket = ""
          prefix = ""
        },
      )
      create_stickiness = false
      stickiness = list(
        {
          type            = ""
          cookie_duration = ""
          enabled         = ""
        },
      )
      create_health_check = false
      health_check = list(
        {
          enabled             = ""
          interval            = ""
          path                = ""
          port                = ""
          protocol            = ""
          timeout             = ""
          healthy_threshold   = ""
          unhealthy_threshold = ""
          matcher             = ""
        },
      )
      default_action = list(
        {
          type = "forward"
          block = list(
            {
              content_type = "text/plain"
              message_body = "Fixed response content"
              status_code  = "200"
            }
          )
        },
      )
      extraTags = {
      }
    },
    {
      name                               = "lb-infra-http-ecs",
      internal                           = false,
      load_balancer_type                 = "application",
      idle_timeout                       = 60,
      enable_cross_zone_load_balancing   = true,
      enable_http2                       = true,
      ip_address_type                    = "ipv4",
      enable_deletion_protection         = false,
      target_group_name                  = "tg-infra-http-ecs",
      target_group_port                  = "80",
      target_group_protocol              = "HTTP",
      target_type                        = "ip",
      lambda_multi_value_headers_enabled = false,
      proxy_protocol_v2                  = false,
      listener_port                      = "80",
      listener_protocol                  = "HTTP",
      subnet_names = list(
        "subnet-infra-public-zone-a",
        "subnet-infra-public-zone-b",
      )
      security_group_names = list(
        "sg_infra_allow_catho",
      )
      create_access_logs = false
      access_logs = list(
        {
          bucket = ""
          prefix = ""
        },
      )
      create_stickiness = false
      stickiness = list(
        {
          type            = ""
          cookie_duration = ""
          enabled         = ""
        },
      )
      create_health_check = false
      health_check = list(
        {
          enabled             = ""
          interval            = ""
          path                = ""
          port                = ""
          protocol            = ""
          timeout             = ""
          healthy_threshold   = ""
          unhealthy_threshold = ""
          matcher             = ""
        },
      )
      default_action = list(
        {
          type = "forward"
          block = list(
            {
              content_type = "text/plain"
              message_body = "Fixed response content"
              status_code  = "200"
            }
          )
        },
      )
      extraTags = {
      }
    },
  )

  lb_attachment_create = false
  lb_attachment_options = list(
    {
      target_group_name    = "tg-infra-http-lambda"
      target_type          = "lambda"
      instance_name        = ""
      lambda_function_name = "lambda-function-infra-teste"
      target_port          = ""
    },
  )
}
```

## Global Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| context | Name of team context. | string | "" | yes |
| environment | Name of environment. | string | "" | yes |
| team | Name of team. | string | "" | yes |
| app_release | Release of application. | string | "" | yes |
| app_source | Link of application git repository. | string | "" | yes |
| provisioning_tool | Name of provisioning tool. | string | "" | yes |
| provisioning_version | Version of provisioning tool. | string | "" | yes |
| provisioning_source | Link of provisioning tool git repository. | string | "" | yes |
| deployment_tool | Name of deployment tool. | string | "" | yes |
| deployment_build_name | Name of deployment build. | string | "" | yes |
| deployment_build_nr | Number of deployment build. | string | "" | yes |

---

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| lb_create | Boolean to create or not the ECS Cluster resource. | bool | - | yes |
| [lb_options](#lb_options) | List with the arguments to create the ECS Cluster resource. | list(map) | [] | yes |
| lb_attachment_create | Boolean to create or not the ECS Task Definition resource. | bool | - | yes |
| [lb_attachment_options](#lb_attachment_options) | List with the arguments to create the ECS Task Definition resource. | list(map) | [] | yes |

---

### Arguments
#### **lb_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The name of the LB. This name must be unique within your AWS account, can have a maximum of 32 characters, must contain only alphanumeric characters or hyphens, and must not begin or end with a hyphen. If not specified, Terraform will autogenerate a name beginning with tf-lb | string | "" | yes |
| internal | If true, the LB will be internal. | bool | - | yes |
| load_balancer_type | The type of load balancer to create. Possible values are **```application```** or **```network```**. | string | "" | yes |
| idle_timeout | The time in seconds that the connection is allowed to be idle. Only valid for Load Balancers of type **```application```**. | number | - | yes |
| enable_cross_zone_load_balancing | If true, cross-zone load balancing of the load balancer will be enabled. **This is a ```network``` load balancer feature**. | bool | - | no |
| enable_http2 | Indicates whether HTTP/2 is enabled in **```application```** load balancers. | bool | - | no |
| ip_address_type | The type of IP addresses used by the subnets for your load balancer. **The possible values are ```ipv4``` and ```dualstack```**. | string | "" | yes |
| enable_deletion_protection | If true, deletion of the load balancer will be disabled via the AWS API. This will prevent Terraform from deleting the load balancer. | bool | - | no |
| target_group_name | The name of the target group. If omitted, Terraform will assign a random, unique name. | string | "" | yes |
| target_group_port | The port on which targets receive traffic, unless overridden when registering a specific target. **Required when ```target_type``` is ```instance``` or ```ip```**. **Does not apply when ```target_type``` is ```lambda```**. | number | - | yes |
| target_group_protocol | The protocol to use for routing traffic to the targets. Should be one of "TCP", "TLS", "UDP", "TCP_UDP", "HTTP" or "HTTPS". **Required when ```target_type``` is ```instance``` or ```ip```**. **Does not apply when ```target_type``` is ```lambda```**.| string | "" | yes |
| target_type | The type of target that you must specify when registering targets with this target group. The possible values are **```instance```** (targets are specified by instance ID) or **```ip```** (targets are specified by IP address) or **```lambda```** (targets are specified by lambda arn). Note that you can't specify targets for a target group using both instance IDs and IP addresses. **If the target type is ```ip```, specify IP addresses from the subnets of the virtual private cloud (VPC) for the target group, the RFC 1918 range (10.0.0.0/8, 172.16.0.0/12, and 192.168.0.0/16), and the RFC 6598 range (100.64.0.0/10). You can't specify publicly routable IP addresses**. | string | "" | yes |
| lambda_multi_value_headers_enabled | Boolean whether the request and response headers exchanged between the load balancer and the Lambda function include arrays of values or strings. **Only applies when ```target_type``` is ```lambda```**. | bool | - | no |
| proxy_protocol_v2 | Boolean to enable / disable support for proxy protocol v2 on Network Load Balancers. See [doc](https://docs.aws.amazon.com/pt_br/elasticloadbalancing/latest/network/load-balancer-target-groups.html#proxy-protocol) for more information.| bool | - | no |
| listener_port | The port on which the load balancer is listening. | number | - | yes |
| listener_protocol | The protocol for connections from clients to the load balancer. Valid values are **```TCP```**, **```TLS```**, **```UDP```**, **```TCP_UDP```**, **```HTTP```** and **```HTTPS```**. | string | "" | yes |
| subnet_names | A list of subnet IDs to attach to the LB. Subnets cannot be updated for Load Balancers of type network. **Changing this value for load balancers of type ```network``` will force a recreation of the resource**. | list | [] | yes |
| security_group_names | A list of security group IDs to assign to the LB. **Only valid for Load Balancers of type ```application```**. | list | [] | yes |
| create_access_logs | Boolean to create or not the **```access_logs```** block. | bool | - | yes |
| [access_logs](#access_logs) | An Access Logs block, see below | block | - | no |
| create_stickiness | Boolean to create or not the **```stickiness```** block. | bool | - | yes |
| [stickiness](stickiness) | A Stickiness block, see below. Stickiness blocks are documented below. **```stickiness``` is only valid if used with Load Balancers of type ```application```**. | block | - | no |
| create_health_check | Boolean to create or not the **```health_check```** block. | bool | - | yes |
| [health_check](#health_check) | A Health Check block, see below. | block | - | no |
| [default_action](#default_action) | An Action block, see below | block | - | yes |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

##### access_logs
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| bucket | The S3 bucket name to store the logs in. | string | "" | yes |
| prefix | The S3 bucket prefix. Logs are stored in the root if not configured. | string | "" | yes |
---

##### stickiness
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| type | The type of sticky sessions. **The only current possible value is ```lb_cookie```**. | string | "" | yes |
| cookie_duration | The time period, in seconds, during which requests from a client should be routed to the same target. After this time period expires, the load balancer-generated cookie is considered stale. The range is 1 second to 1 week (604800 seconds). | number | - | yes |
| enabled | Boolean to enable / disable **```stickiness```**. | bool | - | yes |
---

##### health_check
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| enabled | Indicates whether health checks are enabled. | bool | - | yes |
| interval | The approximate amount of time, in seconds, between health checks of an individual target. Minimum value 5 seconds, Maximum value 300 seconds. **For ```lambda``` target groups, it needs to be greater as the timeout of the underlying ```lambda```**. | number | - | yes | 
| path | The destination for the health check request. **Applies to Application Load Balancers only (HTTP/HTTPS), not Network Load Balancers (TCP)**. | string | "" | yes | 
| port | The port to use to connect with the target. Valid values are either ports 1-65536, or **```traffic-port```**. | number | - | yes |
| protocol | The protocol to use to connect with the target. **Not applicable when ```target_type``` is ```lambda```**. | string | "" | yes |
| timeout | The amount of time, in seconds, during which no response means a failed health check. For Application Load Balancers, the range is 2 to 120 seconds, and the default is 5 seconds for the **```instance```** target type and 30 seconds for the **```lambda target```** type. For Network Load Balancers, you cannot set a custom value, and the default is 10 seconds for TCP and HTTPS health checks and 6 seconds for HTTP health checks. | number | - | yes |
| healthy_threshold | The number of consecutive health checks successes required before considering an unhealthy target healthy. | number | - | yes |
| unhealthy_threshold | The number of consecutive health check failures required before considering the target unhealthy . **For Network Load Balancers, this value must be the same as the ```healthy_threshold```**. | number | - | yes |
| matcher | The HTTP codes to use when checking for a successful response from a target. You can specify multiple values (for example, "200,202") or a range of values (for example, "200-299"). **Applies to Application Load Balancers only (HTTP/HTTPS), not Network Load Balancers (TCP)**. | number | - | yes |
---

##### default_action
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| type | The type of routing action. Valid values are **```forward```**, **```redirect```**, **```fixed-response```**, **```authenticate-cognito```** and **```authenticate-oidc```**.
| target_group_arn | The ARN of the Target Group to which to route traffic. **Required if type is ```forward```**. | string | - | yes | 
| [redirect](#redirect) | Information Block for creating a redirect action. **Required if type is ```redirect```**. | block | - | yes | 
| [fixed_response](#fixed_response) | Information Block for creating an action that returns a custom HTTP response. **Required if type is ```fixed-response```**. | block | - | yes |
---

##### redirect
| host | The hostname. This component is not percent-encoded. The hostname can contain **```#{host}```**. | string | "" | yes |
| path | The absolute path, starting with the leading "/". This component is not percent-encoded. The path can contain #{host}, #{path}, and #{port}. | string | "" | yes |
| port | The port. Specify a value from **```1```** to **```65535```** or **```#{port}```**. | string | "" | yes |
| protocol | The protocol. Valid values are **```HTTP```**, **```HTTPS```**, or **```#{protocol}```**. | string | "" | yes |
| query | The query parameters, URL-encoded when necessary, but not percent-encoded. **Do not include the leading "?"**. | string | "" | yes |
| status_code | The HTTP redirect code. The redirect is either permanent (**```HTTP_301```**) or temporary (**```HTTP_302```**). | string | "" | yes |
---

##### fixed_response
| content_type | The content type. Valid values are **```text/plain```**, **```text/css```**, **```text/html```**, **```application/javascript```** and **```application/json```**. | string | "" | yes | 
| message_body | The message body. | string | "" | yes |
| status_code | The HTTP response code. Valid values are **```2XX```**, **```4XX```**, or **```5XX```**. | string | "" | yes |

---

#### **lb_attachment_options**
| target_group_name | The Target Group Name to Attach. | string | "" | yes |
| target_type | The Name of Target Group to Attach. Valid values are **```instance```** or **```lambda```**. | string | "" | yes |
| instance_name | The name of instance to attach. **Required only if ```target_type``` is ```instance```**. | string | "" | yes |
| lambda_function_name | The name of lambda function to attach. **Required only if ```target_type``` is ```lambda```**. | string | "" | yes |
| target_port | The port on which targets receive traffic. | number | - | yes |

---

## Outputs

| Name | Description |
|------|-------------|
