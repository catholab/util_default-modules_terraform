terraform {
  backend "s3" {
    bucket                  = "infra-lab-terraform-state"
    key                     = "lb/terraform.tfstate"
    region                  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "catho-infra"
  }
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "${var.profile}"
}

module "lb" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//lb"

  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste_terraform.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  lb_create = true
  lb_options = list(
    {
      name                               = "lb-infra-http-lamda",
      internal                           = false,
      load_balancer_type                 = "application",
      idle_timeout                       = 60,
      enable_cross_zone_load_balancing   = true,
      enable_http2                       = true,
      ip_address_type                    = "ipv4",
      enable_deletion_protection         = false,
      target_group_name                  = "tg-infra-http-lambda",
      target_group_port                  = "80",
      target_group_protocol              = "HTTP",
      target_type                        = "lambda",
      lambda_multi_value_headers_enabled = true,
      proxy_protocol_v2                  = false,
      listener_port                      = "80",
      listener_protocol                  = "HTTP",
      subnet_names = list(
        "subnet-infra-public-zone-a",
        "subnet-infra-public-zone-b",
      )
      security_group_names = list(
        "sg_infra_allow_catho",
      )
      create_access_logs = false
      access_logs = list(
        {
          bucket = ""
          prefix = ""
        },
      )
      create_stickiness = false
      stickiness = list(
        {
          type            = ""
          cookie_duration = ""
          enabled         = ""
        },
      )
      create_health_check = false
      health_check = list(
        {
          enabled             = ""
          interval            = ""
          path                = ""
          port                = ""
          protocol            = ""
          timeout             = ""
          healthy_threshold   = ""
          unhealthy_threshold = ""
          matcher             = ""
        },
      )
      default_action = list(
        {
          type = "forward"
          block = list(
            {
              content_type = "text/plain"
              message_body = "Fixed response content"
              status_code  = "200"
            }
          )
        },
      )
      extraTags = {
      }
    },
    {
      name                               = "lb-infra-http-ecs",
      internal                           = false,
      load_balancer_type                 = "application",
      idle_timeout                       = 60,
      enable_cross_zone_load_balancing   = true,
      enable_http2                       = true,
      ip_address_type                    = "ipv4",
      enable_deletion_protection         = false,
      target_group_name                  = "tg-infra-http-ecs",
      target_group_port                  = "80",
      target_group_protocol              = "HTTP",
      target_type                        = "ip",
      lambda_multi_value_headers_enabled = false,
      proxy_protocol_v2                  = false,
      listener_port                      = "80",
      listener_protocol                  = "HTTP",
      subnet_names = list(
        "subnet-infra-public-zone-a",
        "subnet-infra-public-zone-b",
      )
      security_group_names = list(
        "sg_infra_allow_catho",
      )
      create_access_logs = false
      access_logs = list(
        {
          bucket = ""
          prefix = ""
        },
      )
      create_stickiness = false
      stickiness = list(
        {
          type            = ""
          cookie_duration = ""
          enabled         = ""
        },
      )
      create_health_check = false
      health_check = list(
        {
          enabled             = ""
          interval            = ""
          path                = ""
          port                = ""
          protocol            = ""
          timeout             = ""
          healthy_threshold   = ""
          unhealthy_threshold = ""
          matcher             = ""
        },
      )
      default_action = list(
        {
          type = "forward"
          block = list(
            {
              content_type = "text/plain"
              message_body = "Fixed response content"
              status_code  = "200"
            }
          )
        },
      )
      extraTags = {
      }
    },
  )

  lb_attachment_create = false
  lb_attachment_options = list(
    {
      target_group_name    = "tg-infra-http-lambda"
      target_type          = "lambda"
      instance_name        = ""
      lambda_function_name = "lambda-function-infra-teste"
      target_port          = ""
    },
  )
}
