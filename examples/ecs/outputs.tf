output "ecs_cluster_id" {
  value       = "${module.ecs.ecs_cluster_id}"
  description = "The Amazon Resource Name (ARN) that identifies the cluster."
}

output "ecs_cluster_arn" {
  value       = "${module.ecs.ecs_cluster_arn}"
  description = "The Amazon Resource Name (ARN) that identifies the cluster."
}

output "ecs_task_definition_arn" {
  value       = "${module.ecs.ecs_task_definition_arn}"
  description = "Full ARN of the Task Definition (including both family and revision)."
}

output "ecs_task_definition_family" {
  value       = "${module.ecs.ecs_task_definition_family}"
  description = "The family of the Task Definition."
}

output "ecs_task_definition_revision" {
  value       = "${module.ecs.ecs_task_definition_revision}"
  description = "The revision of the task in a particular family."
}

output "ecs_service_id" {
  value       = "${module.ecs.ecs_service_id}"
  description = "The Amazon Resource Name (ARN) that identifies the service."
}

output "ecs_service_name" {
  value       = "${module.ecs.ecs_service_name}"
  description = "The name of the service."
}

output "ecs_service_cluster" {
  value       = "${module.ecs.ecs_service_cluster}"
  description = "The Amazon Resource Name (ARN) of cluster which the service runs on."
}

output "ecs_service_iam_role" {
  value       = "${module.ecs.ecs_service_iam_role}"
  description = "The ARN of IAM role used for ELB."
}

output "ecs_service_desired_count" {
  value       = "${module.ecs.ecs_service_desired_count}"
  description = "The number of instances of the task definition."
}
