# AWS [`ECS`](https://aws.amazon.com/ecs/) Terraform module example

Terraform module which creates the following resources on AWS:
* ECS Cluster
* ECS Task Definition
* ECS Service

These types of resources are supported:

* [aws_ecs_cluster](https://www.terraform.io/docs/providers/aws/r/ecs_cluster.html)
* [aws_ecs_task_definition](https://www.terraform.io/docs/providers/aws/r/ecs_task_definition.html)
* [aws_ecs_service](https://www.terraform.io/docs/providers/aws/r/ecs_service.html)

## Terraform version
- This module was written in terraform version 0.12. For more details, see this [page](https://www.hashicorp.com/blog/announcing-terraform-0-12).

## Usage

```hcl
module "ecs" {
  source                = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//ecs"

  ## GLOBAL OPTIONS ##
  context = "infra"
  environment = "lab"
  team = "infra"
  app_release = "1.0.1"
  app_source = "https://github.com/teste/teste.git"
  provisioning_tool = "terraform"
  provisioning_version = "0.12.6"
  provisioning_source = "https://github.com/teste/teste_terraform.git"
  deployment_tool = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr = "23"

  ecs_cluster_create                    = true
  ecs_cluster_options                   = list(
                                            {
                                              name = "ecs-infra-cluster-teste",
                              		      extraTags = {
                              		      }
                                            },
                                          )

  ecs_task_definition_create		= true
  ecs_task_definition_options           = list(
                                            {
                                              family = "ecs-infra-task-teste",
                                              container_definitions = "task-definitions/service.json",
                                              execution_role_name = "role-infra-ecs-task-teste",
                                              network_mode = "awsvpc",
                                              requires_compatibilities = ["FARGATE"],
                                              memory = "512",
                                              cpu = "256",
                                              create_volume = false
                                              volume = list(
                                                {
                                                  name = "service-storage",
                                                  host_path = "/ecs/service-storage",
                                                },
                                              )
                                              create_proxy_configuration = false
                                              proxy_configuration = list(
                                                {
                                                  container_name = "",
                                                  properties = "",
                                                  type = "",
                                                },
                                              )
                              		      extraTags = {
                              		      }
                                            },
                                          )

  ecs_service_create                    = true
  ecs_service_options                   = list(
                                            {
                                              name = "ecs-infra-service-teste",
                                              ecs_cluster_name = "ecs-infra-cluster-teste",
                                              desired_count = "3",
                                              launch_type = "FARGATE",
                                              scheduling_strategy = "REPLICA",
                                              deployment_maximum_percent = "200",
                                              deployment_minimum_healthy_percent = "100",
                                              iam_role_name = "role-infra-ecs-teste",
                                              create_network_configuration = true,
                                              subnet_names = list(
                                                "subnet-infra-private-zone-a",
                                                "subnet-infra-private-zone-b",
                                              )
                                              security_group_names = list(
                                                "sg_infra_allow_catho",
                                              )
                                              create_deployment_controller = false,
                                              deployment_controller = list(
                                                {
                                                  type = "",
                                                },
                                              )
                                              create_load_balancer = true,
                                              load_balancer = list(
                                                {
                                                  type = "lb",
                                                  elb_name = "",
                                                  lb_name = "tg-infra-http-ecs",
                                                  container_name = "sample-app",
                                                  container_port = "80",
                                                },
                                              )
                                              create_ordered_placement_strategy = false,
                                              ordered_placement_strategy = list(
                                                {
                                                  type = "",
                                                  field = "",
                                                },
                                              )
                                              create_placement_constraints = false
                                              placement_constraints = list(
                                                {
                                                  type = "",
                                                  expression = "",
                                                },
                                              )
                                            },
                                          )
}
```

#### task-definitions/service.json

```hcl
[
    {
      "dnsSearchDomains": null,
      "logConfiguration": {
        "logDriver": "awslogs",
        "secretOptions": null,
        "options": {
          "awslogs-group": "/ecs/first-run-task-definition",
          "awslogs-region": "us-east-1",
          "awslogs-stream-prefix": "ecs"
        }
      },
      "entryPoint": [
        "sh",
        "-c"
      ],
      "portMappings": [
        {
          "hostPort": 80,
          "protocol": "tcp",
          "containerPort": 80
        }
      ],
      "command": [
        "/bin/sh -c \"echo '<html> <head> <title>Amazon ECS Sample App</title> <style>body {margin-top: 40px; background-color: #333;} </style> </head><body> <div style=color:white;text-align:center> <h1>Amazon ECS Sample App</h1> <h2>Congratulations!</h2> <p>Your application is now running on a container in Amazon ECS.</p> </div></body></html>' >  /usr/local/apache2/htdocs/index.html && httpd-foreground\""
      ],
      "linuxParameters": null,
      "cpu": 256,
      "environment": [],
      "resourceRequirements": null,
      "ulimits": null,
      "dnsServers": null,
      "mountPoints": [],
      "workingDirectory": null,
      "secrets": null,
      "dockerSecurityOptions": null,
      "memory": null,
      "memoryReservation": 512,
      "volumesFrom": [],
      "stopTimeout": null,
      "image": "httpd:2.4",
      "startTimeout": null,
      "dependsOn": null,
      "disableNetworking": null,
      "interactive": null,
      "healthCheck": null,
      "essential": true,
      "links": [],
      "hostname": null,
      "extraHosts": null,
      "pseudoTerminal": null,
      "user": null,
      "readonlyRootFilesystem": null,
      "dockerLabels": null,
      "systemControls": null,
      "privileged": null,
      "name": "sample-app"
    }
]
```

## Global Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| context | Name of team context. | string | "" | yes |
| environment | Name of environment. | string | "" | yes |
| team | Name of team. | string | "" | yes |
| app_release | Release of application. | string | "" | yes |
| app_source | Link of application git repository. | string | "" | yes |
| provisioning_tool | Name of provisioning tool. | string | "" | yes |
| provisioning_version | Version of provisioning tool. | string | "" | yes |
| provisioning_source | Link of provisioning tool git repository. | string | "" | yes |
| deployment_tool | Name of deployment tool. | string | "" | yes |
| deployment_build_name | Name of deployment build. | string | "" | yes |
| deployment_build_nr | Number of deployment build. | string | "" | yes |

---

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| ecs_cluster_create | Boolean to create or not the ECS Cluster resource. | bool | - | yes |
| [ecs_cluster_options](#ecs_cluster_options) | List with the arguments to create the ECS Cluster resource. | list(map) | [] | yes |
| ecs_task_definition_create | Boolean to create or not the ECS Task Definition resource. | bool | - | yes |
| [ecs_task_definition_options](#ecs_task_definition_options) | List with the arguments to create the ECS Task Definition resource. | list(map) | [] | yes |
| ecs_service_create | Boolean to create or not the ECS Service resource. | bool | - | yes |
| [ecs_service_options](#ecs_service_options) | List with the arguments to create the ECS Service resource. | list(map) | [] | yes |

---

### Arguments
#### **ecs_cluster_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The name of the cluster (up to 255 letters, numbers, hyphens, and underscores)| string | "" | yes |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

---

#### **ecs_task_definition_options**
| family | A unique name for your task definition. | string | "" | yes |
| container_definition | A list of valid [container definitions](https://docs.aws.amazon.com/pt_br/AmazonECS/latest/developerguide/task_definition_parameters.html) provided as a single valid JSON document. Please note that you should only provide values that are part of the container definition document. For a detailed description of what parameters are available, see the [Task Definition Parameters](https://docs.aws.amazon.com/pt_br/AmazonECS/latest/developerguide/Welcome.html) section from the official [Developer Guide](https://docs.aws.amazon.com/pt_br/kms/latest/developerguide/concepts.html). | string | "" | yes |
| execution_role_name | The Name of the task execution role that the Amazon ECS container agent and the Docker daemon can assume. | string | "" | yes |
| network_mode | The Docker networking mode to use for the containers in the task. The valid values are **```none```**, **```bridge```**, **```awsvpc```**, and **```host```**. | string | "" | yes |
| requires_compatibilities | A set of launch types required by the task. The valid values are **```EC2```** and **```FARGATE```**. | list | [] | yes |
| memory | The amount (in MiB) of memory used by the task. **If the ```requires_compatibilities``` is ```FARGATE``` this field is required**. | number | "" | no | 
| cpu | The number of cpu units used by the task. **If the ```requires_compatibilities``` is ```FARGATE``` this field is required**. | number | "" | no |
| create_volume | Boolean to create or not the **```volume```** block. | bool | - | yes |
| [volume](#volume) | A set of volume blocks that containers in your task may use, see below. | block | - | no |
| create_proxy_configuration | Boolean to create or not the **```proxy_configuration```** block. | bool | - | yes |
| [proxy_configuration](#proxy_configuration) | The proxy configuration details for the App Mesh proxy, see below. | block | - | no |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

##### volume
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The name of the volume. This name is referenced in the **```sourceVolume```** parameter of container definition in the **```mountPoints```** section. | string | "" | yes |
| host_path | The path on the host container instance that is presented to the container. If not set, ECS will create a nonpersistent data volume that starts empty and is deleted after the task has finished. | string | "" | yes |
---

##### proxy_configuration
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| container_name | The name of the container that will serve as the App Mesh proxy. | string | "" | yes |
| properties | The set of network configuration parameters to provide the Container Network Interface (CNI) plugin, specified a key-value mapping. | map | {} | yes |
| type | The proxy type. The default value is **```APPMESH```**. **The only supported value is ```APPMESH```**. | string | "" | yes |

---

#### **ecs_service_options**
| name | The name of the service (up to 255 letters, numbers, hyphens, and underscores). | string | "" | yes |
| ecs_cluster_name | Name of an ECS cluster. | string | "" | yes |
| desired_count | The number of instances of the task definition to place and keep running. **Do not specify if using the ```DAEMON``` scheduling strategy**. | number | - | yes |
| launch_type | The launch type on which to run your service. The valid values are **```EC2```** and **```FARGATE```**. | string | "" | yes |
| scheduling_strategy | The scheduling strategy to use for the service. The valid values are **```REPLICA```** and **```DAEMON```**. **Note that Fargate tasks do not support the ```DAEMON``` scheduling strategy**. | string | "" | yes |
| deployment_maximum_percent | The upper limit (as a percentage of the service's desiredCount) of the number of running tasks that can be running in a service during a deployment. **Not valid when using the ```DAEMON``` scheduling strategy**. | number | - | no |
| deployment_minimum_healthy_percent | The lower limit (as a percentage of the service's desiredCount) of the number of running tasks that must remain running and healthy in a service during a deployment. | number | - | no |
| create_network_configuration | Boolean to enable to use or not the **```subnet```** and **```security_group```** arguments. | bool | - | yes |
| subnet_names | The name of subnets associated with the task or service. | string | "" | yes |
| security_group_names | The security groups associated with the task or service. | string | "" | yes |
| create_deployment_controller |  Boolean to create or not the **```deployment_controller```** block. | bool | - | yes |
| [deployment_controller](#deployment_controller) | Configuration block containing deployment controller configuration, see below. | block | - | no |
| create_load_balancer |  Boolean to create or not the **```load_balancer```** block. | bool | - | yes |
| [load_balancer](#load_balancer) | A load balancer block, see below. | block | - | no |
| create_ordered_placement_strategy | Boolean to create or not the **```ordered_placement_strategy```** block. | bool | - | yes |
| [ordered_placement_strategy](#ordered_placement_strategy) | Service level strategy rules that are taken into consideration during task placement. List from top to bottom in order of precedence. **The maximum number of ```ordered_placement_strategy``` blocks is 5```**, see below. | block | - | no |
| create_placement_constraints |  Boolean to create or not the **```placement_constraints```** block. | bool | - | yes |
| [placement_constraints](#placement_constraints) | rules that are taken into consideration during task placement. **Maximum number of ```placement_constraints``` is 10```**, see below. | block | - | no |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

##### deployment_controller
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| type | Type of deployment controller. Valid values: **```CODE_DEPLOY```**, **```ECS```**. | string | "" | no |
---

##### load_balancer
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| type | The type of the load balancer. Valid values: **```lb```** for ALB/NBL and **```elb```** for Classic Elastic Load Balancer. | string | "" | yes |
| elb_name | The name of the ELB (Classic) to associate with the service. **Required if ```type``` = ```elb```**. | string | "" | no |
| lb_name | The name of the ALB/NBL Target Group to associate with the service. **Required if ```type``` = ```lb```**. | string | "" | no |
| container_name | The name of the container to associate with the load balancer (as it appears in a container definition). | string | "" | yes |
| container_port | The port on the container to associate with the load balancer. | number | - | yes |
---

##### ordered_placement_strategy
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| type | The type of placement strategy. **Must be one of: ```binpack```, ```random```, or ```spread```**. | string | "" | yes |
| field | For the **```spread```** placement strategy, valid values are **```instanceId```** (or **```host```**, which has the same effect), or any platform or custom attribute that is applied to a container instance. For the **```binpack```** type, valid values are **```memory```** and **```cpu```**. For the **```random```** type, this attribute is not needed. For more information, see [Placement Strategy](https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_PlacementStrategy.html). | string | "" | yes | 
---

##### placement_constraints
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| type | The type of constraint. **The only valid values at this time are ```memberOf``` and ```distinctInstance```**. | string | "" | yes |
| expression | Cluster Query Language expression to apply to the constraint. Does not need to be specified for the **```distinctInstance```** type. For more information, see [Cluster Query Language in the Amazon EC2 Container Service Developer Guide](https://docs.aws.amazon.com/pt_br/AmazonECS/latest/developerguide/cluster-query-language.html). | string | "" | yes |

---

## Outputs

| Name | Description |
|------|-------------|
| module.ecs.ecs_cluster_id | The Amazon Resource Name (ARN) that identifies the cluster. |
| module.ecs.ecs_cluster_arn | The Amazon Resource Name (ARN) that identifies the cluster. |
| module.ecs.ecs_task_definition_arn | Full ARN of the Task Definition (including both family and revision). |
| module.ecs.ecs_task_definition_family | The family of the Task Definition. |
| module.ecs.ecs_task_definition_revision | The revision of the task in a particular family. |
| module.ecs.ecs_service_id | The Amazon Resource Name (ARN) that identifies the service. |
| module.ecs.ecs_service_name | The name of the service. |
| module.ecs.ecs_service_cluster | The Amazon Resource Name (ARN) of cluster which the service runs on. |
| module.ecs.ecs_service_iam_role | The ARN of IAM role used for ELB. |
| module.ecs.ecs_service_desired_count | The number of instances of the task definition. |
