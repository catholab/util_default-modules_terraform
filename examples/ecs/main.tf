terraform {
  backend "s3" {
    bucket                  = "infra-lab-terraform-state"
    key                     = "ecs/terraform.tfstate"
    region                  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "catho-infra"
  }
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "${var.profile}"
}

module "ecs" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//ecs"

  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste_terraform.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  ecs_cluster_create = true
  ecs_cluster_options = list(
    {
      name = "ecs-infra-cluster-teste",
      extraTags = {
      }
    },
  )

  ecs_task_definition_create = true
  ecs_task_definition_options = list(
    {
      family                   = "ecs-infra-task-teste",
      container_definitions    = "task-definitions/service.json",
      execution_role_name      = "role-infra-ecs-task-teste",
      network_mode             = "awsvpc",
      requires_compatibilities = ["FARGATE"],
      memory                   = "512",
      cpu                      = "256",
      create_volume            = false
      volume = list(
        {
          name      = "service-storage",
          host_path = "/ecs/service-storage",
        },
      )
      create_proxy_configuration = false
      proxy_configuration = list(
        {
          container_name = "",
          properties     = "",
          type           = "",
        },
      )
      extraTags = {
      }
    },
  )

  ecs_service_create = true
  ecs_service_options = list(
    {
      name                               = "ecs-infra-service-teste",
      ecs_cluster_name                   = "ecs-infra-cluster-teste",
      desired_count                      = "3",
      launch_type                        = "FARGATE",
      scheduling_strategy                = "REPLICA",
      deployment_maximum_percent         = "200",
      deployment_minimum_healthy_percent = "100",
      create_network_configuration       = true,
      subnet_names = list(
        "subnet-infra-private-zone-a",
        "subnet-infra-private-zone-b",
      )
      security_group_names = list(
        "sg_infra_allow_catho",
      )
      create_deployment_controller = false,
      deployment_controller = list(
        {
          type = "",
        },
      )
      create_load_balancer = true,
      load_balancer = list(
        {
          type           = "lb",
          elb_name       = "",
          lb_name        = "tg-infra-http-ecs",
          container_name = "sample-app",
          container_port = "80",
        },
      )
      create_ordered_placement_strategy = false,
      ordered_placement_strategy = list(
        {
          type  = "",
          field = "",
        },
      )
      create_placement_constraints = false
      placement_constraints = list(
        {
          type       = "",
          expression = "",
        },
      )
    },
  )
}