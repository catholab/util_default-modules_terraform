data "aws_vpn_gateway" "uoldiveo_sp" {
  provider = "catho_sp"

  filter {
    name   = "tag:Name"
    values = ["VGW-AWS-TMB-LINK-01"]
  }
}

data "aws_vpn_gateway" "uoldiveo_vg" {
  filter {
    name   = "tag:Name"
    values = ["vgw-catho-uoldiveo"]
  }
}
