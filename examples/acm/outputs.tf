output "acm_certificate_id" {
  value       = "${module.acm.acm_certificate_id}"
  description = "The ARN of the certificate."
}

output "acm_certificate_arn" {
  value       = "${module.acm.acm_certificate_arn}"
  description = "The ARN of the certificate."
}

output "acm_certificate_domain_name" {
  value       = "${module.acm.acm_certificate_domain_name}"
  description = "The domain name for which the certificate is issued."
}

output "acm_certificate_domain_validation_options" {
  value       = "${module.acm.acm_certificate_domain_validation_options}"
  description = "A list of attributes to feed into other resources to complete certificate validation. Can have more than one element, e.g. if SANs are defined. Only set if DNS-validation was used."
}

output "acm_certificate_validation_emails" {
  value       = "${module.acm.acm_certificate_validation_emails}"
  description = "A list of addresses that received a validation E-Mail. Only set if EMAIL-validation was used."
}
