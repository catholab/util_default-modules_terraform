terraform {
  backend "s3" {
    bucket                  = "infra-lab-terraform-state"
    key                     = "acm/terraform.tfstate"
    region                  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "catho-infra"
  }
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "${var.profile}"
}

module "acm" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//acm"


  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  acm_certificate_create = true
  acm_certificate_options = list(
    {
      domain_name               = "*.catho.com.br",
      validation_method         = "",
      subject_alternative_names = [],
      private_key               = file("certs/private_key.pem"),
      certificate_file          = file("certs/cert.pem"),
      certificate_chain         = file("certs/cert-ca.pem"),
      extraTags = {
      }
    },
  )
}
