# AWS [`ACM`](https://aws.amazon.com/acm/) Terraform module example

Terraform module which creates the following resources on AWS:
* ACM

These types of resources are supported:

* [aws_acm_certificate](https://www.terraform.io/docs/providers/aws/r/acm_certificate.html)

## Terraform version
- This module was written in terraform version 0.12. For more details, see this [page](https://www.hashicorp.com/blog/announcing-terraform-0-12).

## Usage

```hcl
module "acm" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//acm"


  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  acm_certificate_create = true
  acm_certificate_options = list(
    {
      domain_name               = "*.catho.com.br",
      validation_method         = "",
      subject_alternative_names = [],
      private_key               = file("certs/private_key.pem"),
      certificate_file          = file("certs/cert.pem"),
      certificate_chain         = file("certs/cert-ca.pem"),
      extraTags = {
      }
    },
  )
}
```

## Global Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| context | Name of team context. | string | "" | yes |
| environment | Name of environment. | string | "" | yes |
| team | Name of team. | string | "" | yes |
| app_release | Release of application. | string | "" | yes |
| app_source | Link of application git repository. | string | "" | yes |
| provisioning_tool | Name of provisioning tool. | string | "" | yes |
| provisioning_version | Version of provisioning tool. | string | "" | yes |
| provisioning_source | Link of provisioning tool git repository. | string | "" | yes |
| deployment_tool | Name of deployment tool. | string | "" | yes |
| deployment_build_name | Name of deployment build. | string | "" | yes |
| deployment_build_nr | Number of deployment build. | string | "" | yes |

---

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| acm_certificate_create | Boolean to create or not the ACM resource. | bool | - | yes |
| [acm_certificate_options](#acm_certificate_options) | List with the arguments to create the ACM resource. | list(map) | [] | yes |

---

### Arguments
#### **acm_certificate_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| domain_name | A domain name for which the certificate should be issued. | string | "" | yes |
| validation_method | Which method to use for validation. **```DNS```** or **```EMAIL```** are valid, **```NONE```** can be used for certificates that were imported into ACM and then into Terraform. | string | "" | yes |
| subject_alternative_names | A list of domains that should be SANs in the issued certificate. | number | - | yes |
| private_key | The certificate's PEM-formatted private key. | string | "" | yes |
| certificate_file | The certificate's PEM-formatted public key. | string | "" | yes |
| certificate_chain | The certificate's PEM-formatted chain. | string | "" | yes |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

---

## Outputs

| Name | Description |
|------|-------------|
| module.acm.acm_certificate_id | The ARN of the certificate. |
| module.acm.acm_certificate_arn | The ARN of the certificate. |
| module.acm.acm_certificate_domain_name | The domain name for which the certificate is issued. |
| module.acm.acm_certificate_domain_validation_options | A list of attributes to feed into other resources to complete certificate validation. Can have more than one element, e.g. if SANs are defined. Only set if DNS-validation was used. |
| module.acm.acm_certificate_validation_emails | A list of addresses that received a validation E-Mail. Only set if EMAIL-validation was used. |
