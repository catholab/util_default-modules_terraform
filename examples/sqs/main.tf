terraform {
  backend "s3" {
    bucket                  = "infra-lab-terraform-state"
    key                     = "sqs/terraform.tfstate"
    region                  = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "catho-infra"
  }
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "${var.profile}"
}

module "sqs" {
  source = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//sqs"


  ## GLOBAL OPTIONS ##
  context               = "infra"
  environment           = "lab"
  team                  = "infra"
  app_release           = "1.0.1"
  app_source            = "https://github.com/teste/teste.git"
  provisioning_tool     = "terraform"
  provisioning_version  = "0.12.6"
  provisioning_source   = "https://github.com/teste/teste.git"
  deployment_tool       = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr   = "23"

  sqs_queue_create = true
  sqs_queue_options = list(
    {
      name                        = "sqs-queue-infra-teste",
      delay_seconds               = 90,
      max_message_size            = 2048,
      message_retention_seconds   = 86400,
      receive_wait_time_seconds   = 10,
      policy_json_file            = policy.json,
      redrive_policy_json_file    = redrive_policy.json,
      visibility_timeout_seconds  = 30,
      fifo_queue                  = false
      content_based_deduplication = false
      extraTags = {
      }
    },
  )
}
