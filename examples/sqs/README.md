# AWS [`SQS`](https://aws.amazon.com/sqs/) Terraform module example

Terraform module which creates the following resources on AWS:
* SQS Queue

These types of resources are supported:

* [aws_sqs_queue](https://www.terraform.io/docs/providers/aws/r/sqs_queue.html)

## Terraform version
- This module was written in terraform version 0.12. For more details, see this [page](https://www.hashicorp.com/blog/announcing-terraform-0-12).

## Usage

```hcl
module "sqs" {
  source                = "git::ssh://git@github.com/catho/util_default-modules_terraform.git//sqs"


  ## GLOBAL OPTIONS ##
  context = "infra"
  environment = "lab"
  team = "infra"
  app_release = "1.0.1"
  app_source = "https://github.com/teste/teste.git"
  provisioning_tool = "terraform"
  provisioning_version = "0.12.6"
  provisioning_source = "https://github.com/teste/teste.git"
  deployment_tool = "jenkins"
  deployment_build_name = "teste_provisioning"
  deployment_build_nr = "23"

  sqs_queue_create                	= true
  sqs_queue_options               	= list(
                                            {
                                              name = "sqs-queue-infra-teste",
                                              delay_seconds = 90, 
                                              max_message_size = 2048,
                                              message_retention_seconds = 86400,
                                              receive_wait_time_seconds = 10,
                                              policy_json_file = policy.json,
                                              redrive_policy_json_file = redrive_policy.json,
                                              visibility_timeout_seconds = 30,
                                              fifo_queue = false
                                              content_based_deduplication = false
                              		      extraTags = {
                              		      }
                                            },
                                          )
}
```

#### policies/sqs-redrive-policy-teste.json

```hcl
{
        "deadLetterTargetArn": "${aws_sqs_queue.terraform_queue_deadletter.arn}",
        "maxReceiveCount":4
}
```

## Global Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| context | Name of team context. | string | "" | yes |
| environment | Name of environment. | string | "" | yes |
| team | Name of team. | string | "" | yes |
| app_release | Release of application. | string | "" | yes |
| app_source | Link of application git repository. | string | "" | yes |
| provisioning_tool | Name of provisioning tool. | string | "" | yes |
| provisioning_version | Version of provisioning tool. | string | "" | yes |
| provisioning_source | Link of provisioning tool git repository. | string | "" | yes |
| deployment_tool | Name of deployment tool. | string | "" | yes |
| deployment_build_name | Name of deployment build. | string | "" | yes |
| deployment_build_nr | Number of deployment build. | string | "" | yes |

---

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| sqs_queue_create | Boolean to create or not the SQS Queue resource. | bool | - | yes |
| [sqs_queue_options](#sqs_queue_options) | List with the arguments to create the SQS Queue resource. | list(map) | [] | yes |

---

### Arguments
#### **sqs_queue_options**

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | This is the human-readable name of the queue. If omitted, Terraform will assign a random name. | string | "" | yes |
| delay_seconds | The time in seconds that the delivery of all messages in the queue will be delayed. An integer from 0 to 900 (15 minutes). | number | - | yes |
| max_message_size | The limit of how many bytes a message can contain before Amazon SQS rejects it. An integer from 1024 bytes (1 KiB) up to 262144 bytes (256 KiB). | number | - | yes |
| message_retention_seconds | The number of seconds Amazon SQS retains a message. Integer representing seconds, from 60 (1 minute) to 1209600 (14 days). | number | - | yes |
| receive_wait_time_seconds | The time for which a ReceiveMessage call will wait for a message to arrive (long polling) before returning. An integer from 0 to 20 (seconds). The default for this attribute is 0, meaning that the call will return immediately. | number | - | yes |
| policy_json_file | The JSON policy document for the SQS queue. For more information about building AWS IAM policy documents with Terraform, see the [AWS IAM Policy Document Guide](https://www.terraform.io/docs/providers/aws/guides/iam-policy-documents.html). | string | "" | yes |
| redrive_policy_json_file | The JSON policy document to set up the Dead Letter Queue, see AWS docs. Note: when specifying maxReceiveCount, you must specify it as an integer (5), and not a string ("5"). | string | "" | yes |
| visibility_timeout_seconds | The visibility timeout for the queue. An integer from 0 to 43200 (12 hours). The default for this attribute is 30. For more information about visibility timeout, see [AWS docs](https://docs.aws.amazon.com/pt_br/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-visibility-timeout.html). | number | - | yes |
| fifo_queue | Boolean designating a FIFO queue. **If not set, it defaults to false making it standard**. | bool | [] | no |
| content_based_deduplication | Enables content-based deduplication for FIFO queues. For more information, see the [related documentation](https://docs.aws.amazon.com/pt_br/AWSSimpleQueueService/latest/SQSDeveloperGuide/FIFO-queues.html#FIFO-queues-exactly-once-processing) | bool | [] | no |
| extraTags | A mapping of custom tags to assign to the resource. | map | {} | no |

---

## Outputs

| Name | Description |
|------|-------------|
| module.sqs.sqs_queue_id | The ARN of the SQS queue. |
| module.sqs.sqs_queue_arn | The URL for the created Amazon SQS queue. |
