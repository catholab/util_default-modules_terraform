resource "aws_ec2_client_vpn_endpoint" "this" {
  count                  = "${local.ec2_client_vpn_endpoint_count}"
  description            = lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "description")
  server_certificate_arn = lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "server_certificate_arn")
  client_cidr_block      = lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "client_cidr_block")
  dns_servers            = lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "dns_servers")
  split_tunnel           = lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "split_tunnel")
  transport_protocol     = lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "transport_protocol")

  authentication_options {
    type                       = lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "authentication_options_type")
    root_certificate_chain_arn = lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "authentication_options_type") == "certificate-authentication" ? lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "root_certificate_chain_arn") : null
    active_directory_id        = lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "authentication_options_type") == "directory-service-authentication" ? lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "active_directory_id") : null
  }

  connection_log_options {
    enabled               = lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "connection_log_options_enabled")
    cloudwatch_log_group  = lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "cloudwatch_log_group")
    cloudwatch_log_stream = lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "cloudwatch_log_stream")
  }

  tags = merge(
    {
      Name = lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "name"),
    },
    "${local.default_tags}",
    lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "extraTags")
  )
}

resource "aws_ec2_client_vpn_network_association" "this" {
  count                  = "${local.ec2_client_vpn_endpoint_count}"
  client_vpn_endpoint_id = "${aws_ec2_client_vpn_endpoint.this[count.index].id}"
  subnet_id              = "${data.aws_subnet.this[count.index].id}"
}

