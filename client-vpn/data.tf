data "aws_vpc" "this" {
  tags = {
    Name = format("vpc-%s", var.context)
  }
}

data "aws_subnet" "this" {
  count  = "${local.ec2_client_vpn_endpoint_count}"
  vpc_id = "${data.aws_vpc.this.id}"
  filter {
    name   = "tag:Name"
    values = list(lookup(element(var.ec2_client_vpn_endpoint_options, count.index), "subnet_name"))
  }
}
