output "ec2_client_vpn_endpoint_id" {
  value       = "${aws_ec2_client_vpn_endpoint.this[*].id}"
  description = "The ID of the Client VPN endpoint."
}

output "ec2_client_vpn_endpoint_dns_name" {
  value       = "${aws_ec2_client_vpn_endpoint.this[*].dns_name}"
  description = "The DNS name to be used by clients when establishing their VPN session."
}

output "ec2_client_vpn_endpoint_status" {
  value       = "${aws_ec2_client_vpn_endpoint.this[*].status}"
  description = "The current state of the Client VPN endpoint."
}

output "ec2_client_vpn_network_association_id" {
  value       = "${aws_ec2_client_vpn_network_association.this[*].id}"
  description = "The unique ID of the target network association."
}

output "ec2_client_vpn_network_association_status" {
  value       = "${aws_ec2_client_vpn_network_association.this[*].status}"
  description = "The current state of the target network association."
}
