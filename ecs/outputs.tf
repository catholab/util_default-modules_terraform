output "ecs_cluster_id" {
  value       = "${aws_ecs_cluster.this[*].id}"
  description = "The Amazon Resource Name (ARN) that identifies the cluster."
}

output "ecs_cluster_arn" {
  value       = "${aws_ecs_cluster.this[*].arn}"
  description = "The Amazon Resource Name (ARN) that identifies the cluster."
}

output "ecs_task_definition_arn" {
  value       = "${aws_ecs_task_definition.this[*].arn}"
  description = "Full ARN of the Task Definition (including both family and revision)."
}

output "ecs_task_definition_family" {
  value       = "${aws_ecs_task_definition.this[*].family}"
  description = "The family of the Task Definition."
}

output "ecs_task_definition_revision" {
  value       = "${aws_ecs_task_definition.this[*].revision}"
  description = "The revision of the task in a particular family."
}

output "ecs_service_id" {
  value       = "${aws_ecs_service.this[*].id}"
  description = "The Amazon Resource Name (ARN) that identifies the service."
}

output "ecs_service_name" {
  value       = "${aws_ecs_service.this[*].name}"
  description = "The name of the service."
}

output "ecs_service_cluster" {
  value       = "${aws_ecs_service.this[*].cluster}"
  description = "The Amazon Resource Name (ARN) of cluster which the service runs on."
}

output "ecs_service_iam_role" {
  value       = "${aws_ecs_service.this[*].iam_role}"
  description = "The ARN of IAM role used for ELB."
}

output "ecs_service_desired_count" {
  value       = "${aws_ecs_service.this[*].desired_count}"
  description = "The number of instances of the task definition."
}
