data "aws_iam_role" "execution_role" {
  count = "${local.ecs_task_definition_count}"
  name  = lookup(element(var.ecs_task_definition_options, count.index), "execution_role_name")
}

data "aws_iam_role" "task_role" {
  count = "${local.ecs_task_definition_count}"
  name  = lookup(element(var.ecs_task_definition_options, count.index), "task_role_name", null) != null ? lookup(element(var.ecs_task_definition_options, count.index), "task_role_name") : "ecsTaskExecutionRole"
}

data "aws_ecs_cluster" "this" {
  count        = "${local.ecs_service_count}"
  cluster_name = lookup(element(var.ecs_service_options, count.index), "ecs_cluster_name")
  depends_on   = ["aws_ecs_cluster.this"]
}

data "aws_vpc" "this" {
  tags = {
    Name = format("vpc-%s", var.context)
  }
}

data "aws_subnet_ids" "this" {
  count  = "${local.ecs_service_count}"
  vpc_id = "${data.aws_vpc.this.id}"
  filter {
    name   = "tag:Name"
    values = lookup(element(var.ecs_service_options, count.index), "subnet_names")
  }
}

data "aws_security_groups" "this" {
  count = "${local.ecs_service_count}"
  filter {
    name   = "tag:Name"
    values = lookup(element(var.ecs_service_options, count.index), "security_group_names")
  }
}

data "aws_lb_target_group" "this" {
  count = "${local.ecs_service_count}"
  name  = lookup(element(var.ecs_service_options, count.index), "load_balancer")[0]["lb_name"]
}
