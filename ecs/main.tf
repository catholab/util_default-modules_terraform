## ECS CLUSTER RESOURCE ##
resource "aws_ecs_cluster" "this" {
  count = "${local.ecs_cluster_count}"
  name  = lookup(element(var.ecs_cluster_options, count.index), "name")
  tags = merge(
    {
      Name = lookup(element(var.ecs_cluster_options, count.index), "name"),
    },
    "${local.default_tags}",
    lookup(element(var.ecs_cluster_options, count.index), "extraTags")
  )
}

resource "aws_ecs_task_definition" "this" {
  count                    = "${local.ecs_task_definition_count}"
  family                   = lookup(element(var.ecs_task_definition_options, count.index), "family")
  container_definitions    = file(lookup(element(var.ecs_task_definition_options, count.index), "container_definitions"))
  execution_role_arn       = "${data.aws_iam_role.execution_role[count.index].arn}"
  task_role_arn            = lookup(element(var.ecs_task_definition_options, count.index), "task_role_name", null) != null ? "${data.aws_iam_role.task_role[count.index].arn}" : null
  network_mode             = lookup(element(var.ecs_task_definition_options, count.index), "network_mode")
  requires_compatibilities = lookup(element(var.ecs_task_definition_options, count.index), "requires_compatibilities")
  memory                   = lookup(element(var.ecs_task_definition_options, count.index), "memory")
  cpu                      = lookup(element(var.ecs_task_definition_options, count.index), "cpu")

  dynamic "volume" {
    for_each = lookup(element(var.ecs_task_definition_options, count.index), "create_volume") == false ? [] : [for volume in lookup(element(var.ecs_task_definition_options, count.index), "volume") : {
      name      = volume.name
      host_path = volume.host_path
    }]

    content {
      name      = volume.value.name
      host_path = volume.value.host_path
    }
  }

  dynamic "proxy_configuration" {
    for_each = lookup(element(var.ecs_task_definition_options, count.index), "create_proxy_configuration") == false ? [] : [for proxy_configuration in lookup(element(var.ecs_task_definition_options, count.index), "proxy_configuration") : {
      container_name = proxy_configuration.container_name
      properties     = proxy_configuration.properties
      type           = proxy_configuration.type
    }]

    content {
      container_name = proxy_configuration.value.container_name
      properties     = proxy_configuration.value.properties
      type           = proxy_configuration.value.type
    }
  }

  tags = merge(
    {
      Name = lookup(element(var.ecs_task_definition_options, count.index), "family"),
    },
    "${local.default_tags}",
    lookup(element(var.ecs_task_definition_options, count.index), "extraTags")
  )
}


resource "aws_ecs_service" "this" {
  count                              = "${local.ecs_service_count}"
  name                               = lookup(element(var.ecs_service_options, count.index), "name")
  cluster                            = "${data.aws_ecs_cluster.this[count.index].id}"
  task_definition                    = "${aws_ecs_task_definition.this[count.index].arn}"
  desired_count                      = lookup(element(var.ecs_service_options, count.index), "desired_count")
  launch_type                        = lookup(element(var.ecs_service_options, count.index), "launch_type")
  scheduling_strategy                = lookup(element(var.ecs_service_options, count.index), "scheduling_strategy")
  deployment_maximum_percent         = lookup(element(var.ecs_service_options, count.index), "deployment_maximum_percent")
  deployment_minimum_healthy_percent = lookup(element(var.ecs_service_options, count.index), "deployment_minimum_healthy_percent")
  depends_on                         = ["data.aws_ecs_cluster.this", "aws_ecs_task_definition.this", "data.aws_subnet_ids.this", "data.aws_security_groups.this"]

  dynamic "deployment_controller" {
    for_each = lookup(element(var.ecs_service_options, count.index), "create_deployment_controller") == false ? [] : [for deployment_controller in lookup(element(var.ecs_service_options, count.index), "deployment_controller") : {
      type = deployment_controller.type
    }]

    content {
      type = deployment_controller.value.type
    }
  }

  dynamic "load_balancer" {
    for_each = lookup(element(var.ecs_service_options, count.index), "create_load_balancer") == false ? [] : [for load_balancer in lookup(element(var.ecs_service_options, count.index), "load_balancer") : {
      elb_name         = load_balancer.type == "classic" ? load_balancer.elb_name : null
      target_group_arn = load_balancer.type == "lb" ? "${data.aws_lb_target_group.this[count.index].arn}" : null
      container_name   = load_balancer.container_name
      container_port   = load_balancer.container_port
    }]

    content {
      elb_name         = load_balancer.value.elb_name
      target_group_arn = load_balancer.value.target_group_arn
      container_name   = load_balancer.value.container_name
      container_port   = load_balancer.value.container_port
    }
  }

  dynamic "ordered_placement_strategy" {
    for_each = lookup(element(var.ecs_service_options, count.index), "create_ordered_placement_strategy") == false ? [] : [for ordered_placement_strategy in lookup(element(var.ecs_service_options, count.index), "ordered_placement_strategy") : {
      type  = ordered_placement_strategy.type
      field = ordered_placement_strategy.field
    }]

    content {
      type  = ordered_placement_strategy.value.type
      field = ordered_placement_strategy.value.field
    }
  }

  dynamic "placement_constraints" {
    for_each = lookup(element(var.ecs_service_options, count.index), "create_placement_constraints") == false ? [] : [for placement_constraints in lookup(element(var.ecs_service_options, count.index), "placement_constraints") : {
      type       = placement_constraints.type
      expression = placement_constraints.expression
    }]

    content {
      type       = placement_constraints.value.type
      expression = placement_constraints.value.expression
    }
  }

  dynamic "network_configuration" {
    for_each = lookup(element(var.ecs_service_options, count.index), "create_network_configuration") == false ? [] : [for network_configuration in [{ subnets = "${data.aws_subnet_ids.this[count.index].ids}", security_groups = "${data.aws_security_groups.this[count.index].ids}" }] : {
      subnets         = network_configuration.subnets
      security_groups = network_configuration.security_groups
    }]

    content {
      subnets         = network_configuration.value.subnets
      security_groups = network_configuration.value.security_groups
    }
  }
}
