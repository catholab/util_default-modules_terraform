locals {
  default_tags = {
    Context                 = "${var.context}"
    Environment             = "${var.environment}"
    Team                    = "${var.team}"
    AppRelease              = "${var.app_release}"
    AppSource               = "${var.app_source}"
    ProvisioningTool        = "${var.provisioning_tool}"
    ProvisioningVersion     = "${var.provisioning_version}"
    ProvisioningSource      = "${var.provisioning_source}"
    DeploymentTool          = "${var.deployment_tool}"
    DeploymentBuildName     = "${var.deployment_build_name}"
    DeploymentBuildNumber   = "${var.deployment_build_nr}"
    DeploymentLastExecution = formatdate("DD/MM/YYYY - hh:mm:ss - ZZZ", timestamp())
  }

  ecs_cluster_count         = var.ecs_cluster_create == true ? length(var.ecs_cluster_options) : 0
  ecs_service_count         = var.ecs_service_create == true ? length(var.ecs_service_options) : 0
  ecs_task_definition_count = var.ecs_task_definition_create == true ? length(var.ecs_task_definition_options) : 0
}
