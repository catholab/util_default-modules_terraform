## GLOBAL VARIABLES ##
variable "context" {
  type = "string"
}

variable "environment" {
  type = "string"
}

variable "team" {
  type = "string"
}

variable "app_release" {
  type = "string"
}

variable "app_source" {
  type = "string"
}

variable "provisioning_tool" {
  type = "string"
}

variable "provisioning_version" {
  type = "string"
}

variable "provisioning_source" {
  type = "string"
}

variable "deployment_tool" {
  type = "string"
}

variable "deployment_build_name" {
  type = "string"
}

variable "deployment_build_nr" {
  type = "string"
}

variable "ecs_cluster_create" {
  type = bool
}

variable "ecs_cluster_options" {
  type    = "list"
  default = []
}

variable "ecs_service_create" {
  type = bool
}

variable "ecs_service_options" {
  type    = "list"
  default = []
}

variable "ecs_task_definition_create" {
  type = bool
}

variable "ecs_task_definition_options" {
  type    = "list"
  default = []
}
